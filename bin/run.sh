#! /bin/bash

set -o errexit

while getopts a:c: option
do
        case "${option}"
        in
                a) APP=${OPTARG};;
                c) CMD=${OPTARG};;

        esac
done

echo "$APP: $CMD"

. $APP\_venv/bin/activate
. $APP.env

cd $APP/web
$CMD

