insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'Views', '', '', 5330, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'ViewsSource', 'play.adventr.tv', '', 5330, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'ViewsBrowser', 'Chrome', '', 5330, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'ViewsOS', 'OS X', '', 5330, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'ViewsRegion', 'Los Angeles, California', '', 5330, '2014-06-15 06:00:00');


insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'Funnel', 'Load#intro#1', '', 5341, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'Funnel', 'intro#future#1', '', 1783, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'Funnel', 'intro#past#1', '', 1370, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'Funnel', 'future#girl#1', '', 970, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'Funnel', 'future#world#1', '', 550, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'Funnel', 'past#girl#1', '', 455, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'Funnel', 'past#world#1', '', 488, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'Funnel', 'girl#url#0', '', 401, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'Funnel', 'world#url#0', '', 110, '2014-06-15 06:00:00');



insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelSource', 'play.adventr.tv','Load#intro#1', 5341, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelSource', 'play.adventr.tv','intro#future#1', 1783, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelSource', 'play.adventr.tv','intro#past#1', 1370, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelSource', 'play.adventr.tv','future#girl#1', 970, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelSource', 'play.adventr.tv','future#world#1', 550, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelSource', 'play.adventr.tv','past#girl#1', 455, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelSource', 'play.adventr.tv','past#world#1', 488, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelSource', 'play.adventr.tv','girl#url#0', 401, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelSource', 'play.adventr.tv','world#url#0', 110, '2014-06-15 06:00:00');



insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelBrowser', 'Chrome','Load#intro#1', 5341, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelBrowser', 'Chrome','intro#future#1', 1783, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelBrowser', 'Chrome','intro#past#1', 1370, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelBrowser', 'Chrome','future#girl#1', 970, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelBrowser', 'Chrome','future#world#1', 550, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelBrowser', 'Chrome','past#girl#1', 455, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelBrowser', 'Chrome','past#world#1', 488, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelBrowser', 'Chrome','girl#url#0', 401, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelBrowser', 'Chrome','world#url#0', 110, '2014-06-15 06:00:00');


insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelOS', 'OS X','Load#intro#1', 5341, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelOS', 'OS X','intro#future#1', 1783, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelOS', 'OS X','intro#past#1', 1370, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelOS', 'OS X','future#girl#1', 970, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelOS', 'OS X','future#world#1', 550, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelOS', 'OS X','past#girl#1', 455, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelOS', 'OS X','past#world#1', 488, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelOS', 'OS X','girl#url#0', 401, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelOS', 'OS X','world#url#0', 110, '2014-06-15 06:00:00');




insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelRegion', 'Los Angeles, California','Load#intro#1', 5341, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelRegion', 'Los Angeles, California','intro#future#1', 1783, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelRegion', 'Los Angeles, California','intro#past#1', 1370, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelRegion', 'Los Angeles, California','future#girl#1', 970, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelRegion', 'Los Angeles, California','future#world#1', 550, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelRegion', 'Los Angeles, California','past#girl#1', 455, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelRegion', 'Los Angeles, California','past#world#1', 488, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelRegion', 'Los Angeles, California','girl#url#0', 401, '2014-06-15 06:00:00');
insert into base_datapoint (analyticsid, datatype, key1, key2, value, date) values( '1-14-1', 'FunnelRegion', 'Los Angeles, California','world#url#0', 110, '2014-06-15 06:00:00');



