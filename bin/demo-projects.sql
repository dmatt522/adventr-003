use ochre

/*

this script will take an existing user account (@user_id) and:
1. reassign it to a new desired email (@user_email)
2. create a copy of a source demo project (@demo_campaign_id)
3. copy everything over to the new demo project, including historical data

it presumes that the existing user account does not have an existing project of
the same name as the demo project.

*/

-- update these variables to reflect the desired user
select * from auth_user where email = "devon@adventr.tv"
set @user_id = 920;
set @user_email = "trevor@javelin.com";
set @demo_campaign_id = 14;

-- user
update auth_user set
  username = @user_email,
  first_name = '',
  last_name = '',
  email = @user_email
where id = @user_id;

select * from auth_user where id = @user_id;

-- client
select client_id into @client_id from base_profile where user_id = @user_id;
select @client_id;

-- demo campaign name
select name into @demo_campaign_name
from base_videocampaign
where id = @demo_campaign_id;

select  @demo_campaign_name;

-- new campaign
insert into base_videocampaign
  (name, client_id, active, created, config_data)
select name, @client_id, active, now(), config_data
from base_videocampaign
where id = @demo_campaign_id;

select last_insert_id() into @new_campaign_id;
select @new_campaign_id;

-- upload assets
insert into base_uploadasset
  (file_original, file_path, file_id, file_preview_flv, mime_type, user_id,
  campaign_id, created, upload_successful, duration, size_kb, width, height,
  thumbnails, thumbnail_chosen, published, encoded, original_width,
  original_height, jobsys, jobid, job_error_notified)
select
	file_original,
	file_path,
	file_id,
	file_preview_flv,
	mime_type,
	@user_id,
	@new_campaign_id,
  now(),
	upload_successful,
	duration,
	size_kb,
	width,
	height,
	thumbnails,
	thumbnail_chosen,
	published,
	encoded,
	original_width,
	original_height,
	jobsys,
	jobid,
	job_error_notified
from
	base_uploadasset
where campaign_id = @demo_campaign_id;

select count(*) from base_uploadasset where campaign_id = @new_campaign_id;

-- data facts
select concat(@client_id, "-", @new_campaign_id, "-1") into @analytics_id;

insert into base_datafacts
	(day_of_month, day_of_week, month, year, location, source, browser, os, views,
  completions, duration, analyticsid, flashversion, viewsunique, hour)
select
	day_of_month,
	day_of_week,
	month,
	year,
	location,
	source,
	browser,
	os,
	views,
	completions,
	duration,
	@analytics_id,
	flashversion,
	viewsunique,
	hour
from
	base_datafacts
where convert(substring(
  analyticsid,
  locate('-',analyticsid) + 1,
  locate('-',analyticsid, locate('-',analyticsid)+1) - locate('-',analyticsid) -1
  ),signed) = @demo_campaign_id

select count(*) from base_datafacts
where convert(substring(
  analyticsid,
  locate('-',analyticsid) + 1,
  locate('-',analyticsid, locate('-',analyticsid)+1) - locate('-',analyticsid) -1
  ),signed) = @new_campaign_id

-- data point
insert into base_datapoint
	(analyticsid, datatype, key1, key2, value, date)
select
 	@analytics_id,
	datatype,
	key1,
	key2,
	value,
	date
from
	base_datapoint
where substring(
  analyticsid,
  locate('-',analyticsid) + 1,
  locate('-',analyticsid, locate('-',analyticsid)+1) - locate('-',analyticsid) -1
  ) = @demo_campaign_id;

select count(*) from base_datapoint
where substring(
  analyticsid,
  locate('-',analyticsid) + 1,
  locate('-',analyticsid, locate('-',analyticsid)+1) - locate('-',analyticsid) -1
  ) = @new_campaign_id

-- data point value
insert into base_datapointvalue
	(charttype, value, campaign_id)
select
 	charttype,
	value,
	@new_campaign_id
from
	base_datapointvalue
where campaign_id = @demo_campaign_id

select count(*)
from base_datapointvalue
where campaign_id = @new_campaign_id
