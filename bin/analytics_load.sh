#! /bin/bash



function run {

    echo "Running: $@"

    "$@"
    status=$?
    if [ $status -ne 0 ]; then
        echo "ERROR: Command failed with err=$status. command was: $@"
        exit $status
    fi
    return $status
}

if [ $# = 0 ]; then
    START_DATE=`date "+%Y-%m-%d" --date='+1 day ago'`
    DIR="/tmp/$START_DATE"
fi 

if [ $# = 1 ]; then
    START_DATE=$1 
    DIR="/tmp/$START_DATE"
fi

if [ $# = 2 ]; then
    START_DATE=$1
    DIR="$2/$START_DATE"
fi

mkdir -p $DIR


FILENAME_EVENTS=$DIR/$START_DATE\_VideoEvent.json
FILENAME_VIEWS=$DIR/$START_DATE\_VideoView.json
FILENAME_ALL=$DIR/$START_DATE\-all.json
FILENAME_VIEWS_SORTED=$DIR/$START_DATE\_VideoView.sorted.json
FILENAME_COMBINED=$DIR/$START_DATE\_combined.json

echo "Running analytics for $START_DATE, output dir=$DIR"

#############################################
# Download analytics data from DynamoDB
run python manage.py analytics_download_data -f $FILENAME_VIEWS  -s $START_DATE -e "Video View"
run python manage.py analytics_download_data -f $FILENAME_EVENTS -s $START_DATE -e "VideoEvent"
#############################################

cat $FILENAME_VIEWS $FILENAME_EVENTS > $FILENAME_ALL

# Save backups to S3
run python manage.py s3save -d $DIR -b adventr-analytics/data/$START_DATE

run python /home/ubuntu/projects/adventr-analytics/hadoop/bin/run.py $START_DATE


