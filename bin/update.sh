#! /bin/bash

set -o errexit

while getopts g:a:b:x option
do
        case "${option}"
        in
                a) APP=${OPTARG};;
                g) GIT=${OPTARG};;
                b) BRANCH=${OPTARG};;
        esac
done

if [ -z $BRANCH ];then
	BRANCH='master'
fi

timestamp=`date "+%Y%m%d_%H%M%S"`
host=`hostname`
TMP=$APP.tmp 

echo "Deploying $GIT to $APP with tag $timestamp on $host from branch $BRANCH"


####################################### 
# Download latest code to tmp directory
rm -rf $TMP
git clone -b $BRANCH git@github.com:redochrelabs/$GIT.git $TMP
####################################### 



####################################### 
# Set up virtualenv and requirements
if [ ! -d $APP\_venv ]; then
    virtualenv $APP\_venv --distribute
fi

. $APP\_venv/bin/activate
pip install -r $TMP/requirements.txt
####################################### 



####################################### 
# Save previous tag
if [ -f $APP/tag.txt ]; then
    cp $APP/tag.txt $TMP/tag-prev.txt
fi
####################################### 


####################################### 
# Run sync in TMP installation
. $APP\_venv/bin/activate
. $APP.env
cd $TMP/web
python manage.py syncdb
python manage.py migrate --all
python manage.py deploy_static 
cd ../../
####################################### 




####################################### 
# Back up previous version and replace with new
if [ -d $APP ]; then
        mkdir -p backups
        cp -r $APP  backups/$APP.$timestamp
        rm -rf $APP
fi
mv $TMP $APP
chown -R www-data $APP
chgrp -R www-data $APP

####################################### 

echo "Restarting uwsgi by touching /tmp/$APP.touch"
touch /tmp/$APP.touch

####################################### 
# Tag branch
cd $APP/web
echo "$timestamp-$host" > ../tag.txt
git tag -a "$timestamp-$host" -m "Deployed $timestamp to $host"

git push --tags
####################################### 



