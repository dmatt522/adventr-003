# Ochre

More instructions to follow, but to run locally, from the project root:

    vagrant ssh

Once inside vagrant:

    cd /projects
    source ochre2_venv/bin/activate
    cd /projects/ochre2/web
    python manage.py runserver 0.0.0.0:8000

Then navigate to http://0.0.0.0:8000/

To deploy:

    ssh -i ~/Dropbox/Adventr/engineering/ochre.pem ubuntu@107.21.228.209
    cd projects
    sudo ./ochre-deploy/utils/update.sh -g ochre -a ochre

Staging:

    ssh -i ~/Dropbox/Adventr/engineering/ochre.pem ubuntu@107.20.220.0

## Demos

To load an account with the Spiderman video (or another!):

  1. Open ./bin/demo-projects.sql in your favorite sql working env
  2. If the user already has an account, identify that account and set
      @user_id to that id
  3. If the user does not, create an account manually and then start at line 21
      to ensure you have a new account with the client's email address. Note:
      By default we have been using `ch00seUrAdventr` as the password.
  4. The rest of the statements can be run statement by statement to confirm
      that records are being loaded properly.
