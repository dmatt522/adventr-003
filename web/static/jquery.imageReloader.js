/**
* jQuery Image Reloader
*
* @author Dumitru Glavan
* @link http://dumitruglavan.com/
* @version 1.0 (3-FEB-2012)
* @requires jQuery v1.4 or later
*
* @example $('.slow-images').imageReloader()
*
* Find source on GitHub: https://github.com/doomhz/jQuery-Image-Reloader
*
* Dual licensed under the MIT and GPL licenses:
*   http://www.opensource.org/licenses/mit-license.php
*   http://www.gnu.org/licenses/gpl.html
*
*/
(function ($) {
  $.fn.imageReloader = function (options) {

    options = $.extend({}, options, {
      loadingClass: "loading-image",
      reloadTime: 4000,
      maxTries:150,
    });

    var $self = $(this);

    if ($self.length > 1) {
      $self.each(function (i, el) {
        $(el).imageReloader(options);
      });
      return $self;
    }

    $self.data("reload-times", 0);
    var imageHeight = $self.height()
    var imageWidth  = $self.width()

    var $imageReplacer = $('<div class="' + options.loadingClass + '">');
    //$imageReplacer.css({height: imageHeight, width: imageWidth})
    $imageReplacer.hide();
    $imageReplacer.insertAfter($self);

    var showImage = function () {
      $self.show();
      $imageReplacer.remove();
      if( options.onLoad )
          options.onLoad($self[0]);
    };

    $self.bind("error", function () {
      $self.hide();
      $imageReplacer.show()
      var reloadTimes = $self.data("reload-times");
      if (reloadTimes < options.maxTries) {
        setTimeout(function () {
          $self.attr("src", $self.attr("src"));
          var reloadTimes = $self.data("reload-times");
          reloadTimes++;
          $self.data("reload-times", reloadTimes);
        }, options.reloadTime);
      } else if (!$self.is(":visible")) {
        showImage();
        $self.attr("alt", "Image not found :(");
      }
    });

    $self.bind("load", function () {
      showImage();
    });

    return this;
  };

})(jQuery);


/*
 * jQuery UI Touch Punch 0.2.2
 *
 * Copyright 2011, Dave Furfero
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Depends:
 *  jquery.ui.widget.js
 *  jquery.ui.mouse.js
 */
(function(b){b.support.touch="ontouchend" in document;if(!b.support.touch){return;}var c=b.ui.mouse.prototype,e=c._mouseInit,a;function d(g,h){if(g.originalEvent.touches.length>1){return;}g.preventDefault();var i=g.originalEvent.changedTouches[0],f=document.createEvent("MouseEvents");f.initMouseEvent(h,true,true,window,1,i.screenX,i.screenY,i.clientX,i.clientY,false,false,false,false,0,null);g.target.dispatchEvent(f);}c._touchStart=function(g){var f=this;if(a||!f._mouseCapture(g.originalEvent.changedTouches[0])){return;}a=true;f._touchMoved=false;d(g,"mouseover");d(g,"mousemove");d(g,"mousedown");};c._touchMove=function(f){if(!a){return;}this._touchMoved=true;d(f,"mousemove");};c._touchEnd=function(f){if(!a){return;}d(f,"mouseup");d(f,"mouseout");if(!this._touchMoved){d(f,"click");}a=false;};c._mouseInit=function(){var f=this;f.element.bind("touchstart",b.proxy(f,"_touchStart")).bind("touchmove",b.proxy(f,"_touchMove")).bind("touchend",b.proxy(f,"_touchEnd"));e.call(f);};})(jQuery);
