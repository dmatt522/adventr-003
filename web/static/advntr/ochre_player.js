var copyright="Copyright 2014 - Red Ochre Inc. ANY USE OF THE FOLLOWING CODE MUST BE SUBJECT TO PREVIOUS AGREEMENTS BETWEEN OCHRE INC AND YOURSELF.  ANY OTHER USE, INCLUDING DE-OBFUSCATION, IS STRICTLY PROHIBITED UNDER U.S. AND INTERNATIONAL COPYRIGHT LAW. info@adventr.tv";

var MOBILE = (navigator && ( navigator.userAgent.match(/Android/i)
                                 || navigator.userAgent.match(/webOS/i)
                                 || navigator.userAgent.match(/iPhone/i)
                                 || navigator.userAgent.match(/iPad/i)
                                 || navigator.userAgent.match(/iPod/i)
                                 || navigator.userAgent.match(/BlackBerry/i)
                                 ));

var HLS_DEBUG = false;


function clipPlayed( clipName )
{
    var s = document.getElementById('selector');
    var name = s[s.selectedIndex].text;

    _gaq.push(['_trackEvent', name,'clipPlayed',clipName]);
}

function twitter_share(msg, url) {

    var twtLink = 'http://twitter.com/home?status=' + encodeURIComponent(msg + ' ' + url);
    window.open(twtLink, 'ochreTweet', 'status=1,width=500,height=400,top=500,left=500');
}

function stripTrailing( str, c )
{
    if( str.charAt(str.length-1) == c)
        str = str.substring(0, str.length-1);

    return str;
}

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function ochre_share(msg, url, img) {

    //url += '?msg=' + encodeURIComponent(msg);
    //url += '&ord=' + (new Date().getTime());

    var share = {
       method: 'stream.share',
       u: url,
       t:msg
     };

    share['link'] = url;
    share['method'] = 'feed';
    share['description'] = msg;

    FB.ui(share, function(response) { /* alert('Thank you for sharing'); */ });


} 

function getAllQueryParamValues(url) {
    var all = []
    var q = url || document.location.search || document.location.hash;
    if (q) {
            if (/\?/.test(q)) { 
                q = q.split("?")[1]; 
            } // strip question mark

            var pairs = q.split("&");
            for (var i = 0; i < pairs.length; i++) {
                var k = urlEncodeIfNecessary(pairs[i].substring(0, pairs[i].indexOf("=")));
                var v = urlEncodeIfNecessary(pairs[i].substring((pairs[i].indexOf("=") + 1)));
                all[k] = v;
            }
    }
    return all; 
}

function getQueryParamValue(param) {
    var q = document.location.search || document.location.hash;
    if (q) {
            if (/\?/.test(q)) { q = q.split("?")[1]; } // strip question mark
            if (param == null) {
                    return urlEncodeIfNecessary(q);
            }
            var pairs = q.split("&");
            for (var i = 0; i < pairs.length; i++) {
                    if (pairs[i].substring(0, pairs[i].indexOf("=")) == param) {
                            return urlEncodeIfNecessary(pairs[i].substring((pairs[i].indexOf("=") + 1)));
                    }
            }
    }
    return "";
}


function urlEncodeIfNecessary(s) {
        var regex = /[\\\"<>\.;]/;
        var hasBadChars = regex.exec(s) != null;
        return hasBadChars && typeof encodeURIComponent != "undefined" ? encodeURIComponent(s) : s;
}


function isValidWidthHeight(width, height ) {
    return !( width <= 0 || height <= 0 || isNaN(width) || isNaN(height))
}

/*******************************************************/
/* HTTP Live Streaming *********************************/
/*******************************************************/
hls = function () {
    
    var config     = null;
    var totalDuration = 0;
    var videoChoices = [];
    var finished = false;
    var currentClipId = null;
    var currentClipDuration = 0;
    var view_id  = '';
    var previousTime = 0;
    var video = null;

    var pageParams = null;

    var controlsVisible = false;

    var rW = 1;

    return {

        init : function(params)
        {

            pageParams = params;

            $(window).resize(function() { hls.onResize($(window).width(), $(window).height()); } );
            $(window).trigger('resize');

            $.getJSON( decodeURIComponent(pageParams['x']), onConfigLoad);
        },

        pause : function()
        {
            video.pause();
            $("#control_play_big").show();
            return false;
        },

        play : function()
        {
            if( !video )
            {
                $("#buffering").show();
                setTimeout( hls.play, 20 );
                return;
            }

            $("#control_play_div").hide();
            $("#control_pause_div").show();

            video.play();
            $("#control_play_big").hide();

            if( videoChoices.length == 0 )
            {
                videoChoices.push( currentClipId );
                setInterval( updateTime, 50 );

                loadChoices( config[currentClipId] );

                mixpanelAnalytics.start( config.options.initial, video );
            }


            return true;
        },

        onPlaylistLoad : function( data, textStatus, jqXHR )
        {
            try {

                var vobj = $("#video_hls");
                vobj.attr('src',    data.playlist );

                var v = $("#video-div");
                $("#choices").css('width', v.css('width') );
                $("#choices").css('height', v.css('height') );

                video = document.getElementById("video_hls");
                video.addEventListener('loadedmetadata', hls.loadStart, false);
                if( video.controller )
                    video.controller.addEventListener('ended', hls.onEnded, false);
                else
                    video.addEventListener('ended', hls.onEnded, false);
                video.load(); 

                view_id  = data.view_id;
            }
            catch(err)
            {
                console.error('onPlaylistLoad: ' + err); 
                return;
            }
        },

        loadStart : function() {
            video.currentTime = 0;
        },

        onEnded : function() {
            
            if( config[currentClipId].video_choices.length == 0 ) {
                $('#control_replay_big').show();
                return;
            }
        },

        onResize: function(w, h) {
            if( !isValidWidthHeight( w, h) )
                return;

            frameWidth  = w;
            frameHeight = h;

            if( config ) {
                if( (frameWidth / config.width) < (frameHeight / config.height) ) {
                    rW = frameWidth / config.width;
                }
                else {
                    rW = frameHeight / config.height;
                }
            }

            var v = $("#video-div");
                v.css('width',  frameWidth + 'px');
                v.css('height', frameHeight + 'px');

            // position play/restart buttons
            // play    145x145
            // restart 170x170
            var halfW = frameWidth  / 2;
            var halfH = frameHeight / 2;

            $('#control_play_big').css('left', (halfW - (145/2) ).toString() + 'px')
                                  .css('top',  (halfH - (145/2) ).toString() + 'px');

            $('#control_replay_big').css('left', (halfW - (175/2) ).toString() + 'px')
                                    .css('top',  (halfH - (175/2) ).toString() + 'px');

            $('#buffering').css('left', (halfW - (16/2) ).toString() + 'px')
                           .css('top',  (halfH - (16/2) ).toString() + 'px');

            $('#controls').css('left', (halfW - (100/2) ).toString() + 'px')

            $('#controls_trigger').css('left', (halfW - (100/2) ).toString() + 'px')
        },

        choose : function(id, autoChoose)
        {
            var choice = id.replace('choice_', '');
            var chosenClipObj = config[choice];


            var choices = $('#choices');
            if( decodeURIComponent(pageParams['x']).indexOf("903-998") >= 0) {
                choices.empty();
            }
            choices.data('chosen', true);

            if( chosenClipObj.hasOwnProperty('url') )
            {
                if( chosenClipObj['url'].length > 0 )
                {
                    $("#control_pause").click();
                    window.open(chosenClipObj.url, '_blank');
                }
            }
            else if( chosenClipObj.hls.hasOwnProperty('playlist') || chosenClipObj.hls.segment_durations.length > 0 )
            {
                var o = $('img', '#choice_' + id);
                var newWidth = (o.width() * 1.5);
                var newHeight = (o.height() * 1.5);
                o.animate({opacity : 0, width : newWidth + "px", height: newHeight + "px"}, 1500, function() {
                    var choices = $('#choices');
                    choices.empty();
                    choices.css('visibility', 'hidden');
                });
               

                $( ".choiceLabel[id!='choice_" + id + "']", '#choices').hide();

                videoChoices.push( choice );

                var onChoice = function( currentClipObj, choice, playlist ) 
                {
                    if( currentClipObj.instantSwitch == '1')
                    {

                        var vobj = $("#video_hls");
                        vobj.attr('src', playlist );
                        video.load();
                        video.play();

                        totalDuration = 0;
                        currentClipDuration = 0;
                        currentClipId  = choice;
                    }
                };

                var currentClipObj = config[currentClipId];
                if( currentClipObj.instantSwitch == '1' && chosenClipObj.hls.hasOwnProperty('playlist') )
                {
                    onChoice(currentClipObj, choice, config.options.baseUrl + '/' + chosenClipObj.hls.playlist);
                }
                else
                {
                    var url    = '/hls_choose/';
                    var params = {
                            view_id : view_id, 
                            segment_id : choice, 
                            bitrates : config.options.bitrates.join(), 
                            instant_switch : currentClipObj.instantSwitch,
                            segment_durations : chosenClipObj.hls.segment_durations.join(), 
                            base_url : config.options.baseUrlHLS
                    };
                    
                    $.post( url, params, function(res) { 

                        onChoice( currentClipObj, choice, res.playlist );

                    }, "json").error( function(data, msg) { console.error("choose: " + msg); });
                }
            }

            mixpanelAnalytics.trackChoice( choice, currentClipId, {'autoChoose' : autoChoose });

            return false;
        },

        getConfig : function() {
            return config;

        }

    }

    function getChoicePropOrDefault( obj, name, def )
    {
        if( obj.hasOwnProperty(name) )
            return obj[name];
        return def; 
    }

    function loadChoices( choiceData )
    {

        var choices = $('#choices');
        choices.data('chosen', false);
        for( var i = 0; i < choiceData.video_choices.length; i++ )
        {
            var choice = choiceData.video_choices[i];
            var color  = getChoicePropOrDefault( choice, 'labelColor', null);
            var border = getChoicePropOrDefault( choice, 'border', null);
            var size   = getChoicePropOrDefault( choice, 'labelSize', null);
            var bkColor= getChoicePropOrDefault( choice, 'backgroundColor', null);
            var alpha  = parseFloat(getChoicePropOrDefault( choice, 'alpha', '1')) * 10;
            var html   = getChoicePropOrDefault( choice, 'html', null);

            var htmlstr = '<div id="choice_' + choice.next_video_id + '" data-choice-id="' + choice.next_video_id + '" class="choiceLabel transparent' + alpha + '" ';
            htmlstr += 'style="position:absolute';

            if( color )
                htmlstr += ';color:' + color;

            if( border )
                htmlstr += ';border:' + border;

            if( size )
                htmlstr += ';font-size:' + size;

            if( bkColor )
                htmlstr += ';background-color:' + bkColor;

            htmlstr += ';display:none';
            htmlstr +='">';

            if( choice.hasOwnProperty('image') )
                htmlstr += '<img data-choice-id="' + choice.next_video_id + '" class="" src="' + config.options.images + '/' + choice.image + '" />';

            if( html )
                htmlstr += html;

            if( choice.hasOwnProperty('label') )
                htmlstr += '<div data-choice-id="' + choice.next_video_id + '" class=" transparent10">' + choice.label + '</div>';

            htmlstr += '</div>';

            choices.append( $(htmlstr) );
        }

        // get duration for this clip
        currentClipDuration = 0;
        for( var i = 0; i < choiceData.hls.segment_durations.length; i++)
            currentClipDuration += choiceData.hls.segment_durations[i]; 
    }

    function updateTime()
    {
        try
        {
            var time = video.currentTime;

            if( isNaN(time) || time < 0)
                return;

            if( time > 1 && time == previousTime && video && !video.paused )
            {
                if( config[currentClipId].video_choices.length == 0 ) {
                    $('#control_replay_big').show();
                    return;
                }
                else if( video && !video.paused)
                    $("#buffering").show(); 
                return;
            }
            else
                $("#buffering").hide();

            previousTime = time;

            var choiceData  = config[currentClipId]
            var choicesTime = parseInt(choiceData.choices_time) / 1000;
            var choices     = $('#choices');


            if( HLS_DEBUG )
                $("#hlsDebug").html( currentClipId + " time=" + time.toFixed(2) + " choicesTime=" + choicesTime  + " currentClipDuration=" + currentClipDuration + " totalDuration=" + totalDuration  + " choices.data(currentClipId)=" + choices.data(currentClipId) + " rW=" + rW).show();

            // determine if we should show choices now
            if( choiceData.video_choices.length > 0 && 
                choices.data(currentClipId) == undefined && 
                time > (totalDuration + choicesTime))
            {
                // add an attribute for this video id to the choices div to signal that we've shown it already
                choices.data(currentClipId, true);

                // choice images should be loaded by now, set positions 

                var tile_width = frameWidth / choiceData.video_choices.length;
                $('.choiceLabel', choices).each( function( i, obj) {

                    var jobj = $(obj);
                    var choice = choiceData.video_choices[i];

                    var x,y;
                    if( choice.hasOwnProperty('left') )
                        x = choice['left'];
                    else
                    {
                        var shift_x = (tile_width * i );
                        var x_position_within_tile = (tile_width/2) - ( jobj.width() / 2 );
                        x = shift_x + x_position_within_tile;
                    }

                    if( choice.hasOwnProperty('top') )
                        y = choice['top'];
                    else
                        y = (frameHeight / 2) - ( jobj.height() / 2);

                    x *= rW;
                    y *= rW;

                    var newWidth = $(obj).width() * rW;
                    var newHeight = $(obj).height() * rW;
  
                    if( choice.hasOwnProperty('width') && choice.hasOwnProperty('height') )
                    {
                        newWidth  = choice['width']*rW
                        newHeight = choice['height']*rW  
                    }

                    jobj.css('width', newWidth + 'px');
                    jobj.css('height', newHeight + 'px');

                    $('img', jobj).css('width', newWidth + 'px');
                    $('img', jobj).css('height', newHeight + 'px');

                    jobj.css('top', y + 'px');
                    jobj.css('left',x + 'px');
                    jobj.show();

                    if( HLS_DEBUG )
                        jobj.removeClass('transparent0').addClass('transparent9').css('border', '1px solid red');
                });

               
                choices.css('visibility', 'visible'); 

                $('div', choices).click( function(e) { 
                    return hls.choose( $(this).data('choice-id').toString(), $(this).data('autoChoose') ) 
                } );
            } 

            // auto choose?
            else if( choiceData.video_choices.length > 0 && choices.data(currentClipId) == true && !choices.data('chosen'))
            {
                if( time > (totalDuration + currentClipDuration - 3) )
                {
                $('#choices').data('chosen', true);
                    // choose first non-url
                    for( var i in choiceData.video_choices )
                    {
                        var id = choiceData.video_choices[i].next_video_id;
                        if( config[id].hasOwnProperty('url') )
                            continue;

                        var chosen = $("#choice_" + id );
                        chosen.data('autoChoose', true);
                        chosen.click();
                        break;
                    }
                }
            }

            // determine if new clip is running now
            if( totalDuration + currentClipDuration < time )
            {
                
                if( config[currentClipId].video_choices.length > 0 )
                {
                    totalDuration    += currentClipDuration;
                    currentClipId  = videoChoices[videoChoices.length - 1 ]
                    choiceData        = config[currentClipId]

                    loadChoices( choiceData ); 
                }
            }
        }
        catch(err)
        {
            //alert("updateTime: " + err); 
            console.error("ERROR updateTime: " + err);
        }
    }

    function showControls()
    {
        if( controlsVisible )
            return;

        controlsVisible = true;
        $("#controls_trigger").hide();
        $("#controls").show( function() {

            setTimeout( function() {
                if( decodeURIComponent(pageParams['x']).indexOf("903-998") >= 0) { 
                    return;
                }
                hideControls();
            }, 3500);
        });

    }

    function hideControls() 
    {
        if( !controlsVisible )
            return;

        controlsVisible = false;

        $("#controls").hide( );
        $("#controls_trigger").show();
    }

    function toggleControls()
    {
        
        if( decodeURIComponent(pageParams['x']).indexOf("903-998") >= 0) { 
            showControls();
            return;
        }
        if( controlsVisible )
            hideControls();
        else 
            showControls();
    }

    function onConfigLoad(data, textStatus, jqXHR)
    {
        config = data;
        currentClipId = data.options.initial;

        config.options.images = stripTrailing(config.options.images, '/');

        if( pageParams.hasOwnProperty('poster'))
            config.splash['src'] = decodeURIComponent(pageParams['poster']);
        else if( config.splash.src )
            config.splash['src'] = config.options.images + '/' + config.splash.src;

            
        var vobj = $("#video_hls");

        if( decodeURIComponent(pageParams['x']).indexOf("903-998") >= 0) {
            config.splash.src = null;
        }

        if( config.splash.src )
            vobj.attr('poster', config.splash.src );

        // check for hls support
        if( config.options.baseUrlHLS )
        { 

            $("#control_play").click( function() {
                $("#control_play_div").hide();
                $("#control_pause_div").show(); 
                hls.play();
                toggleControls();
            });

            $("#control_pause").click( function() {
                $("#control_pause_div").hide();
                $("#control_play_div").show(); 
                hls.pause();
            }); 

            $("#control_share").click( function() {
                $("#shareTemplate").tmpl( { config : hls.getConfig(), STATIC_URL : STATIC_URL } ).appendTo("#video-div");
            });


            if( (frameWidth / config.width) < (frameHeight / config.height) ) {
                rW = frameWidth / config.width;
            }
            else {
                rW = frameHeight / config.height; 
            }

            vobj.attr('width',  frameWidth);
            vobj.attr('height', frameHeight);

            var v = $("#video-div");
                v.css('width',  frameWidth + 'px');
                v.css('height', frameHeight + 'px');


            $("#video-div").show();

            $("#controls_trigger").click( toggleControls );


            $("#control_play_big").click( function() { $("#control_play").click(); } );
            $("#control_replay_big, #control_replay_div").click( function() { 
                mixpanelAnalytics.trackChoice('Replay', currentClipId);
                document.location.reload(true); 
            } );


            var obj = config[currentClipId];

            if( obj.instantSwitch == '1' && obj.hls.hasOwnProperty('playlist') ) // only instantSwitch uses the published playlist type
            {
                hls.onPlaylistLoad( {playlist : config.options.baseUrl + '/' + obj.hls.playlist, view_id:''} );
            }
            else
            {

                $.ajax({
                    dataType: "json",
                    url: '/hls_create/?segment_id=' + currentClipId + '&bitrates=' + config.options.bitrates + '&segment_durations=' + obj.hls.segment_durations + '&instant_switch=' + obj.instantSwitch + '&base_url=' + config.options.baseUrlHLS,
                    data: {},
                    error : function(jqXHR, textStatus, err) { 
                        console.error("onConfigLoad: " + textStatus + ": " + err);
                    },
                    success: hls.onPlaylistLoad
                });
            }
        }
        else if( config.options.altMp4 )
        {
            $("#flashContent").html( $('<video width="' + config.width + '" height="' + config.height + '" src="' + config.options.altMp4 + '" type="video/mp4" poster="' + config.options.images + '/' + config.splash.src + '" autoplay controls> </video>') );
            $("#flashContent").show();
        }


        mixpanelAnalytics.init( config.name, pageParams );
    }


}();


function advntrPlaying(videoName)
{
    console.log('advntrPlaying(' + videoName + ')');
    cm.reset();
}

// called from flash
function advntrChoiceDisplay( id, props )
{
    console.log('advntrChoiceDisplay(' + id + ') props=' + props.top + ',' + props.left+ ',' + props.width+ ',' + props.height);
    cm.displayChoice( id, props );
}

// called from flash
function advntrChoiceMade( id )
{
    console.log('advntrChoiceMade(' + id + ')');
    cm.reset();
}

function getFlashMovieObject(movieName){
	if (window.document[movieName]){
		return window.document[movieName];
	}
	if (navigator.appName.indexOf("Microsoft Internet")==-1){
            if (document.embeds && document.embeds[movieName])
                return document.embeds[movieName];
	}
	else{
		return document.getElementById(movieName);
	}
}

/*******************************************************/
/* Cam                 *********************************/
/*******************************************************/
cm = function() {

    var choices = {};

    var content = null;
    var video   = null;
    var lastImageData;
    var canvasSource   = null;
    var canvasBlended  = null;
                      
    var contextSource  = null;
    var contextBlended = null;

    return {

        displayChoice : function( id, props )
        {
            if( !video )
                return;

            if( !choices[id] ) 
            {
                choices[ id ] = props; 

                $("#content").append( $("<div class='camchoicebutton' style='top:" + props.top + "px;left:" + props.left + "px;width:" + props.width + "px;height:" + props.height + "px;'><img src='" + STATIC_URL + "advntr/assets/shim.gif' width='" + props.width + "px' height='" + props.height + "px' /></div>") );
            } 

        },

        reset : function()
        {
            choices = {};
            $('.camchoicebutton').remove();
        },


        init : function(w,h)
        { 
            // add video/webcam elements
            $('body').append(' <div id="content" style="position:relative;text-align:left;"> <canvas id="canvas-source" width="' + w + 'px" height="' + h + 'px"  class="invisible"></canvas> <video id="webcam" autoplay width="' + w + '" height="' + h + '" class="invisible"></video> <video id="video-demo" controls="controls" poster="" width="' + w + '" height="' + h + '"></video> <canvas id="canvas-blended" width="' + w + '" height="' + h + '" class="invisible"></canvas> </div>');



            window.requestAnimFrame = (function(){
                    return  window.requestAnimationFrame       ||
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame    ||
                            window.oRequestAnimationFrame      ||
                            window.msRequestAnimationFrame     ||
                            function( callback ){
                                    window.setTimeout(callback, 1000 / 60);
                            };
            })();


            content = $('#content');
            video   = $('#webcam')[0];

            var resize = function() {
                    var w = $(this).width();
                    var h = $(this).height() - 110;
                    var ratio = video.width / video.height;
                    if (content.width() > w) {
                            content.width(w);
                            content.height(w / ratio);
                    }
                    else {
                            content.height(h);
                            content.width(h * ratio);
                    }
                    content.css('left', (w - content.width()) / 2 );
                    content.css('top', ((h - content.height()) / 2) + 55 );
            }
            $(window).resize(resize);


            var webcamError = function(e) {
                    console.error("Webcam Error: " + e);
            };

            if (navigator.getUserMedia) {
                    navigator.getUserMedia({audio: false, video: true}, function(stream) {
                            video.src = stream;
                            update();
                    }, webcamError);
            } else if (navigator.webkitGetUserMedia) {
                    navigator.webkitGetUserMedia({audio:false, video:true}, function(stream) {
                            video.src = window.webkitURL.createObjectURL(stream);
                            update();
                    }, webcamError);
            } else {
                    //video.src = 'somevideo.webm'; // fallback.
            }


            canvasSource = $("#canvas-source")[0];
            canvasBlended = $("#canvas-blended")[0];

            contextSource = canvasSource.getContext('2d');
            contextBlended = canvasBlended.getContext('2d');

            // mirror video
            contextSource.translate(canvasSource.width, 0);
            contextSource.scale(-1, 1);
        }

    }

    function choose( id )
    {
        cm.reset();
        var f = getFlashMovieObject('AdvntrPlayer');
        if( f )
        {
            try
            {
                f.choose( id );
            }
            catch(err)
            {
                console.error("Error calling choose(" + id + ") err=" + err);
            }

        }
        else
        {
            console.error("AdvntrPlayer obj is null");
        }
    }

    function update() {
            drawVideo();
            blend();
            checkAreas();
            requestAnimFrame(update);
    }

    function drawVideo() {
            contextSource.drawImage(video, 0, 0, video.width, video.height);
    }

    function blend() {
            var width = canvasSource.width;
            var height = canvasSource.height;
            // get webcam image data
            var sourceData = contextSource.getImageData(0, 0, width, height);
            // create an image if the previous image doesnâ€™t exist
            if (!lastImageData) lastImageData = contextSource.getImageData(0, 0, width, height);
            // create a ImageData instance to receive the blended result
            var blendedData = contextSource.createImageData(width, height);
            // blend the 2 images
            differenceAccuracy(blendedData.data, sourceData.data, lastImageData.data);
            // draw the result in a canvas
            contextBlended.putImageData(blendedData, 0, 0);
            // store the current webcam image
            lastImageData = sourceData;
    }

    function fastAbs(value) {
            // funky bitwise, equal Math.abs
            return (value ^ (value >> 31)) - (value >> 31);
    }

    function threshold(value) {
            return (value > 0x15) ? 0xFF : 0;
    }

    function difference(target, data1, data2) {
            // blend mode difference
            if (data1.length != data2.length) return null;
            var i = 0;
            while (i < (data1.length * 0.25)) {
                    target[4*i] = data1[4*i] == 0 ? 0 : fastAbs(data1[4*i] - data2[4*i]);
                    target[4*i+1] = data1[4*i+1] == 0 ? 0 : fastAbs(data1[4*i+1] - data2[4*i+1]);
                    target[4*i+2] = data1[4*i+2] == 0 ? 0 : fastAbs(data1[4*i+2] - data2[4*i+2]);
                    target[4*i+3] = 0xFF;
                    ++i;
            }
    }

    function differenceAccuracy(target, data1, data2) {
            if (data1.length != data2.length) return null;
            var i = 0;
            while (i < (data1.length * 0.25)) {
                    var average1 = (data1[4*i] + data1[4*i+1] + data1[4*i+2]) / 3;
                    var average2 = (data2[4*i] + data2[4*i+1] + data2[4*i+2]) / 3;
                    var diff = threshold(fastAbs(average1 - average2));
                    target[4*i] = diff;
                    target[4*i+1] = diff;
                    target[4*i+2] = diff;
                    target[4*i+3] = 0xFF;
                    ++i;
            }
    }


    function checkAreas() {

            for( var choice in choices )
            {
                var props = choices[choice];

                if( !props || !props.hasOwnProperty('left') || !props.hasOwnProperty('top') || !props.hasOwnProperty('width') || !props.hasOwnProperty('height') )
                    continue;

                var blendedData = contextBlended.getImageData(props.left, props.top, props.width, props.height);
                var i = 0;
                var average = 0;
                // loop over the pixels
                while (i < (blendedData.data.length * 0.25)) {
                        // make an average between the color channel
                        average += (blendedData.data[i*4] + blendedData.data[i*4+1] + blendedData.data[i*4+2]) / 3;
                        ++i;
                }
                // calculate an average between of the color values of the note area
                average = Math.round(average / (blendedData.data.length * 0.25));
                if (average > 10) {
                        choose( choice );
                }
            }
    }


}();

mixpanelAnalytics = function() {

    var properties = {}
    var choiceCount = 0;

    var timer = null;

    var ENGAGEMENT_INTERVAL = 5000; // milliseconds
    var video = null;
    var previousTime = 0;

    return {

        init: function(videoName, pageParams) {

            properties['Video']    = videoName;
            properties['viewId']   = getIdentifier(32);
            properties['distinct_id'] = getIdentifier(32); // TODO set and use from cookie 
            properties['referrer'] = document.referrer;
            properties['ua']       = ( navigator ? navigator.userAgent : '<unknown>' );

            mixpanelAnalytics.trackChoice('Load');

        },

        reset : function() {
            properties['viewId']   = getIdentifier(32);
            choiceCount = 0;
            previousTime = 0;

            if( timer )
                clearInterval( timer );
        },


        sendEngagementMetric : function() {

            if( !video )
            {
                console.error("Video element is null!");
                return;
            }

            var time = video.currentTime;
            if( isNaN(time) || time < 0)
                return;

            time = Math.ceil( time );

            if( previousTime == time )
                return;

            previousTime = time;
            
            var props = copyDict( properties );

            props['duration'] = time;
            if( window.mixpanel )
                mixpanel.track( "Video View", props );

            //$.post( '/analytics/track/', props, function() { }, "json").error( function(data, msg) { console.error("mixpanelAnalytics: " + msg); });
        },

        start : function(initial, v) {
            video = v;

            mixpanelAnalytics.trackChoice( initial, 'Load' );
            timer = setInterval( mixpanelAnalytics.sendEngagementMetric, ENGAGEMENT_INTERVAL );
        },

        trackChoice : function(choiceLabel, referrer, extraProps) {


            var props = copyDict( properties );
                props['action']      = 'ClipChosen';
                props['choiceCount'] = choiceCount;
                props['clip']        = choiceLabel;

            if( referrer )
                props['referringChoice'] = referrer;

            if( extraProps )
            {
                for( var k in extraProps )
                    props[k] = extraProps[k];
            }

            if( window.mixpanel )
                mixpanel.track( "VideoEvent", props ); 

            //$.post( '/analytics/track/', props, function() { }, "json").error( function(data, msg) { console.error("mixpanelAnalytics: " + msg); });

            choiceCount++;
        }
    };

    function getIdentifier(strlen)
    {
            var chars       = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var num_chars   = chars.length - 1;
            var randomChar  = "";
            
            for (var i = 0; i < strlen; i++)
                randomChar += chars.charAt(Math.floor(Math.random() * num_chars));

            return randomChar;
    }

    function copyDict(dict)
    {
        var d = {}
        for( var k in dict )
            d[k] = dict[k];

        return d;
    }


}();






