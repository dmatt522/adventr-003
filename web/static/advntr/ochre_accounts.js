var copyright="Copyright 2014 - Red Ochre Inc. ANY USE OF THE FOLLOWING CODE MUST BE SUBJECT TO PREVIOUS AGREEMENTS BETWEEN OCHRE INC AND YOURSELF.  ANY OTHER USE, INCLUDING DE-OBFUSCATION, IS STRICTLY PROHIBITED UNDER U.S. AND INTERNATIONAL COPYRIGHT LAW. info@adventr.tv";

var J_DEBUG = true;

var MAX_FUNNEL_LINE_WIDTH = 48;

var MOBILE = (navigator && ( navigator.userAgent.match(/Android/i)
                                 || navigator.userAgent.match(/webOS/i)
                                 || navigator.userAgent.match(/iPhone/i)
                                 || navigator.userAgent.match(/iPad/i)
                                 || navigator.userAgent.match(/iPod/i)
                                 || navigator.userAgent.match(/BlackBerry/i)
                                 ));

var MONTH_NAMES = [ "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December" ];

var removedConnections = {};

if (!String.prototype.trim) {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
    };
}

Object.keys = Object.keys || (function () {
    var hasOwnProperty = Object.prototype.hasOwnProperty,
        hasDontEnumBug = !{toString:null}.propertyIsEnumerable("toString"),
        DontEnums = [
            'toString',
            'toLocaleString',
            'valueOf',
            'hasOwnProperty',
            'isPrototypeOf',
            'propertyIsEnumerable',
            'constructor'
        ],
        DontEnumsLength = DontEnums.length;

    return function (o) {
        if (typeof o != "object" && typeof o != "function" || o === null)
            throw new TypeError("Object.keys called on a non-object");

        var result = [];
        for (var name in o) {
            if (hasOwnProperty.call(o, name))
                result.push(name);
        }

        if (hasDontEnumBug) {
            for (var i = 0; i < DontEnumsLength; i++) {
                if (hasOwnProperty.call(o, DontEnums[i]))
                    result.push(DontEnums[i]);
            }
        }

        return result;
    };
})();

String.prototype.hashCode = function(){
    var hash = 0, i, c;
    if (this.length == 0) return hash;
    for (i = 0, l = this.length; i < l; i++) {
        c  = this.charCodeAt(i);
        hash  = ((hash<<5)-hash)+c;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

String.prototype.stripTrailing = function(c) {

    var str = this;

    while( str.charAt( str.length - 1 ) == c )
        str = str.substring( 0, str.length - 1 );

    return str;
}


function getQueryParamValue(param) {
    var q = document.location.search || document.location.hash;
    if (q) {
            if (/\?/.test(q)) { q = q.split("?")[1]; } // strip question mark
            if (param == null) {
                    return urlEncodeIfNecessary(q);
            }
            var pairs = q.split("&");
            for (var i = 0; i < pairs.length; i++) {
                    if (pairs[i].substring(0, pairs[i].indexOf("=")) == param) {
                            return urlEncodeIfNecessary(pairs[i].substring((pairs[i].indexOf("=") + 1)));
                    }
            }
    }
    return "";
}


function urlEncodeIfNecessary(s) {
        var regex = /[\\\"<>\.;]/;
        var hasBadChars = regex.exec(s) != null;
        return hasBadChars && typeof encodeURIComponent != "undefined" ? encodeURIComponent(s) : s;
}


function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function dictIsEmpty( dict )
{
    for (var prop in dict) if (dict.hasOwnProperty(prop)) return false;
        return true;
}

function strToDate( datestr )
{
    var parts = datestr.split('-');
    return new Date( parseInt( parts[0] ), parseInt( parts[1] ) - 1, parseInt( parts[2] ) );
}

function dateDiff(dateEarlier, dateLater) {
    var one_day=1000*60*60*24
    return (  Math.round((dateLater.getTime()-dateEarlier.getTime())/one_day)  );
}


/**************************************************************/
/* DASHBOARD */
/**************************************************************/
var pieCalculator = function() {
    var data = [];

    return {

        init : function( json_data, itemName ) {

            var totals = {}
            for( var strDate in json_data )
            {
                var items = json_data[strDate];

                for( var i = 0; i < items.length; i++ )
                {
                    var d = items[i][itemName];
                    if( !d || d == 'None')
                        continue;


                    if( ! totals[d] )
                        totals[d] = {label: d, data: 0 };

                    totals[d]['data'] += 1;
                }
            }

            for( var k in totals )
            {
                data.push( totals[k] );
            }
        },

        getData : function() {
            return data;
        }
    }
}();

var barChartCalculator = function() {

    var data = [];

    return {

        init : function( json_data, itemName ) {

            var totals = {};
            for( var strDate in json_data )
            {
                var items = json_data[strDate];

                for( var i = 0; i < items.length; i++ )
                {
                    var d = items[i][itemName];
                    if( !d || d == 'None')
                        continue;

                    if( ! totals[d] )
                        totals[d] = 0;
                    totals[d] += 1;
                }
            }

            for( var k in totals )
            {
                data.push( [k, totals[k]] );
            }
        },

        getData : function() {
            return data;
        }
    }

}();

function compareLowerCase(a,b) {
    var al = a.toLowerCase();
    var bl = b.toLowerCase();
  if (al < bl)
     return -1;
  if (al > bl)
    return 1;
  return 0;
}

function compareData(a,b) {
  if (a.label2 < b.label2)
     return -1;
  if (a.label2 > b.label2)
    return 1;
  return 0;
}

var plot      = null;
var skip      = {};
var selectAll = true;

function plotAccordingToChoices(segmentName) {

    var data = [];

    var sdata = clone(click_data);
    for( var key in sdata )
    {
        var skipThis = skip[key];

        var d   = sdata[key];
        var obj = d.data;

        if(skipThis)
        {
            d['oldcolor'] = d.color;
            d.color = '#ffffff';
        }
        else if( d.oldcolor)
            d.color = d.oldcolor;


        for( var i = 0; i < obj.length; i++)
        {
            var d = obj[i]
            d[0] = strToDate(d[0]);

            if(skipThis)
                d[1] = null;
        }

        data.push(sdata[key]);
    }

    data.sort(compareData);

    if (data.length > 0)
    {
        $("#legendHolder").show()

        if( plot )
            plot.shutdown();

        var $chart_div = $("#chart_div");

        $chart_div.empty().show();

        // set the width explicitly before loading, to prevent resize bug
        $chart_div.width($chart_div.parent().width());

        plot = $.plot($chart_div, data,
        {
            selection: {
                  mode: "x"
            },
            series: {
                shadowSize: 0,
                points : {show: true, lineWidth : 2, fill:false},
                lines: { show: true, lineWidth: 2, fill: false }

            },
            legend: {
                container: '#legendHolder' ,
            },
            yaxis: {
                color: "#cccccc",
                tickDecimals: 0,
                axisLabel: segmentName,
                axisLabelUseCanvas: false,
                labelWidth:15,
                axisLabelFontSizePixels: 11,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding:10
            },
            xaxis : {
                mode: "time",
                timeformat: "%m/%d",
                color: "#cccccc",
                axisLabel: "Date",
                axisLabelUseCanvas: false,
                axisLabelFontSizePixels: 11,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 10
            },
            grid : {
                hoverable : true,
                clickable : true,
                tickColor: "#eeeeee",
                borderColor:"#dddddd",
                borderWidth: 1
            },

            hooks : {

                drawOverlay : [ function() {

                    if( $('#selectAll').length == 0 )
                    {

                        $('tbody', '#legendHolder').append('<tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(0,0,0);overflow:hidden"></div></div></td><td id="selectAll" class="legendLabel">All</td></tr>');

                        $('.legendColorBox').unbind('click').click(function() {

                            var t = $('.legendLabel', $(this).parent()).text().trim();
                            if( t == 'All' )
                            {
                                if( selectAll )
                                {
                                    $("#selectAll").html('Unselect All')
                                    for( var key in click_data )
                                        skip[key] = true;
                                }
                                else
                                {
                                    skip = {};
                                }

                                selectAll = !selectAll;
                            }
                            else
                                skip[t] = !skip[t];

                            plotAccordingToChoices(segmentName.capitalize());
                            return true

                        });
                    }

                }]

            }

        });

        var previousPoint = null;
        $chart_div.unbind("plothover").bind("plothover", function(event, pos, item) {
             if (item)
             {
                var index = item.seriesIndex + '-' +item.dataIndex;
                if (previousPoint != index)
                {
                    previousPoint = index;

                    $("#tooltip").remove();
                    var x = new Date(item.datapoint[0]);
                    var y = item.datapoint[1];

                    showTooltip(item.pageX, item.pageY, item.series.label + ": " + y + " on " + (x.getMonth()+1) + '/' + x.getDate());
                }
            }
            else
            {
                $("#tooltip").remove();
                previousPoint = null;
            }

        });

        // restore the 100% width
        $chart_div.width('100%');
    }
    else
    {
        $('#chart_div').empty().hide();
        $('#chart_help').html( $('<div class="dataEmpty">No published data for that filter</div>') ).show();

    }
}


function loadHelp()
{
    $('#help-modal').bind('show').modal({ backdrop: true }).on('shown', function() { $('#intro_iframe').attr('src', '/embed.html?load=0&poster=//d252srr1zuysk4.cloudfront.net/clients/ochre-1/106/instructionalcover.png&width=640&height=326&x=http://static.theochre.com/clients/ochre-1/106/published/ochre-1-instructional-demo-49081246.data&altMp4=//d252srr1zuysk4.cloudfront.net/clients/ochre-1/106/1-beginning-mobile.mp4'); }).bind('hidden', function() { $('#intro_iframe').unbind('load').attr('src', STATIC_URL + 'blank.html'); });
}


function showTooltip(x, y, contents) {
        $("<div id='tooltip'>" + contents + "</div>").css({
                position: "absolute",
                display: "none",
                top: y + 5,
                left: x + 5,
                border: "1px solid rgb(237,194,64)",
                padding: "2px",
                "background-color": "rgb(237,194,64)",
                opacity: 0.80
        }).appendTo("body").fadeIn(200);
}



function updateAnalyticsButtons()
{
    if( $('*[data-provider="Mixpanel"]').length == 0 && $("#mpButton").length == 0)
         $(".modal-body", "#modal-from-dom").append( $('<input type="button" class="btn" id="mpButton" value="Add Mixpanel" onclick="addMixpanel()" style="margin-right:5px" />') );

    if( $('*[data-provider="Google Analytics"]').length == 0 && $("#gaButton").length == 0 )
        $(".modal-body", "#modal-from-dom").append( $('<input type="button" class="btn" id="gaButton" value="Add Google Analytics" onclick="addGA()" style="margin-right:5px" />') );

}


function addMixpanel()
{
    $("#mpButton").remove();
    $("#analyticsBody").append( $('<tr class="analyticsRow" data-provider="Mixpanel"><td>Mixpanel</td><td><input type="text" size="10" /></td><td><button type="button" class="btn editable-cancel" onclick="$(this).parent().parent().remove();updateAnalyticsButtons();"><i class="icon-remove"></i></button></td></tr>') );

    updateAnalyticsButtons();
}

function addGA()
{
    $("#gaButton").remove();
    $("#analyticsBody").append( $('<tr class="analyticsRow" data-provider="Google Analytics"><td>Google Analytics</td><td><input type="text" size="10" /></td><td><button type="button" class="btn editable-cancel" onclick="$(this).parent().parent().remove();updateAnalyticsButtons();"><i class="icon-remove"></i></button></td></tr>') );

    updateAnalyticsButtons();
}


function onRemoveConnection()
{

    var sourceId = $(this).data('source-id');
    var targetId = $(this).data('target-id');

    removedConnections = {};
    if( confirm("Remove connection from " + config[sourceId].original_asset_filename + " to " + config[targetId].original_asset_filename + "?" ) )
        removeConnectionsFor( sourceId, targetId, true );
}


function getRemoveLink( sourceId, targetId)
{
    return '<a href="javascript:void(0);"><i class="remove-connection icon-remove-sign" data-source-id="' + sourceId + '" data-target-id="' + targetId + '"></i></a>';
}


/**************************************************************/
/* Funnel */
/**************************************************************/
var funnelDisplay = function() {

    var max_children = 0;
    var elementsPerLevel = [];

    var config_data = {}

    var aggregated_ids = {};
    var expanded_ids = {};

    var indexCounter = [0];

    var r = null;

    var connections = {};

    return {
        updateConnections : function() {
            var maxWidth = 1186;
            for( var sourceId in connections )
                for( var targetId in connections[ sourceId ] )
                    maxWidth = Math.max(maxWidth, r.connection( connections[sourceId][targetId] ) );
            $("#funnel svg").attr('width', maxWidth);
            r.safari();
        },

        getConnections : function() {
            return connections;
        },
        raphael : function() {
            return r;
        },
        init : function(cfg, initial) {

            max_children = 0;
            elementsPerLevel = [];
            config_data = cfg;
            connections = {};
            aggregated_ids = {};
            expanded_ids = {};
            indexCounter = [0];
            if( r )
                r.remove();


            if( initial )
                aggregate( initial, 0);

            if( max_children < 3 )
                max_children = 3; // create initial space

            $("#funnel").height( max_children * 200 );
            r = Raphael("funnel", $("#funnel").width(), $("#funnel").height());

            if( initial ) {
                expand( config_data[initial], 0, 0, max_children * 150 );
                $("#funnel svg").attr('width', (indexCounter.length - 1) * 220 + 30);
            }

        },

        addConnection : function( sourceId, targetId, labelStr, color, width, connectionProps ) {

            width = (width/100) * MAX_FUNNEL_LINE_WIDTH;

            var connection = r.connection( $('#tile_' + sourceId), $('#tile_' + targetId), color, color + '|' + width, labelStr, connectionProps );

            if( !connections[sourceId] )
                connections[sourceId] = {};

            connections[sourceId][targetId] = connection;
        },


        removeConnection : function( sourceId, targetId ) {
            var sourcesConnections = connections[sourceId];
            if( !sourcesConnections )
                return;

            var connection = sourcesConnections[targetId];
            if( !connection )
                return;

            connection.bg.remove();
            connection.line.remove();
            delete connections[sourceId][targetId];

            var subId = '#' + sourceId + '-' + targetId;
            $(subId + '-arrow').remove();
            $(subId + '-remove').remove();

            funnelDisplay.updateConnections();
        }
    };


    function choicesHasVideos(video_choices) {

        for( var choice_index in video_choices ) {

            var next_video_id = video_choices[choice_index].next_video_id;

            var obj = config_data[next_video_id];
            if( !obj )
                continue;

            var isVideo = !obj.original_asset_mime_type || obj.original_asset_mime_type.indexOf('video/') == 0;
            if( isVideo )
                return true;
        }

        return false;
    }

    function aggregate( id, level )
    {
        if( aggregated_ids[id] )
            return;
        aggregated_ids[id] = true;

        var obj = config_data[id];
        if( !obj ) {
            console.error("Empty obj for id " + id);
            return;
        }

        if( obj.original_asset_mime_type && obj.original_asset_mime_type == 'ochre/static')
            return;

        if( !obj.id )
            obj.id = id;

        if( obj.id != 'Replay' && !choicesHasVideos(obj.video_choices) && config_data['Replay'] && obj.original_asset_mime_type.indexOf('video/') == 0 )
            obj.video_choices.push( config_data['Replay'] );

        //if( obj.id != 'Finished' && !choicesHasVideos(obj.video_choices) && config_data['Finished'] && obj.original_asset_mime_type.indexOf('video/') == 0)
         //   obj.video_choices.push( config_data['Finished'] );

        // keep track of the number of elements per level, and find the max
        if( level > elementsPerLevel.length - 1 )
            elementsPerLevel.push(1);
        else
            elementsPerLevel[level] += 1;

        if( elementsPerLevel[level] > max_children )
            max_children = elementsPerLevel[level];

        for( var choice_index in obj.video_choices )
            aggregate(obj.video_choices[choice_index].next_video_id, level + 1);
    }


    function expand( obj, level, idx, canvas_size )
    {
        var total_clicks = 0;
        if( obj.clicks )
        {
            for( var c in obj.clicks )
                total_clicks += obj.clicks[c];
        }


        if( $("#tile_" + obj.id).length > 0 )
            return;

        var clipHeight = 120; // max-height of a clipTile

        var tile_height = canvas_size / elementsPerLevel[level];
        var shift_y     = (tile_height * idx );

        var y_position_within_tile = (tile_height / 2) - (clipHeight/2);

        var top  = y_position_within_tile  + shift_y;
        var left = level * 220;

        var isVideo = !obj.original_asset_mime_type || obj.original_asset_mime_type.indexOf('video/') == 0;

        var tile = $("#tileTemplate").tmpl( {   clip:obj,
                                                top:top,
                                                left:left,
                                                clip_name: obj.id.toString(),
                                                num_clicks: total_clicks, isVideo:isVideo,
                                                level:level } );

        tile.appendTo('#funnel');

        if( !window.click_data )
            enableTileTools(tile);

        tile.draggable( draggable_options_clips );
        tile.droppable({
          greedy: true,
          activeClass: "ui-state-active",
          hoverClass: "ui-state-hover",
          drop: function( event, ui ) {
            var oldTileId = $( this ).data('clip-id');
            var oldFilename = $( this ).attr('data-original-asset-filename');
            var newClipTileObj = $(ui.helper);
            var newFilename = newClipTileObj.attr('data-original-asset-filename');
              // check for non video moves
              if (typeof newFilename != "undefined") {


            vex.dialog.confirm({
              message: 'Are you sure you want to replace ' +
                oldFilename + ' with ' + newFilename + '?',
              callback: function(value) {
                if (value) replaceTile(oldTileId, newClipTileObj);
              }
            });
                  } // end check filename
          }
        });


        var next_level = level + 1;
        if( !indexCounter[next_level] )
            indexCounter[next_level] = 0;

        var total_perc = 0;
        var hasVideos = choicesHasVideos( obj.video_choices );
        for( var choice_index in obj.video_choices )
        {
            var choice_entry = obj.video_choices[choice_index];

            var choice_obj = config_data[choice_entry.next_video_id];

            if( !choice_obj )
                continue;

            if( choice_obj.original_asset_mime_type && choice_obj.original_asset_mime_type == 'ochre/static')
                continue;

            if(!expanded_ids[obj.id + '-' + choice_entry.next_video_id] && !expanded_ids[choice_entry.next_video_id + '-' + obj.id] )
            {
                expanded_ids[obj.id + '-' + choice_entry.next_video_id] = true;
                if( $("#tile_" + choice_obj.id).length == 0 )
                {
                    expand( choice_obj, next_level, indexCounter[next_level], canvas_size );
                    indexCounter[next_level]++;
                }
            }

            var rc = 255, g = 204, b = 0, perc = 5;
            var labelStr = null;

            if( choice_obj.id == 'Finished' || (choice_obj.original_asset_mime_type && choice_obj.original_asset_mime_type.indexOf('video/') == 0 )  ) {

                if( obj.clicks )
                    labelStr = '0%';
                else if( !window.click_data )
                    labelStr = getRemoveLink(obj.id, choice_entry.next_video_id);


                if( total_clicks > 0 &&
                    choice_obj.clicks &&
                    choice_obj.clicks[obj.id] &&
                    choice_obj.clicks[obj.id] > 0)
                {
                    perc = ( choice_obj.clicks[obj.id] / total_clicks);

                    g = 255 - parseInt( ( 255 - g) * perc);
                    b = 255 - parseInt( 255 * perc );

                    perc *= 100;
                    perc = Math.ceil(perc);
                    if( perc > 100)
                        perc = 100;
                    total_perc += perc;

                    labelStr = '<span style="font-weight:bold;">' + perc + "%</span>";
                }
                var rgb = "rgb(" + rc + ',' + g + ',' + b + ')';

                if( window.click_data && perc < 7 )
                {
                    rgb = "rgb(255,231,136)";
                    perc = 5;
                }
            }
            else if( window.click_data && choice_obj.original_asset_mime_type.indexOf('ochre/') == 0 ) {

                var num_clicks = ( choice_obj.clicks && choice_obj.clicks[obj.id] > 0 ? choice_obj.clicks[obj.id] : '0');
                labelStr = '<span style="font-weight:bold;">' + num_clicks + ' clicks</span>';
            }
            else if( !window.click_data )
                labelStr = getRemoveLink(obj.id, choice_entry.next_video_id);

            rgb = "rgb(255,229,124)";

            funnelDisplay.addConnection( obj.id, choice_obj.id, labelStr, rgb, perc, { bb1: { index:parseInt(choice_index), total:obj.video_choices.length} } );
        }

        if( hasVideos && obj.clicks && obj.video_choices.length > 0 && total_perc <= 100 )
        {
            var remTop  = tile.height()-10;
            var remLeft = tile.width()+2;

            $('<div class="assetTileRemaining" style="top:' + remTop + 'px;left:' + remLeft + 'px;">(' + (100 - total_perc) + '%)</div>').appendTo(tile);

        }

    }




}();


if( window.Raphael )
{
    Raphael.fn.connection = function (obj1, obj2, line, bg, labelStr, connectionProps ) {
        var scrollOffsetY = $("#funnel").scrollTop();
        var scrollOffsetX = $("#funnel").scrollLeft();
    try {
        if (obj1.line && obj1.from && obj1.to) {
            line = obj1;
            obj1 = line.from;
            obj2 = line.to;
            connectionProps = line.connectionProps;
        }
        var bb1 = { x : obj1.position().left + scrollOffsetX, y : obj1.position().top + scrollOffsetY, width: obj1.width(), height : obj1.height() },
            bb2 = { x : obj2.position().left + scrollOffsetX, y : obj2.position().top + scrollOffsetY, width: obj2.width(), height : obj2.height() },
            outgoingY = parseInt( bb1.height * ( (connectionProps.bb1.index+1) / ( connectionProps.bb1.total + 1 ) ) ),
            p = [

        /* 0 */   {x: bb1.x + bb1.width / 2, y: bb1.y - 1},

        /* 1 */   {x: bb1.x + bb1.width / 2, y: bb1.y + bb1.height + 1},

        /* 2 */   {x: bb1.x - 1, y: bb1.y + bb1.height / 2},

        /* 3 */   {x: bb1.x + bb1.width + 1, y: bb1.y + outgoingY},

        /* 4 */   {x: bb2.x + bb2.width / 2, y: bb2.y - 1},

        /* 5 */   {x: bb2.x + bb2.width / 2, y: bb2.y + bb2.height + 1},

        /* 6 */   {x: bb2.x - 1, y: bb2.y + bb2.height / 2},

        /* 7 */    {x: bb2.x + bb2.width + 1, y: bb2.y + bb2.height / 2}],

            d = {}, dis = [];
        for (var i = 0; i < 4; i++) {
            for (var j = 4; j < 8; j++) {
                var dx = Math.abs(p[i].x - p[j].x),
                    dy = Math.abs(p[i].y - p[j].y);
                if ((i == j - 4) || (((i != 3 && j != 6) || p[i].x < p[j].x) && ((i != 2 && j != 7) || p[i].x > p[j].x) && ((i != 0 && j != 5) || p[i].y > p[j].y) && ((i != 1 && j != 4) || p[i].y < p[j].y))) {
                    dis.push(dx + dy);
                    d[dis[dis.length - 1]] = [i, j];
                }
            }
        }
        if (dis.length == 0) {
            var res = [0, 4];
        } else {
            res = d[Math.min.apply(Math, dis)];
        }
        var x1 = p[res[0]].x,
            y1 = p[res[0]].y,
            x4 = p[res[1]].x,
            y4 = p[res[1]].y;
        dx = Math.max(Math.abs(x1 - x4) / 1.5, 10); // denominator controls curviness
        dy = Math.max(Math.abs(y1 - y4) / 1.5, 10); // denominator controls curviness
        var x2 = [x1, x1, x1 - dx, x1 + dx][res[0]].toFixed(3),
            y2 = [y1 - dy, y1 + dy, y1, y1][res[0]].toFixed(3),
            x3 = [0, 0, 0, 0, x4, x4, x4 - dx, x4 + dx][res[1]].toFixed(3),
            y3 = [0, 0, 0, 0, y1 + dy, y1 - dy, y4, y4][res[1]].toFixed(3);


        var pathId = obj1.attr('data-clip-id') + '-' + obj2.attr('data-clip-id');
        var arrowClass, arrowX, arrowY, labelX, labelY;

        if( x4 < bb2.x ) {
            arrowClass = ''; arrowX = -11; arrowY = -10;
        }

        if( x4 > ( bb2.x + obj2.width() ) ) {
            arrowClass = 'icon-chevron-left'; arrowX = -3; arrowY = -10;
        }

        if( y4 < bb2.y ) {
            arrowClass = 'icon-chevron-down'; arrowX = -11; arrowY = -14;
        }

        if( y4 > ( bb2.y + obj2.height() ) ) {
            arrowClass = 'icon-chevron-up'; arrowX = -11; arrowY = -3;
        }



        if( x1 < bb1.x ) {
            labelX = -15; labelY = -10;         /* left */
        }

        if( x1 > ( bb1.x + obj1.width() ) ) {
            labelX = 2; labelY = -10;          /* right */
        }

        if( y1 < bb1.y ) {
            labelX = -11; labelY = -14;         /* top */
        }

        if( y1 > ( bb1.y + obj1.height() ) ) {
            labelX = -11; labelY = -3;          /* bottom */
        }

        var path = ["M", x1.toFixed(3), y1.toFixed(3), "C", x2, y2, x3, y3, x4.toFixed(3), y4.toFixed(3)].join(",");
        if (line && line.line) {
            line.bg && line.bg.attr({path: path});
            line.line.attr({path: path});

            var o = $('#' + pathId + '-remove');
            o.css('top', (y1+labelY) + 'px');
            o.css('left',(x1+labelX) + 'px');

            var a = $('#' + pathId + '-arrow');
            a.css('top', parseInt(y4+arrowY) + 'px');
            a.css('left', parseInt(x4+arrowX) + 'px');

            var arrowElement = $('i', a );
            if( arrowElement.attr('class') != arrowClass )
                arrowElement.attr('class', arrowClass );

            return Math.max(bb1.x + bb1.width, bb2.x + bb2.width) + 100;

        } else {
            var color = typeof line == "string" ? line : "#000";
            var linepath = this.path(path);
            var linebg = bg && bg.split && this.path(path).attr({stroke: bg.split("|")[0], fill: "none", "stroke-width": bg.split("|")[1] || 3});

            var f = $('#funnel');
            if( labelStr )
                f.append( getPathContent( pathId + '-remove', x1+labelX, y1+labelY, labelStr ) );

            f.append( getPathContent( pathId + '-arrow', x4+arrowX, y4+arrowY, '<i class="' + arrowClass + '" ></i>' ));

            return {
                bg: linebg,
                line: linepath.attr({stroke: color, fill: "none"}),
                from: obj1,
                to: obj2,
                path:path,
                connectionProps:connectionProps
            };
        }
    }
    catch(err)
    {
        console.error("Raphael.fn.connection: " + err);
    }
    };

}


function getPathContent(pathId, x1, y1, content)
{
    //console.log(pathId, x1, y1, content);
    var str = '<div id="' + pathId + '" style="position:absolute;top:' + parseInt(y1) + 'px;left:' + parseInt(x1) + 'px;z-index:20;">' + content + '</div>';
    return $(str);
}


function getThumbnailForId( id )
{
    if( config[id] && config[id].original_asset_thumbnail )
        return UPLOAD_URL + '/' + config[id].original_asset_thumbnail;

    var thumb = $('#tile_' + id).data('original-asset-thumbnail');
    if( !thumb )
        return UPLOAD_URL + '/advntr/unknown_thumbnail.png';

    return UPLOAD_URL + '/' + thumb;
}

function getAssetMimeTypeBG( mime_type )
{
    if( mime_type.indexOf('video/') == 0 )
        return '#333333';
    return '#ffffff';
}

function getFilenameForId( id )
{
    var obj = config[id];
    if( !obj )
    {
        console.error("Missing obj for id " + id);
        return '';
    }

    if( obj.hasOwnProperty('original_asset_filename') )
        return obj.original_asset_filename;
    else
        return id;
}

function enableTileTools(tile)
{
    if( MOBILE )
    {
        $('.clipTileInfo', tile).show();
        $('.clipTileInfoBg', tile).show();
        return;
    }

    $(tile).hover(
        function() {
            $('.clipTileInfo', this).show();
            $('.clipTileInfoBg', this).css( 'width', ( $('.clipTileInfo', this).width() + 4 ) + 'px' ).show();
        },
        function() {
            $('.clipTileInfoBg', this).hide();
            $('.clipTileInfo', this).hide();


        }
    );
}

var transparencies = [
    { text : 'Fully transparent', value: '0' },
    { text : '.1', value: '.1' },
    { text : '.2', value: '.2' },
    { text : '.3', value: '.3' },
    { text : '.4', value: '.4' },
    { text : '.5', value: '.5' },
    { text : '.6', value: '.6' },
    { text : '.7', value: '.7' },
    { text : '.8', value: '.8' },
    { text : '.9', value: '.9' },
    { text : 'No transparency', value: '1' }
];



var switchings = [
    { text : 'Seamless', value: '0' },
    { text : 'Instant Switch', value: '1' },
    { text : 'Wait', value: '4' }
];

// for clips already placed on the canvas
var draggable_options_clips = {
    drag: function(e) {
        //$("#funnel svg").attr('width', $("#funnel").get(0).scrollWidth);
        funnelDisplay.updateConnections();
    }
    //, containment: "#dropZone"
};

// for clips in the assets holder area
var draggable_options_assets = {
    start : function() {
        $("#funnel").css('overflow-x', 'hidden');
        $("#dropZone").addClass('dropZoneEnabled');
    },

    stop : function(e, b) {
        $("#funnel").css('overflow-x', 'scroll');
        $("#dropZone").removeClass('dropZoneEnabled');
    },
    helper: "clone"
};

function saveAnalytics()
{
    var saved = [];
    var bad = false;
    $(".analyticsRow").each( function() {

        var provider = $(this).data('provider');
        var value    = $('input', this ).val();
        if( !bad && value.trim() == '' )
        {
            alertModal('Please enter an identifier for all values.');
            bad = true;
            return;
        }

        saved.push( { 'provider' : provider, 'id' : value } );
    });

    if( bad )
        return;

    config['analytics'] = saved;
    updateAnalytics();
    $('#modal-from-dom').modal('hide');
}


function findAsset(obj, filePath)
{
    for( k in obj )
    {
        var o = obj[k]
        if (typeof o == "object" )
        {
            if( findAsset(o, filePath ) )
                return true;
        }

        if( o == filePath )
            return true;
    }

    return false;
}

function deleteAsset(assetId, filePath, fileId)
{

    if( findAsset( config, filePath) )
    {
        alertModal(fileId + ' is being used in your project. Please remove it and try again.');
        return;
    }


    var $el = $('#upload_asset_' + fileId);
    if( !confirm('Are you sure you want to delete ' + $el.data('original-asset-filename') + '?' ) )
        return;

    // remove from imageAssets and videoAssets
    for( i in imageAssets )
        if( imageAssets[i].id == filePath )
            imageAssets.splice(i, 1);

    for( i in videoAssets )
        if( videoAssets[i].id == filePath )
            videoAssets.splice(i, 1);

    addSpinner($el);

    $.ajax({
        dataType: "json",
        type:"POST",
        url: '/accounts/delete_asset/',
        data: {'asset_id' : assetId },
        error : function(jqXHR, textStatus, err) {
            console.error("init: " + textStatus + ": " + err);
            alertModal('There was an error deleting ' + $el.data('original-asset-filename') + ' and we are looking into it. Sorry for the inconvenience.');
            removeSpinner($el);
        },
        success: function(data, textStatus, jqXHR) {
            $el.fadeOut('slow', function() { this.remove(); } );
        }

    });
}


function openAssets()
{
    $("#editPane").removeClass('span12').addClass('span9').addClass('editPaneBorder');
    $("#assets").show();
    $('.icon-backward', '#assetsButton').removeClass('icon-backward').addClass('icon-forward');
}

function closeAssets()
{
    $("#assets").hide();
    $("#editPane").removeClass('span9').removeClass('editPaneBorder').addClass('span12');
    $('.icon-forward', '#assetsButton').removeClass('icon-forward').addClass('icon-backward');
}


function getTextVal( id, def )
{
    var el = $('#' + id);
    if( !def )
        def = '';

    if( el.length == 0 )
        return def;

    var obj = $('#' + id).editable('getValue');
    if( obj == null || obj == undefined || obj[id] == null || obj[id] == undefined )
        return def;

    var val = obj[id].toString().trim()

    if( val.length == 0 || val == 'Empty' || val == 'Edit')
        return def;

    return val;

}


function getTextVal2( el )
{
    if( el.length == 0 )
        return '';

    var obj = el.editable('getValue');
    if( !obj )
        return '';

    var val = obj['undefined'].toString().trim()

    if( val == 'Empty' || val == 'Edit')
        return '';
    return val;

}


function autoPlace(tileId, vertical, useChoiceId)
{
    var item = config[tileId];


    var poster = $('#posterImage');
    var width = poster.width();
    var height = poster.height();

    var numChoices = $('.choice-props').length;
    var tile_width  = width  / numChoices;
    var tile_height = height / numChoices;

    $('.choice-props').each( function(i)
    {
        var choiceId = $(this).data('clip-id');

        if( useChoiceId && useChoiceId != choiceId )
            return;

        var choiceImg = $("#choice-img-" + choiceId);
        var left = 0, top = 0;

        if( !vertical )
        {
            var shift_x = (tile_width * i );
            var x_position_within_tile = (tile_width/2) - ( choiceImg.width() / 2 );

            left = shift_x + x_position_within_tile;
            top  = (height / 2) - ( choiceImg.height() / 2);
            
            if(choiceImg.height() == 0) {
            	top = top - 50;
            }
            left = left - 35;
        }
        else
        {
            var shift_y = (tile_height * i );
            var y_position_within_tile = (tile_height/2) - ( choiceImg.height() / 2 );

            top  = shift_y + y_position_within_tile;
            left = (width / 2) - ( choiceImg.width() / 2);
            if(choiceImg.width() == 0){
            	left = left - 35;
            }
            top= top - 50;
        }

        var choiceProps = $("#choice-props-" + choiceId);
        $('.choice-top',  choiceProps).editable('setValue', parseInt(top/rW));
        $('.choice-left', choiceProps).editable('setValue', parseInt(left/rW));

        updateImgPosition( choiceImg, top, left, 1);
    });
}



function clone(obj) {
    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;

    // Handle Date
    if (obj instanceof Date) {
        var copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        var copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        var copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
        }
        return copy;
    }

    console.error("Unable to copy obj! Its type isn't supported.");
}

var cleanedIds = undefined;
var nonOrphans = {};
function cleanConfig(cfg, obj)
{
    if( cleanedIds == undefined )
        cleanedIds = {};

    if( obj.hasOwnProperty('clicks') )
        delete obj['clicks'];

    for( var i = 0; i < obj.video_choices.length; i++)
    {
        // don't get caught in a loop
        var next_id = obj.video_choices[i].next_video_id;
        nonOrphans[next_id] = true;
        if( cleanedIds[next_id] )
            continue;
        cleanedIds[next_id] = true;

        cleanConfig( cfg, cfg[ next_id ] )
    }
}

function renderConfigObj(initial)
{
    var newconfig = clone(config);

    if( newconfig['Replay'] )
        delete newconfig['Replay'];

    if( newconfig['Finished'] )
        delete newconfig['Finished'];

    if( newconfig.options.initial )
        cleanConfig(newconfig, newconfig[newconfig.options.initial] )

    if( initial )
        newconfig.options.initial = initial;
    cleanedIds = {};

    // find orphans (nonOrphans filled from cleanConfig)
    for( var id in newconfig )
    {
        if( id == newconfig.options.initial )
            continue;

        if( newconfig[id].video_choices && !nonOrphans[id] )
            delete newconfig[id];
    }
    nonOrphans = {};

    newconfig.name                  = getTextVal("config-name");
    if( newconfig.options.watermark && $('#edit-watermark').data('src'))
        newconfig.options.watermark.src = $('#edit-watermark').data('src');
    newconfig.splash.src            = getTextVal("config-splash-src");
    newconfig.options.altMp4        = getTextVal("config-options-altMp4");
    newconfig.options.volume        = getTextVal("config-options-volume");
    newconfig.options.clickThrough  = getTextVal("config-options-clickThrough");
    newconfig.share.image           = newconfig.splash.src;
    newconfig.share.link            = getTextVal("config-share-link");
    newconfig.campaign.ad_id        = getTextVal("config-campaign-ad_id");
    newconfig.share.fb_msg          = newconfig.share.twitter_msg = getTextVal("config-share-message");

    return newconfig;
}

function renderConfig(initial)
{
    var newconfig = renderConfigObj(initial);

    return JSON.stringify(newconfig)
}

function logError(msg, err)
{
    console.error("*** OCHRE ERROR: " + msg + ' campaign=' + CAMPAIGN_ID + ' exception=' + err + ' callstack=' + err.stack);
    if( J_DEBUG )
        alert(msg);
}

function showSaveDialog(msg)
{
    if( msg )
        $('#save-msg').text(msg);
    else
        $('#save-msg').text('Saving...');
    $('#save-modal').bind('show', function() { }).modal({ backdrop: 'static', keyboard: false });
}

function padNum(num)
{
    if( num < 10 )
        return ('0' + num);
    return num;
}

function hideSaveDialog()
{
    $('#save-modal').modal('hide');
}


function update_mixpanel( method, data )
{
    if(window.mixpanel && window.mixpanel.track)
        mixpanel.track(method, data);
}

function saveConfig()
{
    try {

        showSaveDialog();
        var rendered = renderConfig();

        update_mixpanel('Save', { campaign_id:CAMPAIGN_ID});

        $("#configData").val( rendered );
        $("#dataForm").submit();
    }
    catch(err)
    {
        logError("Error saving campaign", err);
        hideSaveDialog();
    }
}


function updateAnalytics()
{
    var str = "Adventr Analytics";
    for( var i = 0; i < config.analytics.length;i++)
        str += ', ' + config.analytics[i].provider;

    $("#edit-analytics").text(str);
}


function removeClass(jObj, startswith)
{
    var classes = jObj[0].className.split(/\s+/);
    for( var i in classes)
    {
        var cls = classes[i];
        if( cls.indexOf(startswith) == 0 )
            return jObj.removeClass(cls);
    }

    return jObj;
}


function saveTile(tileId)
{
    if( !config[tileId] )
        config[tileId] = {};

    if( config[tileId].original_asset_mime_type.indexOf('ochre/') == 0 )
    {
        var url = getTextVal('item_url');
        if( url.indexOf('http') != 0 )
            url = 'http://' + url;


        if( config[tileId].original_asset_mime_type == 'ochre/twitter' )
        {
            config[tileId]['share_url'] = url;
            config[tileId]['share_msg'] = getTextVal('item_share_msg');
            var msg = encodeURIComponent( config[tileId]['share_msg'] + " " + url );
            url = "https://twitter.com/intent/tweet?source=webclient&text=" + msg;

        }
        else if( config[tileId].original_asset_mime_type == 'ochre/facebook')
        {
            config[tileId]['share_url'] = url;
            config[tileId]['share_msg'] = getTextVal('item_share_msg');
            var next = encodeURIComponent("https://video.theochre.com/fb_share/?campaignId=" + CAMPAIGN_ID);
            var desc = encodeURIComponent(config[tileId]['share_msg']);
            var link = encodeURIComponent(url);
            url = "https://www.facebook.com/dialog/feed?app_id=444928435575766&description=" + desc + "&display=popup&e2e=%7B%7D&link=" + link + "&locale=en_US&next=" + next + "&sdk=joey&t=" + desc + "&u=" + link;
        }

        config[tileId]['url'] = url;

    }
    else
    {

        config[tileId]['choices_time'] = parseFloat(getTextVal('item_choices_time', '0')) * 1000;
        config[tileId]['instantSwitch'] = parseInt(getTextVal('item_switching'));
        config[tileId]['share_msg'] = getTextVal('item_share_msg');


        // keep track of previous choices to delete later
        var prevChoices = [];
        for( var i in config[tileId]['video_choices'])
            prevChoices[config[tileId]['video_choices'][i].next_video_id] = true;


        if(config.options.initial != tileId && $('#item_initial').is(':checked'))
            setInitial(tileId, true);

        var newChoices = [];

        // gather choice data
        $('.choice-props').each( function(index) {
            var clipId = $(this).attr('data-clip-id');

            var choiceTop    = $('.choice-top',  this);
            var choiceLeft   = $('.choice-left', this);
            var choiceTrans  = $('.choice-transparency', this);
            var choiceText   = $(".choice-text", this);
            var choiceWidth  = $(".choice-width", this);
            var choiceHeight = $(".choice-height", this);
            var imgSelector  = $(".img-selector", this);
            var imgSelectorHover  = $(".img-selector-hover", this);

            var choice      = { next_video_id : clipId };
            choice['top']   = getTextVal2(choiceTop);
            choice['left']  = getTextVal2(choiceLeft);
            choice['alpha'] = getTextVal2(choiceTrans);

            var choice_type = $("input:radio[name='choice_type_" + clipId + "']:checked").val();
            var imgVal = getTextVal2(imgSelector);
            if( choice_type == 'image' && imgVal )
            {
                choice['image'] = imgVal;

                var hoverVal = getTextVal2(imgSelectorHover);
                if( hoverVal && hoverVal.trim().length > 0 )
                    choice['image2'] = hoverVal;
            }
            else // text
            {
                choice['label']  = getTextVal2(choiceText);
                choice['width']  = parseInt(getTextVal2(choiceWidth));
                choice['height'] = parseInt(getTextVal2(choiceHeight));
            }

            newChoices.push( choice );

            delete prevChoices[clipId];
        });

        // cleanup deleted choices
        for( var k in prevChoices )
            removeChoice(k);

        config[tileId]['video_choices'] = newChoices;
    }

    $('#edit-modal').modal('hide');
}


function editWatermark()
{
    $('#watermark-modal').empty();

    $("#watermarkTemplate").tmpl( {
                                config:config,
                                UPLOAD_URL : UPLOAD_URL
                             }).appendTo("#watermark-modal");

    var selectImg   = $('#config-options-watermark-src');
    var selectTop   = $('#config-options-watermark-top');
    var selectLeft  = $('#config-options-watermark-left');
    var selectWidth   = $('#config-options-watermark-width');
    var selectHeight  = $('#config-options-watermark-height');
    var selectTrans = $('#config-options-watermark-transparency');

    var poster       = $('#watermarkPoster');
    var watermarkImg = $('#watermarkImage');

    selectImg.editable({ value : ( config.options.watermark.src ? config.options.watermark.src : 'Edit'), source: imageAssets, select2 : select2options });
    selectTop.editable( { value: config.options.watermark.top,  validate : function( value) { return validatePos( poster, value, 'original-height'); } } );
    selectLeft.editable({ value: config.options.watermark.left, validate : function( value) { return validatePos( poster, value, 'original-width'); } } );
    selectTrans.editable({ value : ( config.options.watermark.alpha ? config.options.watermark.alpha : '1'), source: transparencies });


    var updateWatermark = function() {

        var src = getTextVal('config-options-watermark-src');
        if( !src )
            return;

        var top  = getTextVal('config-options-watermark-top');
        var left = getTextVal('config-options-watermark-left');


        var alpha = getTextVal('config-options-watermark-transparency');
        var newAlpha = parseFloat(alpha) * 10;

        watermarkImg.css('top',  (top  * rW) + 'px');
        watermarkImg.css('left', (left * rW) + 'px');

        var useWidth = config.options.watermark.width;
        var wval = getTextVal('config-options-watermark-width');
        if( wval )
            useWidth = parseInt(wval);

        if( useWidth )
        {
            $('img', watermarkImg).css('width', (useWidth * rW) + 'px');
            $(watermarkImg).css('width', (useWidth * rW) + 'px');
        }


        var useHeight = config.options.watermark.height
        var hval = getTextVal('config-options-watermark-height');
        if( hval )
            useHeight = parseInt(hval);

        if( useHeight )
        {
            $('img', watermarkImg).css('height', (useHeight * rW) + 'px');
            $(watermarkImg).css('height', (useHeight * rW) + 'px');
        }

        removeClass($('img', watermarkImg), 'transparent').addClass( 'transparent' + newAlpha);

        var img = $('img', watermarkImg);

        var loadWM = function(e, t, f)
        {
            $("#watermarkStage").show();
            //console.log("loadWM " + rW);

            if( rW == null )
            {
                if( f === undefined )
                    f = 20;
                else if( f < 0 )
                    return;
                f--;

                t = e.target;
                setTimeout( function() { loadWM(e, t, f); }, 400 );
                return;
            }

            if( !t )
                t = this;

            var im = $(t);
            var w = ( t.naturalWidth  != undefined ? t.naturalWidth :  t.width);
            var h = ( t.naturalHeight != undefined ? t.naturalHeight : t.height);
            im.data('original-width',  w).data('original-height', h);

            im.css('width', (w * rW) + 'px');
            im.css('height', (h * rW) + 'px');


            if( !config.options.watermark.width)
                config.options.watermark.width = w;

            if( !config.options.watermark.height)
                config.options.watermark.height = h;

            if( config.options.watermark.height != h || config.options.watermark.width != w )
                $('#watermarkReset').show();

            selectWidth.editable( { value: config.options.watermark.width   } );
            selectHeight.editable({ value: config.options.watermark.height } );

            $('img', watermarkImage).css('width',  (config.options.watermark.width * rW) + 'px');
            $('img', watermarkImage).css('height', (config.options.watermark.height * rW) + 'px');
        }

        img.load( loadWM );
        img.attr('src', UPLOAD_URL + '/' + src);
        img.show();

        watermarkImg.hover( function() {
            $('div', this).show();
        }, function() {
            $('div', this).hide();
        });

        watermarkImg.Resizable(
                {
                        minWidth: 3,
                        minHeight: 3,
                        maxWidth: 1200, // should be width of cake
                        maxHeight: 1200, // height of cake
                        minTop: 0,
                        minLeft: 0,
                        maxRight: 700, // width of cake
                        maxBottom: 500, //height of cake
                        dragHandle: false,

                        onDragStart: function() { },
                        onDrag: function(x, y) { },

                        handlers: {
                                se: '.resizeSE',
                                e:  '.resizeE' ,
                                ne: '.resizeNE',
                                n:  '.resizeN' ,
                                nw: '.resizeNW',
                                w:  '.resizeW' ,
                                sw: '.resizeSW',
                                s:  '.resizeS'
                        },

                        onResize :

                            function(sWidth, sHeight, sTop, sLeft, wImg, wReset) {

                            return function(size, position) {

                                var topVal = parseInt(wImg.css('top'));
                                var leftVal = parseInt(wImg.css('left'));

                                if( topVal < 0 || leftVal < 0 )
                                    return;


                                var img = $('img', this);

                                img.css('width', size.width + 'px');
                                img.css('height', size.height + 'px');


                                sWidth.editable('setValue', parseInt(size.width/rW));
                                sHeight.editable('setValue', parseInt(size.height/rW));

                                wReset.show();

                                sTop.editable('setValue',  parseInt(topVal/rW));
                                sLeft.editable('setValue',  parseInt(leftVal/rW));
                            };

                            }(selectWidth, selectHeight, selectTop, selectLeft, watermarkImg, $('#watermarkReset') ),
                }
        );
    };

    watermarkImg.draggable({

        drag: function(cTop, cLeft) {
                return function(e) {
                    var t = parseInt(this.offsetTop/rW);
                    var l = parseInt(this.offsetLeft/rW);
                    if( t < 0 ) t = 0;
                    if( l < 0 ) l = 0;

                    cTop.editable('setValue', t);
                    cLeft.editable('setValue', l);
                };
        }(selectTop, selectLeft ),
        containment : 'parent'
    });

    selectImg.on('save', function() { setTimeout( updateWatermark, 10); });
    selectTop.on('save', function() { setTimeout( updateWatermark, 10); });
    selectLeft.on('save', function() { setTimeout( updateWatermark, 10); });
    selectTrans.on('save', function() { setTimeout( updateWatermark, 10); });

    selectWidth.on('save', function() { setTimeout( updateWatermark, 10); });
    selectHeight.on('save', function() { setTimeout( updateWatermark, 10); });

    $('#watermarkReset').click( function() {

        var selectWidth   = $('#config-options-watermark-width');
        var selectHeight  = $('#config-options-watermark-height');

        var im = $('img', '#watermarkImage');
        selectWidth.editable( 'setValue', im.data('original-width') );
        selectHeight.editable('setValue', im.data('original-height') );

        updateWatermark();

        $(this).hide();
    } );

    if( config.options.initial )
    {
        poster.load( function() {
            $("#watermarkStage").show();

            var maxWidth = parseInt($(this).css('max-width'));
            var w = ( this.naturalWidth  != undefined ? this.naturalWidth :  this.width);
            var h = ( this.naturalHeight != undefined ? this.naturalHeight : this.height);
            $(poster).data('original-width', w).data('original-height', h);

            rW = ( w >= maxWidth ? ( maxWidth / w ) : ( w / maxWidth ) );
            updateWatermark();
        });

        var imgSrc = UPLOAD_URL + '/' + OUTPUT_PATH + '/thumbnails/' + config.options.initial + '_poster_0000.png';
        poster.attr('src', imgSrc);
    }

    $("#watermarkStage").hover(
        function() { $('.choiceImg').removeClass('choiceBorderOn').addClass('choiceBorderOn'); },
        function() { $('.choiceImg').removeClass('choiceBorderOn').removeClass('choiceBorderOff'); }
    );

    $('#watermark-modal').bind('show', function() {
        $(this).css({'margin-left': -($(this).width())/2});
    }).modal({ backdrop: true });
}

function validatePos( el, value, wT )
{
    if( value < 0 )
        return "Please enter a number > 0";

    var max = parseInt(el.data(wT))
    if( value > max )
        return "Please enter a number <= " + max;
}

function updateImgPosition( obj, top, left, ratio )
{
    var x = parseInt( left * ratio);
    var y = parseInt( top * ratio);

    obj.css('left', x + 'px');
    obj.css('top', y + 'px');
    //
    //
    //obj.setAttribute('data-x', x);
    //obj.setAttribute('data-y', y);
    //
    //console.log(obj);
    //console.log(this.obj, x, y);
}


function loadChoiceImage(e, t)
{
    if( rW == null )
    {
        t = e.target;
        setTimeout( function() { loadChoiceImage(e, t); }, 100 );
        return;
    }

    if( !t )
        t = this;

    var im = $(t);
    var w = ( t.naturalWidth  != undefined ? t.naturalWidth :  t.width);
    var h = ( t.naturalHeight != undefined ? t.naturalHeight : t.height);
    im.data('original-width',  w).data('original-height', h);

    im.css('width', (w * rW) + 'px');
    im.css('height', (h * rW) + 'px');

    //im.setAttribute('data-x', (w * rW));
    //im.setAttribute('data-y', (h * rW));

    var clipId = im.attr('data-clip-id');

    var choiceProps = $("#choice-props-" + clipId);
    var choiceTop   = $('.choice-top',  choiceProps);
    var choiceLeft  = $('.choice-left', choiceProps);
    var choiceTrans = $('.choice-transparency', choiceProps);

    //console.log("choiceTrans: ",choiceTrans);

    $('div', im).addClass('transparent' + (parseFloat(getTextVal2(choiceTrans)) * 10));

    //console.log("'transparent' + (parseFloat(getTextVal2(choiceTrans)) * 10): ", 'transparent' + (parseFloat(getTextVal2(choiceTrans)) * 10));
    updateImgPosition( im.parent(), parseInt(getTextVal2(choiceTop)), parseInt(getTextVal2(choiceLeft)), rW);

    if( config[clipId] && config[clipId].original_asset_mime_type == 'ochre/static' )
        $('.slow-images', '#choice-' + clipId).attr('src', $(t).attr('src'));
}


function saveWatermark()
{
    config.options.watermark.src   = getTextVal('config-options-watermark-src');
    config.options.watermark.top   = getTextVal('config-options-watermark-top');
    config.options.watermark.left  = getTextVal('config-options-watermark-left');
    config.options.watermark.width   = getTextVal('config-options-watermark-width');
    config.options.watermark.height  = getTextVal('config-options-watermark-height');
    config.options.watermark.alpha = getTextVal('config-options-watermark-transparency');

    $('#edit-watermark').text($('#config-options-watermark-src').text());
    $('#edit-watermark').data('src', config.options.watermark.src);
    $('#watermark-modal').modal('hide');
}




function removeConnectionsFor( sourceId, tileId, once )
{
    if( removedConnections[sourceId + '-' + tileId] )
        return;
    removedConnections[sourceId + '-' + tileId]  = true;

    var obj = config[sourceId];
    if( !obj )
    {
        console.error("ERROR: Expected obj for sourceId=" + sourceId);
        return;
    }

    for( var i = 0; i < obj.video_choices.length; i++)
    {
        var choice = obj.video_choices[i];
        if( choice.next_video_id == tileId )
        {
            funnelDisplay.removeConnection( sourceId, choice.next_video_id );
            obj.video_choices.splice(i, 1);
            i=-1;
        }

        if( !once )
            removeConnectionsFor( choice.next_video_id, tileId );
    }
}


function loadPosterForSize()
{
    if( !config.options.initial )
        return;

    if( config.width > 0 )
        return;

    $('#posterLoader').load( function(e) {

        var im = $(this);
        var w = ( this.naturalWidth  != undefined ? this.naturalWidth :  this.width);
        var h = ( this.naturalHeight != undefined ? this.naturalHeight : this.height);

        config.width = parseInt(w);
        config.height = parseInt(h);

        console.log("Set new width=" + config.width + " height=" + config.height + " from " + config.options.initial);
    } );

    var item = config[config.options.initial];

    var usePath = OUTPUT_PATH;
    if( item.hasOwnProperty('original_output_path') )
        usePath = item.original_output_path;

    var useId = item.id
    if( item.hasOwnProperty('original_asset_id') )
        useId = item.original_asset_id;

    $('#posterLoader').attr('src',  UPLOAD_URL + '/' + usePath + '/thumbnails/' + useId + '_poster_0000.png');
}


function setInitial(tileId, resetDimensions)
{
    if( !config[tileId] )
        return;

    $('#icon_initial').remove();

    if( resetDimensions )
        config.width = config.height = 0;

    removeConnectionsFor( config.options.initial, tileId );

    config.options.initial = tileId;

    loadPosterForSize();

    var obj = $('#tile_' + tileId);

    var top = (obj.height() / 2) - (15);
    var left = (obj.width() / 2) - (15);
    obj.append( $('<div id="icon_initial" style="position:absolute;top:' + top + 'px;left:' + left + 'px;"><img src="' + STATIC_URL + 'advntr/assets/ochre_play_big.png" style="width:30px;" /></div>') );
}

function findAndSetInitial( tileId )
{
    var clip = config[tileId];
    for( var i in clip.video_choices )
    {
        var choice = clip.video_choices[i];
        var choiceObj = config[ choice.next_video_id ];
        if( choiceObj && choiceObj.original_asset_mime_type.indexOf('video/') == 0 )
            return setInitial( choice.next_video_id, true );
    }

    // find any video and make it initial
    for( var k in config )
    {
        if( k != tileId && config[k].original_asset_mime_type && config[k].original_asset_mime_type.indexOf('video/') == 0 )
            return setInitial( k, true );
    }
}

function alertModal(msg) {
    $('#alert-modal').find('.modal-body').html(msg);
    $('#alert-modal').bind('show', function() { }).modal({ backdrop: true });


}

function confirmPublish()
{
    if( CAMPAIGN_ID == 0 )
        return alertModal('You must save your project first before publishing');

    if( !config.options.initial )
        return alertModal('You haven\'t added any videos to your project yet');

    $('#publish-modal').bind('show', function() { }).modal({ backdrop: true });
}

function downloadClip(path)
{
    window.open( UPLOAD_URL + '/' + path, '_blank');
    update_mixpanel('Download', { campaign_id:CAMPAIGN_ID, file:path});
}


function removeEditChoice(tileId)
{
    if( confirm('Remove ' + tileId) )
    {
        $('#choice-' + tileId).remove();
        $('#choice-img-' + tileId).remove();
    }
}

function confirmAndRemoveChoice(tileId)
{
    var clip = config[tileId];
    if( !clip )
        return;

    if( !confirm('Remove ' + clip.original_asset_filename + '?') )
        return;

    removeChoice(tileId);
}

function removeChoice(tileId)
{

    if( tileId == config.options.initial )
    {
        config.options.initial = null;
        try {
            findAndSetInitial( tileId );
        }
        catch(err)
        {
            console.error('Error removing choice ' + tileId + ' err=' + err);
            config.options.initial = tileId;
            return;
        }

        if( !config.options.initial )
            addDropHelp();
    }

    removedConnections = {};
    if( config.options.initial )
        removeConnectionsFor( config.options.initial, tileId );

    var clip = config[tileId];
    for( var i = 0; i < clip.video_choices.length; i++ )
        funnelDisplay.removeConnection( tileId, clip.video_choices[i].next_video_id );

    $("#tile_" + tileId).fadeOut('slow', function() { this.remove() } );

    delete config[tileId];
}


function disableConnectingAction()
{
    $('.clipTile').removeClass('clipTileEnabled');
    connectionsEnabledFor = null;

    return false;
}

function enableConnections( tileId )
{
    if( connectionsEnabledFor )
        return disableConnectingAction();

    $(".clipTile", "#funnel").each( function() {
        var thisId = $(this).attr('data-clip-id');
        if( thisId == tileId || thisId == config.options.initial )
            return;

        for( var i = 0; i < config[tileId].video_choices.length; i++ )
            if( config[tileId].video_choices[i].next_video_id == thisId )
                return;

        $(this).addClass('clipTileEnabled');
    });


    connectionsEnabledFor = tileId;
    enablingConnections = true;

    return false;
}

function genTileId(tileId)
{
    // find existing elements with this tileId, add +1 if found
    var found = 0;
    while( config[tileId] )
        tileId = tileId + '-' + (found+1);

    return tileId;
}

function addNonChoice(tileId)
{
    var targetId = genTileId(tileId);

    var choice = {
        next_video_id : targetId,
        top:0, left:0, width: 96, height:57, label: "Text",
    };

    // add to config
    config[targetId] = {
        "id": targetId,
        "url": "",
        "original_asset_mime_type": "ochre/static",
        "original_asset_thumbnail": "",
        "video_choices": [],
        "mp4": "",
        "instantSwitch": 1,
        "flv": "",
        "original_asset": ""
    };

    addChoice( choice, tileId );

    $('#choice-container').animate({scrollTop:$('#choice-container')[0].scrollHeight}, 'slow');

    var jChoiceImgId = '#choice-img-' + targetId;
    updateImgPosition( $(jChoiceImgId), choice.top, choice.left, rW);
    $('div', jChoiceImgId).show();
}

function replaceTile(oldTileId, newClipTileObj) {

  replaceTileConfig(oldTileId, newClipTileObj);
  replaceTileTemplate(oldTileId, newClipTileObj);

}

function replaceTileConfig (oldTileId, newClipTileObj) {

  // Create a new config object we can work with safely (without old tile)
  var newConfig = _.omit(JSON.parse(JSON.stringify(config)), oldTileId);

  // Deep clone the old config, replacing relevant attributes
  var newTileId = genTileId(newClipTileObj.data('clip-id'));
  newConfig[newTileId] = _.extend(JSON.parse(JSON.stringify(config[oldTileId])), {
    id: newTileId,
    original_asset: newClipTileObj.data('original-asset'),
    original_asset_filename: newClipTileObj.data('original-asset-filename'),
    original_asset_id: newClipTileObj.data('clip-id'),
    original_asset_mime_type: newClipTileObj.data('original-asset-mime-type'),
    original_asset_preview_flv: newClipTileObj.data('original-asset-preview-flv'),
    original_asset_thumbnail: newClipTileObj.data('original-asset-thumbnail')
  });

  // Replace references to the old tile
  if (newConfig.options.initial === oldTileId) newConfig.options.initial = newTileId;
  _.each(newConfig, function(value) {
    _.each(value.video_choices, function(choice) {
      if (choice.next_video_id === oldTileId) {
        choice.label = newTileId;
        choice.next_video_id = newTileId;
      };
    });
  });

  config = JSON.parse(JSON.stringify(newConfig));

}

function replaceTileTemplate (oldTileId, newClipTileObj) {

  // All the above, and the connections aren't reattached. For now if we replace
  // a tile, we just need to hard-refresh the window. Unbelievable.
  checkModified(true);
  // location.reload();

}

$.getScript("//cdn.bootcss.com/interact.js/1.2.4/interact.min.js");


function addChoice( choice, parentTileId)
{
    if( $('#stageContainer').length == 0 )
    {
        var cc = $('<div id="stageContainer"></div>');
        cc.prependTo('#stage');
    }

    ///////////////////////////////////////////////////////////
    // Add images or text to stage
    var jChoiceImgId = 'choice-img-' + choice.next_video_id;
    if( $('#' + jChoiceImgId).length === 0 )
    {
        var h = $('<div id="' + jChoiceImgId + '" class="choiceImg" data-x="' + choice.left +'" data-y="'+ choice.top +'">' +
                       '<img style="display:none;" data-clip-id="' + choice.next_video_id + '" data-parent-clip-id="' + parentTileId + '" src=""/>' +
                       '<div class="innerBlock">' + 
                       '<div class="labelStyle" style="display:none;">' + ( choice.label ? choice.label : "") + '</div>' +
                   '</div>' + '</div>');

        h.appendTo('#stageContainer');

        $('#' + jChoiceImgId).css('top', choice.top).css('left', choice.left);
    }



    ///////////////////////////////////////////////////////////
    // Add choice props section
    $("#choiceProps").tmpl( {   choice : choice,
                                STATIC_URL : STATIC_URL,
                                UPLOAD_URL : UPLOAD_URL
                             }).appendTo("#choice-container");


    //enableTileTools($('#choice-' + choice.next_video_id));

    ///////////////////////////////////////////////////////////
    // configure choice controls
    var choiceProps = $('#choice-props-' + choice.next_video_id);

    var choiceTop   = $('.choice-top',  choiceProps);
    var choiceLeft  = $('.choice-left', choiceProps);
    var choiceTrans = $('.choice-transparency', choiceProps);
    var choiceImg   = $('#choice-img-' + choice.next_video_id);

    var choiceText   = $('.choice-text', choiceProps);
    var choiceWidth  = $('.choice-width', choiceProps);
    var choiceHeight = $('.choice-height', choiceProps);
    var imgSelector  = $('.img-selector', choiceProps);
    var imgSelectorHover = $('.img-selector-hover', choiceProps);

    removeClass($('#' + jChoiceImgId), 'transparent').addClass('transparent' + choice.alpha*10);

    function dragMoveListener (event) {
        var target = event.target;

        var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
            y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

        target.setAttribute('data-x', x);
        target.setAttribute('data-y', y);

        if (y<0) y = 0;
        if (x<0) x = 0;

        choiceTop.editable('setValue', Math.round(y/rW));
        choiceLeft.editable('setValue', Math.round(x/rW));

        $(target).css("left", x);
        $(target).css("top", y);
    }

    function drStart (event) {
        var target = event.target;
        target.setAttribute('data-x', $(target).css("left"));
        target.setAttribute('data-y', $(target).css("top"));
    }

    function resizeFinish (event) {};

    interact('#' + jChoiceImgId)
        .draggable({
            onstart: drStart,
            onmove: dragMoveListener,
            restrict: {
                restriction: '#stage',
                endOnly: false,
                elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
            }
        })
        .resizable({
            onstart: drStart,
            edges: { left: false, right: true, bottom: true, top: false},
            restrict: {
                restriction: '#stage'
                //elementRect: { left: 0, right: 0, top: 1, bottom: 1 }
            },
            onend: resizeFinish
        })
        .on('resizemove', function (event) {
            var target = event.target,
                x = (parseFloat(target.getAttribute('data-x')) || 0),
                y = (parseFloat(target.getAttribute('data-y')) || 0);

            $(target).children().css("width", event.rect.width + 'px');
            $(target).children().css("height", event.rect.height + 'px');

            $(target).css("width", $(target).children(".innerBlock").css("width") + 'px');
            $(target).css("height", $(target).children(".labelStyle").css("height") + 'px');

            x += event.deltaRect.left;
            y += event.deltaRect.top;

            $(target).css("left", x);
            $(target).css("top", y);

            target.setAttribute('data-x', Math.round(x));
            target.setAttribute('data-y', Math.round(y));

            choiceTop.editable('setValue', Math.round(y/rW));
            choiceLeft.editable('setValue', Math.round(x/rW));
            choiceWidth.editable('setValue',  Math.round(parseInt($(target).css("width"))/rW) + "px");
            choiceHeight.editable('setValue', Math.round(parseInt($(target).css("height"))/rW) + "px");
        });

    choiceImg.hover(
        function(cImg, imgSelect) {
            return function() {

                var img = getTextVal2(imgSelect);
                if( img )
                    cImg.attr('src', UPLOAD_URL + '/' + img);
            }

        }($('img', choiceImg), imgSelectorHover),

        function(cImg, imgSelect) {
            return function() {

                var img = getTextVal2(imgSelect);
                if( img )
                    cImg.attr('src', UPLOAD_URL + '/' + img);
            }
        }($('img', choiceImg), imgSelector)
    );


    imgSelector.editable({ value : ( choice.image ? choice.image : 'Edit'), source: imageAssets, select2 : select2options, placement:'bottom' });
    imgSelectorHover.editable({ value : ( choice.image2 ? choice.image2 : 'Edit'), source: imageAssets, select2 : select2options, placement:'bottom' });

    choiceText.editable({ placement:'bottom' });
    choiceWidth.editable({value : (choice.width ? choice.width : 100), placement:'bottom'});
    choiceHeight.editable({value : (choice.height ? choice.height : 50)});
    choiceTop.editable( { value: choice.top,  validate : function( value) { return validatePos( $('#posterImage'), value, 'original-height'); } } );
    choiceLeft.editable({ value: choice.left, validate : function( value) { return validatePos( $('#posterImage'), value, 'original-width'); } } );
    choiceTrans.editable({ value : ( choice.alpha ? choice.alpha : '1'), source: transparencies });

    choiceWidth.on('save',
        function(cImg) {
            return function(e, res) {
                $('div', cImg).css('width', (res.newValue * rW) + 'px');
            }
        }(choiceImg)
    );

    choiceHeight.on('save',
        function(cImg) {
            return function(e, res) {
                $('div', cImg).css('height', (res.newValue * rW) + 'px');
            }
        }(choiceImg)
    );

    choiceText.on('save',
        function(cImg, cWidth, cHeight) {
            return function(e, res) {
                 if( !res )
                    return;

                $('img', cImg).hide();
                $('div', cImg).show().html(res.newValue);//.css('width', 'auto').css('height', 'auto');

                var newWidth = parseInt( $('div', cImg).width() / rW );
                var newHeight = parseInt( $('div', cImg).height() / rW );

                cWidth.editable('setValue', newWidth);
                cHeight.editable('setValue', newHeight);
            }
        }(choiceImg, choiceWidth, choiceHeight)
    );

    choiceTop.on('save',
        function( cImg, cLeft, res) {

            return function(e, res) {
                updateImgPosition( cImg, res.newValue, parseInt(getTextVal2(cLeft)), rW);
            }
        }(choiceImg, choiceLeft)
    );

    choiceLeft.on('save',
        function(cImg, cTop, res) {

            return function(e, res) {
                updateImgPosition( cImg, parseInt(getTextVal2(cTop)), res.newValue, rW);
            }
        }(choiceImg, choiceTop)
    );

    choiceTrans.on('save',
        function(cImg, res) {

            return function( e, res) {
                var newAlpha = parseFloat(res.newValue) * 10;
                console.log("newAlpha",newAlpha);
                console.log($('div', cImg),$('img', cImg))
                console.log("-----------------------------", cImg);
                removeClass($('#' + jChoiceImgId), 'transparent').addClass('transparent' + newAlpha);
            }
        }( choiceImg )
    );

    imgSelector.on('save',
        function( cImg, res) {
            return function( e, res) {
                if( !res )
                    return;

                var img = $('img', cImg);

                img.show();
                img.load( loadChoiceImage );
                img.attr('src', UPLOAD_URL + '/' + res.newValue);
            };
        }( choiceImg )
    );



    var changeType = function(clipId, type) {

        var choiceImg   = $("#choice-img-" + clipId);

        if( type == "image")
        {
            choiceImg.css('width', 'auto').css('height', 'auto');
            $('div', choiceImg).hide();
            $('img', choiceImg).show();

            var clipObj = config[clipId];
            var imgSelector  = $(".img-selector", '#choice-props-' + clipId);
            if( clipObj.original_asset_mime_type == 'ochre/static' )
            {
                var imgSrc = getTextVal2(imgSelector);
                if( !imgSrc )
                    imgSrc = 'advntr/unknown_thumbnail.png';

                $('.slow-images', '#choice-' + clipId).attr('src', UPLOAD_URL + '/' + imgSrc);
            }
        }
        else
        {
            $('img', choiceImg).hide();
            $('div', choiceImg).html( getTextVal2( $(".choice-text", "#choice-props-" + clipId) )).show();

            var clipObj = config[clipId];
            if( clipObj.original_asset_mime_type == 'ochre/static' )
                $('.slow-images', '#choice-' + clipId).attr('src', UPLOAD_URL + '/advntr/unknown_thumbnail.png');

        }

        return true;
    };

    var jChoiceId = '#choice-' + choice.next_video_id;

    $('input:radio', jChoiceId).change(
        function(){
            var t = $(this);
            var on     = t.data('on');
            var off    = t.data('off');
            var clipId = t.attr('data-clip-id');
            var type   = t.attr('value');

            $("." + off).hide('fast', function(){ if( changeType( clipId, type) ) $("." + on).show();  });
        }
    );


    $(jChoiceId).hover(

        function() {
            var clipId = $(this).attr('data-clip-id');
            $('#choice-img-' + clipId).removeClass('choiceBorderOn').addClass('choiceBorderOn');
        },

        function() {
            var clipId = $(this).attr('data-clip-id');
            $('#choice-img-' + clipId).removeClass('choiceBorderOn').removeClass('choiceBorderOff');
        }
    );
}


function editBlock(tileId, level)
{

    $('#edit-modal').empty();

    var item = config[tileId];
    item.id = tileId;

    $("#choiceEditor").tmpl( {
                                level:level,
                                clip:item,
                                config:config,
                                share_msg : getTextVal("config-share-message"),
                                initial: ( tileId == config.options.initial),
                                choiceTile: ( item.original_asset_mime_type.indexOf('ochre/') != 0),
                                UPLOAD_URL : UPLOAD_URL
                             }).appendTo("#edit-modal");

    for( var idx in item.video_choices )
    {
        var choice = item.video_choices[idx];
        addChoice( choice, tileId);
    }


    if( item.original_asset_mime_type.indexOf('ochre/') != 0 )
    {
        var poster = $('#posterImage');

        if( config.width > 0 && config.height > 0 )
        {
            var useId = config.options.initial;
            if( item.hasOwnProperty('original_asset_id') )
                useId = item.original_asset_id;

            var usePath = OUTPUT_PATH;
            if( item.hasOwnProperty('original_output_path') )
                usePath = item.original_output_path;

            var imgSrc = UPLOAD_URL + '/' + usePath + '/thumbnails/' + useId + '_poster_0000.png';

            rW = null;
            poster.load(
                function(e, img) {

                    var img = this;

                    $("#stage").show();

                    removeSpinner("#stage");

                    $("#stageTools").show();


                    var maxWidth = parseInt($(img).css('max-width'));
                    var w = config.width;
                    var h = config.height;
                    $(poster).data('original-width', w).data('original-height', h);
                    rW = ( w >= maxWidth ? ( maxWidth / w ) : ( w / maxWidth ) );

                    var newWidth  = w*rW;
                    var newHeight = h*rW;

                    $(poster).css('width', newWidth + 'px');
                    $(poster).css('height', newHeight + 'px');


                    $(img).width(newWidth);
                    $(img).height(newHeight);

                    for( var idx in item.video_choices )
                    {
                        var choice    = item.video_choices[idx];
                        var choiceImg = $("#choice-img-" + choice.next_video_id);

                        if( choice.hasOwnProperty('top') )
                        {
                            choiceImg.css('top', (choice.top * rW) + 'px');
                            choiceImg.css('left', (choice.left * rW) + 'px');
                        }

                        if( choice.image )
                        {
                            var cImg = $('img', choiceImg);
                            cImg.load( loadChoiceImage );
                            cImg.attr('src', UPLOAD_URL + '/' + choice.image);
                            cImg.show();
                        }
                        else
                        {
                            var d = $('div', choiceImg);
                            d.css( 'width', (parseInt(choice.width) * rW) + 'px');
                            d.css( 'height', (parseInt(choice.height) * rW) + 'px');
                            d.html( ( choice.label ? choice.label : "") ).show();

                            if( !choice.hasOwnProperty('top') )
                                autoPlace(tileId, false, choice.next_video_id);

                        }
                    }


                    $("#previewPlay").click( function() {
                        removeClass( $('div', this), 'ochreicon').addClass('ochreicon-spinner');
                        var ifr = $('iframe', '#previewStage');
                        ifr.attr('width', newWidth + 'px');
                        ifr.attr('height', newHeight + 'px');

                        var newConfig = renderConfigObj(tileId);
                        newConfig.width = parseInt(newWidth);
                        newConfig.height = parseInt(newHeight);
                        newConfig.options.replayButton = true;
                        newConfig.options.fullscreen = false;
                        newConfig.options.instantPlay = true;
                        newConfig.share.enabled = false;
                        if( !usePath )
                            console.error('usePath empty');

                        newConfig.splash.src = usePath + '/thumbnails/' + useId + '_poster_0000.png';
                        newConfig[tileId].video_choices = [];

                        $.ajax({
                            dataType: "json",
                            type:"POST",
                            url: '/accounts/campaigns/preview/',
                            data: {'campaign' : CAMPAIGN_ID, 'config_json' : JSON.stringify(newConfig) , 'width' : newWidth, 'height' : newHeight },
                            error : function(jqXHR, textStatus, err) {
                                console.error("init: " + textStatus + ": " + err);
                            },
                            success: function(data, textStatus, jqXHR) {

                                $(poster).hide(function(d) {
                                    var pS = $('#previewStage');
                                    $('iframe', pS).attr('src', '/embed.html?x=/static/previews/' + d.config_url + '&width=' + d.width + '&height=' + d.height);
                                    pS.show();
                                }(data));
                            }

                        });
                    });


                    var p = $(poster);
                    var top = (p.height() / 2) - (15);
                    var left = (p.width() / 2) - (15);
                    p.append( $('<div style="position:absolute;top:' + top + 'px;left:' + left + 'px;"><img src="' + STATIC_URL + 'advntr/assets/ochre_play_big.png" style="width:30px;" /></div>') );

                }
            );

            poster.attr('src', imgSrc);
            $("#stage").hover(
                function() {
                    $('.choiceImg').removeClass('choiceBorderOn').addClass('choiceBorderOn');
                },
                function() {
                    $('.choiceImg').removeClass('choiceBorderOn').removeClass('choiceBorderOff');
                }
            );

            $("#stage").show();
            addSpinner("#stage");

        }
        else
        {
            var imgSrc = UPLOAD_URL + '/' + item.original_asset_thumbnail;
            poster.attr('src', imgSrc);
            $("#stage").show();
        }
    }

    $("#item_choices_time").editable();
    $("#item_share_msg").editable();
    $("#item_url").editable();
    $("#item_switching").editable({ value : item.instantSwitch, source: switchings });

    $('#edit-modal').bind('show', function() {
        $(this).css({'margin-left': -($(this).width())/2});
    }).modal({ backdrop: true });

    $('#edit-modal').bind('hidden', function() {
        $('iframe', '#previewStage').unbind('load').attr('src', STATIC_URL + 'blank.html');
    });

    var si = $(".slow-images", "#edit-modal");
    si.imageReloader();

}

function select2ImageFormat(val) {
    if( !val || val.length == 0 )
        return "Empty";

    return "<img src='" + UPLOAD_URL + '/' + val.id +  "' class='select-img' /> " + val.text;
}

var select2options = {
    formatResult: select2ImageFormat, formatSelection: select2ImageFormat, allowClear: true, placeholder: "Empty", escapeMarkup: function(m) { return m; }
};


function publish()
{

    try {
        $('#publish-modal').modal('hide');
        showSaveDialog('Publishing...');

        var rendered = renderConfig();
        $("#configData").val( rendered );
        $("#publishing").val( 1 );
        $("#dataForm").submit();

        update_mixpanel('Publish', { campaign_id:CAMPAIGN_ID});
    }
    catch(err)
    {
        logError("Error saving campaign " + CAMPAIGN_ID + " err=" + err);
        hideSaveDialog();
    }
}

function updateImageAssets()
{
    $("#config-options-watermark-src").editable({ value : ( config.options.watermark ? config.options.watermark.src : 'Edit'), source: imageAssets, select2 : select2options });
    $("#config-splash-src").editable({ value: ( config.splash ? config.splash.src : 'Edit'), source: imageAssets, select2 : select2options });
}

function updateVideoAssets()
{
    $("#config-options-altMp4").editable({ value: (config.options.altMp4 ? config.options.altMp4 : 'Edit'), source: videoAssets });
}

function addAssetToSection( section, toId, before, t, m )
{
    if( before )
    {
        var o = $(section + ' .asset-storage', toId);
        var ofirst;
        if( m )
            ofirst = $(m, o).first();

        if( !ofirst || ofirst.length == 0 )
            ofirst = o.first();
        ofirst.before(t);
    }
    else
        t.appendTo( $(section + ' .asset-storage', toId) );

    $(section, toId).show();
}

function addAsset( obj, toId, before, m )
{
    var t = $('#assetTemplate').tmpl( obj );

    if( obj.asset.mime_type.indexOf('image/') == 0 )
        addAssetToSection('.tabImages', toId, before, t, m);
    else if( obj.asset.mime_type.indexOf('video/') == 0 )
    {
        addAssetToSection('.tabVideos', toId, before, t, m);
        t.draggable(draggable_options_assets);
    }
    else
    {
        addAssetToSection('.tabOther', toId, before, t, m);

        if( obj.asset.mime_type.indexOf('ochre/') == 0 )
            t.draggable(draggable_options_assets);

    }

    return t;
}


function addDropHelp()
{
    if( MOBILE )
        $('.dropZoneHelp').html( $('<span>Add a video from the right</span>') );

    $('#dropZone').addClass('dropZoneEnabled');
    $('.dropZoneHelp').show();
    openAssets();
}

function addConnection( sourceId, targetId )
{

    targetLabel = '';
    if( config[targetId].hasOwnProperty('original_asset_id') )
        targetLabel = config[targetId].original_asset_id;

    config[sourceId].video_choices.push( { next_video_id: targetId, label : targetLabel, width:200, height:100 } );

    var connectionProps = { bb1: { index:config[sourceId].video_choices.length-1, total:config[sourceId].video_choices.length} };
    var removeLink = getRemoveLink(sourceId, targetId);

    funnelDisplay.addConnection( sourceId, targetId, removeLink, '#ffcc00', 5, connectionProps );

    var removeId = '#' + sourceId + '-' + targetId + '-remove';
    $('.remove-connection', removeId).bind("click", onRemoveConnection );
}

function addSpinner( toId )
{
    var obj  = $(toId);
    obj.append( $('.loadingSpinner').clone() );

    var spin = $(".loadingSpinner", obj);

    var top = (obj.height() / 2) - (spin.height() / 2);
    spin.css('top', top + 'px');
    spin.show();
}

function addAlert(type, msg)
{
    $('#adventr_alerts').show().append($('<div class="alert ' + type + '"> <button type="button" class="close" data-dismiss="alert">&times;</button> <strong>' + msg + '</strong> </div>'));
}

function removeSpinner( fromId )
{
    $('.loadingSpinner', fromId).remove();
}

function resetCampaign()
{
    if( !confirm("Are you sure you want to reset this campaign?") )
        return;

    if( CAMPAIGN_ID == 0 )
        location.reload();

    $.ajax({
        dataType: "json",
        type:"POST",
        url: '/accounts/campaigns/reset/',
        data: {'campaign' : CAMPAIGN_ID },
        error : function(jqXHR, textStatus, err) {
            console.error("init: " + textStatus + ": " + err);
        },
        success: function(data, textStatus, jqXHR) {
            location.reload();
        }

    });

}


function sizeDownWidthHeight( width, height )
{
    if( width == 0 ) width = 640;
    if( height == 0 ) height = 390;

    newWidth  = width;
    newHeight = height;
    if( width > 640)
    {
        newWidth = 640;
        newHeight = height / width * newWidth;
    }
    return [newWidth, newHeight]
}

function enableZoom()
{
    if($('#funnel').css('zoom'))
    {
        if( !MOBILE )
            $('.zoomablePane').hover( function() { $('#zoomControls').fadeIn(100); }, function() { $('#zoomControls').fadeOut(100); } );
        else
            $('#zoomControls').show();

        $('#funnel').css('zoom', 1);
    }
}

function zoomFunnel(val)
{
    $('#funnel')[0].style.zoom = parseFloat($('#funnel')[0].style.zoom) + val;

    var width  = $('#funnel').width()  + ($('#funnel').width() * val);
    var height = $('#funnel').height() + ($('#funnel').height() * val);

    funnelDisplay.raphael().setSize(width, height);
    funnelDisplay.updateConnections();

    //console.log("width=" + width + " height=" + height);
    return false;
}

function tileIsLoading($el) {
    var $p = $el.find('.loading-image');
    return ( $p.length > 0 );
}

function preview(id)
{
    if( tileIsLoading( $('#tile_' + id) ) )
        return alertModal('Preview not ready yet');

    var rendered = renderConfig(id);

    addSpinner( '#previewDialog' );

    $("#previewDialogIfr").bind('load', function() {
        removeSpinner('#previewDialog');
    } );

    size = sizeDownWidthHeight( config.width, config.height )

    width = size[0];
    height = size[1];

    if( width > 0 && height > 0 )
    {
        $('#previewDialog').css('width', (width + 20) + 'px');
        $('#previewDialog').css('height', (height + 20) + 'px');
    }

    $.ajax({
        dataType: "json",
        type:"POST",
        url: '/accounts/campaigns/preview/',
        data: {'campaign' : CAMPAIGN_ID, 'config_json' : rendered, 'width' : config.width, 'height' : config.height },
        error : function(jqXHR, textStatus, err) {
            console.error("init: " + textStatus + ": " + err);
        },
        success: function(data, textStatus, jqXHR) {
            $("#previewDialogIfr").attr('src', '/accounts/campaigns/preview/?config_url=' + data.config_url + '&width=' + data.width + '&height=' + data.height);
        }

    });

    $('#previewDialog').bind('hidden', function() {
        $('#previewDialogIfr').unbind('load');
        $('#previewDialogIfr').attr('src', STATIC_URL + 'blank.html');
    });

    $('#previewDialog').bind('show', function() {
        $(this).css({'margin-left': -($(this).width())/2});
    }).modal({ backdrop: true });

    update_mixpanel('Preview', { campaign_id:CAMPAIGN_ID, video_id : id});
}

function editConfig()
{

    $('#modal-from-dom').empty();

    $("#editConfigTemplate").tmpl( {}).appendTo("#modal-from-dom");

    $('#modal-from-dom').bind('show', function() { }).modal({ backdrop: true });


   $.ajax({
      dataType: "json",
      url: '/accounts/campaigns/config/',
      data: {'campaign' : CAMPAIGN_ID },
      error : function(jqXHR, textStatus, err) {
        console.error("init: " + textStatus + ": " + err);
    },
      success: function(data, textStatus, jqXHR) {
        $("#configDataEdit").text( data.config );

     }

    });
}

function saveConfigAdmin()
{
    $("#configData").val( $("#configDataEdit").val() );
    $("#dataForm").submit();
}

function s3_upload(obj){

    var s3upload_update = function ( upload_asset_id, state )
    {
       $.ajax({
              type:"POST",
              dataType: "json",
              url: '/accounts/s3state/',
              data: {'upload_asset_id' : upload_asset_id, 'state' : state },
              error : function(jqXHR, textStatus, err) {
                console.error("init: " + textStatus + ": " + err);
            },

        success: function(data, textStatus, jqXHR) { }

      });
    };

    openAssets();
    var inputId = '#' + obj.id;

    var s3upload = new S3Upload({
        file_dom_selector: inputId,
        s3_sign_put_url: '/accounts/s3sign/',
        campaign_id: CAMPAIGN_ID,

        onFileUploadReady: function(upload_asset, existed, file) {

            if( findAsset( config, this.upload_asset.file_original) )
            {
                alertModal( this.upload_asset.file_original + ' is connected in your project. Please disconnect it and try again.');
                return false;
            }

            if( existed && !confirm(upload_asset.file_original + ' already exists. Overwrite?') )
            {
                return false;
            }

            // remove using the original filename since the id is random for identical filenames
            $('*[data-original-asset-filename="' + this.upload_asset.file_original + '"]').remove();

            var elem = addAsset( { asset: this.upload_asset, thumbnail : '' }, '#yourVideos', true, '.assetTile' );
            $(".progress", elem).show();
            enableTileTools(elem);

            update_mixpanel('Upload', { campaign_id:CAMPAIGN_ID, file: this.upload_asset.file_original });
            return true;
        },

        onProgress: function(percent, message, file) {

            if( this.upload_asset )
                $(".bar", "#upload_asset_" + this.upload_asset.file_id).css("width", percent + "%");
        },
        onFinishS3Put: function(url, file) {

            s3upload_update(this.upload_asset.id, 1);

            var id="#upload_asset_" + this.upload_asset.file_id;

            $(".progress", id).remove();

            if( file.type.indexOf('image') == 0 )
            {
                imageAssets.push( { text : this.upload_asset.file_id, id: this.upload_asset.thumbnail_chosen } );
                updateImageAssets();
            }
            else if( file.type.indexOf('video') == 0 )
            {
                videoAssets.push( { text : this.upload_asset.file_id, id: this.upload_asset.file_path } );
                updateVideoAssets();
            }

            var si = $(".slow-images", id);
            si.attr('src', UPLOAD_URL + '/' + this.upload_asset.thumbnail_chosen);
            si.imageReloader({onLoad: function(el) {


            }});
        },
        onError: function(status) {
            if( !this.upload_asset )
                return;

            var id="#upload_asset_" + this.upload_asset.file_id;

            $(".progress", id).html('Upload Error');
            $(".slow-images", id).attr('src', '');
            s3upload_update(this.upload_asset.id, 0);

            update_mixpanel('UploadFail', { campaign_id:CAMPAIGN_ID, file: this.upload_asset.file_original});
        }
    });

    $(inputId).val('');
}

function checkModified(reload)
{
    var currentConfigStr = renderConfig();
    if( originalConfigStr != currentConfigStr )
    {
        //console.log(originalConfigStr);
        //console.log(currentConfigStr);

        $.ajax({
            dataType: "json",
            type:"POST",
            url: '/accounts/campaigns/edit/',
            data: {campaign : CAMPAIGN_ID, configData: currentConfigStr, auto_save:1 },
            error : function(jqXHR, textStatus, err) {
                console.error("autosave(" + textStatus + ") " + err);
            },
            success: function(data, textStatus, jqXHR) {

                // This is horrible. We really need to clean up the flow on this
                // page.
                if (typeof reload !== "undefined") location.reload();

                config.campaign.campaignId = CAMPAIGN_ID = data.campaign_id;
                $("input:hidden[name='campaign']", "#dataForm").val(CAMPAIGN_ID);

                var d = new Date();
                $('#auto_saved').html( 'Auto saved at ' + padNum(d.getHours()) + ':' + padNum(d.getMinutes()) + ':' + padNum(d.getSeconds()));

                originalConfigStr  = currentConfigStr;
            }

        });

    }
}

function accountsEditSetup() {

        updateImageAssets();
        updateVideoAssets();


        $("#config-name").editable({ value: ( config.name ? config.name : 'Edit') });
        $("#config-share-message").editable({ value: (config.share.fb_msg ? config.share.fb_msg : 'Edit') });
        $("#config-share-link").editable({ value: (config.share.link ? config.share.link : 'Edit') });
        $("#config-options-clickThrough").editable({ value: (config.options.clickThrough ? config.options.clickThrough : 'Edit') });
        $("#config-campaign-ad_id").editable({ value: (config.campaign.ad_id ? config.campaign.ad_id : 'Edit') });

        $("#config-options-volume").editable( {
            value : ( config.options ? config.options.volume : 'Unset' ),
            source: [
                {value: "0", text: 'Mute'},
                {value: ".1", text: '1'},
                {value: ".2", text: '2'},
                {value: ".3", text: '3'},
                {value: ".4", text: '4'},
                {value: ".5", text: '5'},
                {value: ".6", text: '6'},
                {value: ".7", text: '7'},
                {value: ".8", text: '8'},
                {value: ".9", text: '9'},
                {value: "1", text: 'Max'}
            ]
        });

        updateAnalytics();

        $("#edit-analytics").click( function() {

            $('#modal-from-dom').empty();
            $("#editAnalyticsTemplate").tmpl( { config : config} ).appendTo("#modal-from-dom");

            updateAnalyticsButtons();

            $('#modal-from-dom').bind('show', function() { }).modal({ backdrop: true });

        });


        funnelDisplay.init(config, config.options.initial);

        $('.remove-connection').bind("click", onRemoveConnection );

        $('body').click( function(e) {

            if( !connectionsEnabledFor )
                return true;

            if( enablingConnections )
            {
                enablingConnections = false;
                return true;
            }

            var tobj = $(e.target);
            if( !tobj )
            {
                console.error("expected a target object for campaign=" + CAMPAIGN_ID);
                return false;
            }

            var targetId = null;
            if( tobj.hasClass('clipTile') )
                targetId = tobj.attr('data-clip-id');
            else if( tobj.parents().hasClass('clipTile') )
            {
                var c = tobj.parents('.clipTile');
                if( c.length == 0 )
                {
                    console.error("expected a .clipTile for parent. campaign= " + CAMPAIGN_ID);
                    return false;
                }

                targetId = $(c[0]).attr('data-clip-id');
            }

            if( targetId && targetId != config.options.initial && targetId != connectionsEnabledFor )
                addConnection( connectionsEnabledFor, targetId );


            return disableConnectingAction();
        });



        $("#dropZone").droppable({
              drop: function( event, ui ) {

                var clipTileObj = $(ui.helper);
                var originalTileId;

                var tileId = originalTileId = clipTileObj.attr('data-clip-id');
                tileId = genTileId(tileId);

                if( !clipTileObj.hasClass('clipTileDroppable') )
                    return;

                // create config obj

                var p = $(this).offset();
                var top  = ui.position.top  - p.top;
                var left = ui.position.left - p.left;

                var mime_type = clipTileObj.data('original-asset-mime-type');

                var isVideo = mime_type.indexOf('video/') == 0;


                var usePath = OUTPUT_PATH;
                if( clipTileObj.data('original-output-path') )
                    usePath = clipTileObj.data('original-output-path');

                config[tileId] = {
                    "id" : tileId,
                    "campaign_id" : clipTileObj.data('campaign-id'),
                    "original_asset_id" : originalTileId,
                    "original_asset" : clipTileObj.data('original-asset'),
                    "original_output_path": usePath,
                    "original_asset_mime_type" : mime_type,
                    "original_asset_filename" : clipTileObj.data('original-asset-filename'),
                    "original_asset_preview_flv" : clipTileObj.data('original-asset-preview-flv'),
                    "original_asset_thumbnail" : clipTileObj.data('original-asset-thumbnail'),
                    "share_msg" : "",
                    "choices_time":5000,
                    "instantSwitch":1,
                    "flv":"",
                    "mp4" : "",
                    "video_choices":[ ],
                    "hls" : { "segment_durations" : [] }
                };

                if( mime_type == 'ochre/replay' )
                    config[tileId]['url'] = 'replay';

                if( $('.dropZoneHelp').is(':visible') )
                    $('.dropZoneHelp').hide();

                var tile = $("#tileTemplate").tmpl( {level:1, clip : config[tileId], top:top, left:left, isVideo:isVideo, UPLOAD_URL : UPLOAD_URL } );
                tile.appendTo('#funnel');
                tile.draggable(draggable_options_clips);

		var si = $(".slow-images", tile);
                si.imageReloader();

                enableTileTools(tile);

                if( config.options.initial == null && isVideo )
                {
                    config.options.initial = tileId;
                    setInitial( tileId );
                }
              }
        });

        // do after asset load
        var val = null;
        if( config.options.watermark.src.length > 0 )
        {
            $('#edit-watermark').data('src', config.options.watermark.src);
            val = $('*[data-original-asset="' + config.options.watermark.src + '"]').attr('data-clip-id'); // get the clip-id for this file
        }
        if( !val )
            val = 'Empty';

        $('#edit-watermark').text( val );

        $('#uploadButton').on('click', function() {
            $('#fileinput').click();
        });

        if( !MOBILE )
            $('#uploadButton').show();
        else // all future previewButtons created should be hidden
            $('<style>.previewButton { display:none !important; }</style>').appendTo('body');

        // file upload button
        $('input[type=file]').on('click', function() {
            if( CAMPAIGN_ID == 0 ) {
                alert('You must save your project before uploading videos');
                return false;
            }

            return true;
        });

        $(".slow-images").imageReloader();

        setInitial( config.options.initial );

        if( !config.options.initial )
            addDropHelp();
        else
            openAssets();

        $("#load-revisions").editable();
        $('#load-revisions').on('save', function(e, params) {
            location.href= "/accounts/campaigns/edit/?campaign=" + CAMPAIGN_ID + "&revision=" + params.newValue;
        });

        if( config.width == 0 )
            loadPosterForSize();


        setInterval( checkModified, 5000);
        originalConfigStr = renderConfig();

        enableZoom();

        $(window).resize();

        if( getQueryParamValue('publish') == '1' )
            confirmPublish()


}

function setCookie(cname, cvalue, exdays)
{
    var d = new Date();
    if(exdays === undefined )
        exdays = 3650 // 10 years
    d.setTime(d.getTime()+(exdays*24*60*60*1000));
    var expires = "expires="+d.toGMTString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname)
{
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++)
    {
      var c = ca[i].trim();
      if (c.indexOf(name)==0) return c.substring(name.length,c.length);
    }
    return "";
}

function changeFunnelSegment()
{

    var s = $("#filter").val();
    if( s == '' )
        $("#filters").hide();
    else
        $("#filters").show();
}

///////////////////////////////////////
// Dashboard/Funnel select value loading
function checkChartType() {
    if( $("#equals").is(':visible')) {
        loadValueData();
    }
}

function checkFunnelType() {
    if($("#filters").is(':visible')) {
        $("#equals").show();
        loadValueData(true);
    }
}

function chartEquals()
{
    $("#equals").show();

    if( $(".icon-circle-arrow-right").length > 0 ) {
        $(".icon-circle-arrow-right").removeClass("icon-circle-arrow-right").html('&nbsp;=&nbsp;');

        loadValueData();
    }
    else
    {
        $(".equals_button").html("");
        $(".equals_button").addClass("icon-circle-arrow-right");

        $("#equals").hide();
        $("#equals_text").val("");
    }
}

function loadValueData(useSelect) {
    // Get current value selected
    // Use the text value from the option, not the value

    var segment = $("#filter").val();

    if( values_store[segment] ) {
        renderValueData(segment, useSelect);
    }
    else {


        var $loader = $('<div class="save-loading"> <img src="' + STATIC_URL + 'advntr/assets/ochre_replay2.png" /> </div>');
        $('#equals').append($loader);
        $loader.show();

        $.ajax({
            dataType: "json",
            type:"GET",
            url: '/accounts/dashboard_values?campaign=' + CAMPAIGN_ID,
            error : function(jqXHR, textStatus, err) {
                console.error("init: " + textStatus + ": " + err);
            },
            success: function(data, textStatus, jqXHR) {
                values_store = data.data;
                renderValueData(segment, useSelect);
                $loader.remove();
            }

        });
    }
}

function renderValueData(type, skipAny) {
    var $equals_text = $('#equals_text').empty();

    if( !skipAny ) {
        var $option = $('<option/>');
        $option.attr('value', '').attr('selected', 'selected');
        $option.text('Any');
        $equals_text.append($option);
    }

    for( var idx in values_store[type] ) {
        var $option = $('<option/>');
        var value = values_store[type][idx];
        $option.attr('value', value);
        $option.text(value);
        $equals_text.append($option);
    }

    $equals_text.select2();
    $equals_text.select2("val", "");
}

///////////////////////////////////////
///////////////////////////////////////
function renderDates() {

    var getDateStr = function(d) {
        return MONTH_NAMES[d.getMonth()] + ' ' + padNum(d.getDate()) + ', ' + d.getFullYear();
    };

    var sd = strToDate($("#start_date").val());
    var ed = strToDate($("#end_date").val());

    $("#datePickSelector").hide();
    $("#datePickRange").html( getDateStr(sd) + " to " + getDateStr(ed));

    $("#datePickBase").show();
}

function submitChartQuery( get_data )
{
    $('.chart-data').hide();
    var $loader = $('<div class="save-loading"> <img src="' + STATIC_URL  + 'advntr/assets/ochre_replay2.png" /> </div>');
    $("#chart_holder").append( $loader );

    $('.statContainer').hide();
    $loader.css('top', '50px').css('left', '150px').show();


    $.ajax({
        datatype: "json",
        type:"get",
        url: '/accounts/dashboard_filter/',
        data: get_data,
        error : function(jqxhr, textstatus, err) {
            console.error("init: " + textstatus + ": " + err);
            $loader.remove();

            addAlert( 'alert-error', 'An error occurred and we are looking into it. Sorry for the inconvenience' );
        },
        success: function(data, textstatus, jqxhr) {
            $loader.remove();

            skip = {};
            selectAll = true;
            click_data = data.click_data;

            plotAccordingToChoices(data.segment_type.capitalize());

            if( !dictIsEmpty(data.table) ) {

                var body = $('tbody', '.statContainer');
                body.empty();

                var numPlays = 0;
                var numDuration = 0;
                var numDurationCount = 0;
                var ks = Object.keys(data.table).sort(compareLowerCase);

                if( ks.length == 0 )
                    return;

                for( k in ks)
                {
                    var label = ks[k];
                    var obj = data.table[label];
                    var d = (obj.duration_count > 0 ? (obj.duration / obj.duration_count).toFixed(2) : 0)
                    body.append( $('<tr><td>' + label + '</td><td>' + numberWithCommas(obj.plays) + '</td><td>' + (d >= 1 ? d : 'N/A') + '</td></tr>') )

                    numPlays += obj.plays;
                    if( d >= 1 )
                    {
                        numDuration += obj.duration;
                        numDurationCount += obj.duration_count;
                    }
                }

                var d = ( numDurationCount > 0 ? (numDuration / numDurationCount).toFixed(2) : 0);
                body.append( $('<tr><td>Total</td><td>' + numberWithCommas(numPlays) + '</td><td>' + d + '</td></tr>') )

                var s = $("#select_segment").val();
                var t = $('select', '#select_' + s).find(':selected').text();
                var dateStr = $('#start_date').val() + ' - ' + $('#end_date').val();
                $('#dataType').html(t + ' (' + dateStr + ')');

                $('.statContainer').show();
            }
        }

    });
}

function submitFunnelQuery( get_data )
{
    var chart = $("#funnel");
    chart.empty();
    var w = chart.width();
    var h = chart.height();

    var $loader = $('<div class="save-loading"> <img src="' +  STATIC_URL + 'advntr/assets/ochre_replay2.png" /> </div>');
    $("#funnel").show().append($loader);
    $loader.css('top', '50px').css('left', '150px').show();

    setTimeout( function() {
    $.ajax({
        datatype: "json",
        type:"get",
        url: '/accounts/campaigns/funnel_filter/',
        data: get_data,
        error : function(jqxhr, textstatus, err) {
            console.error("init: " + textstatus + ": " + err);
            $loader.remove();

            addAlert( 'alert-error', 'An error occurred and we are looking into it. Sorry for the inconvenience' );
        },
        success: function(data, textstatus, jqxhr) {
            $loader.remove();

            config_data = data.config_data;

            click_data  = data.has_data;
            if( !click_data )
            {
                $('#funnel').html( $('<div class="dataEmpty">No published data for that filter</div>') );
            }
            else if( !config_data.options.initial )
            {
                $('#funnel').html( $('<div class="dataEmpty">Your campaign is empty. Please add new videos</div>') );
            }
            else
            {

/*
                config_data['Replay'] = { id : 'Replay',
                                        original_asset_thumbnail : 'advntr/assets/ochre_replay2.png',
                                        next_video_id : '',
                                        clicks : [],
                                        video_choices : [ ] };
                                    */

                if( !config_data.Load )
                    config_data['Load'] = {}
                config_data['Load']['id'] = 'Load';
                config_data['Load']['campaign_name'] = CAMPAIGN_NAME;
                config_data['Load']['original_asset_thumbnail'] = 'advntr/assets/ochre_play_big.png';
                config_data['Load']['video_choices'] = [
                    { next_video_id : config_data.options.initial }
                ];

                funnelDisplay.init(config_data, 'Load');
            }



        }

    });

    }, 1000);
}
