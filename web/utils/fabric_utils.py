import string
import datetime
from functools import partial
import fabric
import os, glob
from fabric import *
from fabric.api import lcd, env,cd,require
from fabric.operations import *
from fabric.contrib import files

from xml.dom import minidom

import logging
logger = logging.getLogger(__name__)

#files to be excluded from serach replace operation
exclude_files = ['*.pyc', 'raphael.js']

def collect_files( files ):
    collected = []
    for f in files:
        if f.find("*") >= 0:
                for ff in glob(f):
                        collected.append(ff)
        else:
            collected.append(f)

    return collected 

def search_replace( dirs, search, replace, exclude_files=[]):

    # find . | xargs file | grep ".*: .* text" | sed "s;\(.*\): .* text.*;\1;"

    exclude_list = ' '.join(["--exclude='{}'".format(f) for f in exclude_files])

    for dir in dirs:

        try:

            env.warn_only = True

            logger.info('Searching for files in ' + dir + ' to replace ' + search + ' with ' + replace)

            output=local("grep -r " + exclude_list + " -l " + '"' + search + '" ' + dir + '/*', capture=True)


            for line in output.split('\n'):
                line = line.strip()
                if len(line) == 0:
                    continue

                local('sed "s/' + search + '/' + replace.replace('/', '\/') + '/g" ' + line + ' > ' + line + '.tmp')
                local('mv ' + line + '.tmp ' + line)

        except Exception, inst:
            logger.info("Error: " + str(inst))

        finally:
            env.warn_only = False

search_replace = partial(search_replace, exclude_files=exclude_files)
        

def minify_files( cmd, file_list, start_dir ):

    for file in file_list:
        local( "%s %s > %s.tmp" % (cmd, file, file) )
        local( "mv %s.tmp %s" % (file, file) )
        

def version_files( find_dirs, file_list ):

    file_list = collect_files( file_list )

    for file in file_list:
        version_file( file, find_dirs)

def version_file( file, find_dirs):

    try:
        if not os.path.exists( file ):
            logger.error("File %s DOES NOT EXIST **************")
            return

        revision = get_git_revision(file)

        ind      = file.rfind('/')
        path     = file[0:ind]
        filename = file[file.rfind('/') + 1:]

        # make file.ext = file_revision.ext
        ind = filename.rfind('.')
        if ind < 0:
            raise Exception("File name missing extension: " + file)

        name     = filename[0:ind]
        ext      = filename[ind:]
        new_filename = name + '_' + revision + ext

        print 'Versioning ' + file + ' to ' + path + '/' + new_filename

        search_replace( find_dirs, filename, new_filename )

        mv_cmd = 'cp -f ' + file + ' ' + path + '/' + new_filename
        local(mv_cmd)
    except Exception, inst:
        logger.error("Exception %s" % (inst))
       

def iter_file( filename ):

    for line in open( filename, "r").readlines():

        # remove anything after '#'
        idx = string.find( line, "#" )
        if idx >= 0:
            line = line[0:idx]

        file = line.strip()

        if len(file) == 0:
            continue

        yield file

def getMD5( filename ):
    import hashlib
    file = open(filename)
    m = hashlib.md5()
    m.update(file.read())
    digest = m.hexdigest()
    file.close()

    return digest

def get_git_revision( filename ):
    output=local('git log --oneline -n1 ' + filename, capture=True)
    return output.split(' ')[0]


def xmltodict(xmlstring):
	doc = xml.dom.minidom.parseString(xmlstring)
	remove_whitespace_nodes(doc.documentElement)
	return elementtodict(doc.documentElement)

def get_revision( filename ):

    output=local('svn info --xml ' + filename, capture=True)

    d = xmltodict(output)

    return d['entry'][0]['revision']
        


def get_exclude_files( filename ):
    all = []
    for file in iter_file( filename ):
        all.append( file )

    return all


def get_timestamp():
    return datetime.datetime.now().strftime('%Y%m%d_%H%M%S')


def nolines(str):
    return ''.join(str.strip().splitlines())


def get_backup_dir(remote_file=None):

    backup_file_path = None
    if not remote_file:
        backup_file_path=prompt('Local backup filename', default=(''))
        if len(backup_file_path.strip()) == 0:
            raise Exception("Need backup file")
    else:
        local( 'export PYTHONPATH=`pwd`;python base/manage.py s3_download %s %s' % ('reactr.backup', remote_file) )
        backup_file_path = '/tmp/' + remote_file


    backup_file = backup_file_path[backup_file_path.rfind('/')+1:]

    dir = '/tmp'
    with lcd('/tmp'):
        tar_file = backup_file
        if backup_file.endswith('gz'):
            local('gzip -d %s' % (backup_file))
            tar_file = backup_file[0:backup_file.rfind('.')]

        dir = '/tmp/' + tar_file
        if tar_file.endswith('tar'):
            local('tar xf %s' % (tar_file) )
            dir = '/tmp/' + tar_file[0:tar_file.rfind('.')]

    return dir

