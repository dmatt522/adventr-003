import os
def ENV(str, default=''):
    return os.environ.get(str,default)

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

EMAIL_SUBJECT_PREFIX = ENV('EMAIL_SUBJECT_PREFIX', default='[Ochre] ')

ADMINS = (('Admin', 'ryan@adventr.tv'), ('Devon', 'devon@adventr.tv'))
SERVER_EMAIL= ENV('SERVER_EMAIL', default=ADMINS[0][1])

NOTIFY_EMAILS = [SERVER_EMAIL, "ryan@adventr.tv"]

USE_X_FORWARDED_HOST = True

ALLOWED_HOSTS = [
    "staging.theochre.com",
    "video.theochre.com",
    "www.theochre.com",
    "localhost.local",
    "*.theochre.com",
    "107.21.228.209",
    "adventr.tv",
    "www.adventr.tv",
    "play.adventr.tv",
    "analytics.adventr.tv",
    "staging.adventr.tv",
    "127.0.0.1:8001",

]

MANAGERS = ADMINS

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '7q_fs&m(+vl*&+@b_5zod-az9+vpc4&ckcn9lo^0o_!_o)g+#$'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #     'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.i18n',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.messages.context_processors.messages',
    'context_processors.add_settings',
)



MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'base.middleware.ImpersonateMiddleware',
    'base.middleware.ProfileHeaderMiddleware',
    #'djstripe.middleware.SubscriptionPaymentMiddleware',
)

ROOT_URLCONF = 'web.urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_ROOT, 'templates'),
)

INSTALLED_APPS = (

    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'kombu.transport.django',
    'djcelery',
    'south',
    'djstripe',
    'raven.contrib.django.raven_compat',
    'base',

)


# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        }
    },
    'handlers': {

        'null': {
            'level':'DEBUG',
            'class':'django.utils.log.NullHandler',
        },
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
        },



        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },

        'boto' : {
            'handlers': ['null'],
            'level': 'DEBUG',
            'propagate': False


        },

        '': {
            'handlers': ['console', 'mail_admins'],
            'level': 'DEBUG',
            'propagate': True
        },
    }
}



DEBUG = ENV('DJANGO_DEBUG')=='1'
TEMPLATE_DEBUG = DEBUG

GOOGLE_ANALYTICS_ID = 'UA-34958621-1'
AUTH_PROFILE_MODULE = 'base.Profile'
AUTHENTICATION_BACKENDS = ['django.contrib.auth.backends.ModelBackend', 'base.fb_tools.facebook_auth.FacebookAuth']

LOGIN_URL='/accounts/login/'


import dj_database_url
DATABASES = {'default': dj_database_url.config(default='sqlite:///projects/ochre/ochre.sqlite')}


################################################
# Set in heroku and read from env
AWS_ACCESS_KEY    = ENV('AWS_ACCESS_KEY')
AWS_SECRET_KEY    = ENV('AWS_SECRET_KEY')
AWS_CLOUDFRONT_ID = ENV('AWS_CLOUDFRONT_ID')
AWS_STATIC_BUCKET = ENV('AWS_STATIC_BUCKET')
AWS_UPLOAD_BUCKET = ENV('AWS_UPLOAD_BUCKET')
UPLOAD_URL = '//s3.amazonaws.com/' + AWS_UPLOAD_BUCKET
VIDEO_URL = '//s3.amazonaws.com/' + AWS_STATIC_BUCKET
STATIC_ROOT       = ''
STATIC_URL        = ENV('STATIC_URL')
FACEBOOK_APP_ID   = ENV('FACEBOOK_APP_ID')
FACEBOOK_SECRET   = ENV('FACEBOOK_SECRET')

HTTP_DOMAIN       = ENV('HTTP_DOMAIN')
HTTPS_DOMAIN      = ENV('HTTPS_DOMAIN')
PLAY_URL          = ENV('PLAY_URL', HTTPS_DOMAIN)

PUBLISHED_DIR     = ENV('PUBLISHED_DIR', PROJECT_ROOT + '/../../ochre_published')
PREVIEWS_DIR      = ENV('PREVIEWS_DIR', PROJECT_ROOT + '/static/previews')
PLAYLISTS_DIR     = ENV('PLAYLISTS_DIR', PROJECT_ROOT + '/static/playlists')

MONGODB_URL       = ENV('MONGODB_URL')

PLACEHOLDER_CAMPAIGN_ID = ENV('PLACEHOLDER_CAMPAIGN_ID', default=1)


STATIC_DIR        = ENV('STATIC_DIR', default=os.path.join(PROJECT_ROOT, "static") )

STATICFILES_DIRS = (STATIC_DIR,)

ZENCODER_API_KEY = 'd678608399b507aa520278d7b94393c0'
ZENCODER_CALLBACK = HTTP_DOMAIN + '/zencallback'

ZENCODER_CREATE_THUMBNAILS=True

EMAIL_HOST_USER     = ENV('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = ENV('EMAIL_HOST_PASSWORD')
EMAIL_HOST          = ENV('EMAIL_HOST', default='email-smtp.us-east-1.amazonaws.com')
EMAIL_PORT          = int(ENV('EMAIL_PORT', default='587'))
EMAIL_USE_TLS       = True


MAILCHIMP_API_KEY = ENV('MAILCHIMP_API_KEY')
MAILCHIMP_BASE_API_ENDPOINT = 'https://us8.api.mailchimp.com/3.0'  # includes customer DC as subdomain
MAILCHIMP_SUBSCRIBER_LIST = '8b47e3762d'  # Used for signing up new subscribers


ADVENTR_EVENT_TABLES = ENV('ADVENTR_EVENT_TABLES')

MAXMIND_CITY_DB = ENV('MAXMIND_CITY_DB')
MAXMIND_REGION_CODES = ENV('MAXMIND_REGION_CODES')

MIXPANEL_AD_TRACKING_TOKEN = ENV('MIXPANEL_AD_TRACKING_TOKEN', '')
MIXPANEL_ACCOUNTS_ACTIVITY_TOKEN = ENV('MIXPANEL_ACCOUNTS_ACTIVITY_TOKEN', False)
MIXPANEL_API_KEY    = ENV('MIXPANEL_API_KEY', '')
MIXPANEL_API_SECRET = ENV('MIXPANEL_API_SECRET', '')



FACEBOOK_APP_ID = ENV('FACEBOOK_APP_ID')
FACEBOOK_APP_SECRET = ENV('FACEBOOK_APP_SECRET')
FACEBOOK_REQUEST_PERMISSIONS='email'

CONFIG_URL_BASE = ENV('CONFIG_URL_BASE', 'http://static.theochre.com')


# Configuration for free plans - Watermark
WATERMARK = {
    'source': 'clients/ochre-1/231/advntr-blur-left-ogni.png',
    'top': '0',
    'height': '80',
    'width': '194',
    'left': '0',
    'alpha': '0.6'
}

STRIPE_PUBLIC_KEY = ENV('STRIPE_PUBLIC_KEY', '')
STRIPE_SECRET_KEY = ENV('STRIPE_SECRET_KEY', '')

DJSTRIPE_PLANS = {
    "FRD999": {
        "stripe_plan_id": "FRD999",
        "name": "Food Revolution Day Special ($9.99/mo)",
        "description": "LIMITED TIME Food Revolution Day Special",
        "price": 999,  # $9.99
        "currency": "usd",
        "interval": "month"
    },
    "PRO60": {
        "stripe_plan_id": "PRO60",
        "name": "Professional Account ($60 /mo)",
        "description": "Professional Account",
        "price": 6000,  # $60.00
        "currency": "usd",
        "interval": "month"
    },
}

EXTRA_ALLOWED_HOSTS = ENV('EXTRA_ALLOWED_HOSTS')
if EXTRA_ALLOWED_HOSTS:
    ALLOWED_HOSTS += EXTRA_ALLOWED_HOSTS.split(',')

UTILS_DIR = ENV('UTILS_DIR', default='bin')

ACCOUNT_CODES = ENV('ACCOUNT_CODES').split(',')

# Celery broker
BROKER_URL = ENV('BROKER_URL', False)
CELERY_RESULT_BACKEND = ENV('CELERY_RESULT_BACKEND', False)

CELERY_ACCEPT_CONTENT = ['pickle', 'json', 'msgpack', 'yaml']

import djcelery
djcelery.setup_loader()

CELERY_IMPORTS = ('base.encoding', )

SOUTH_MIGRATION_MODULES = {
    'djstripe': 'djstripe.south_migrations',
}



try:
    from settings_local import *

except ImportError:
    pass

# Set your DSN value
if not DEBUG:
    RAVEN_CONFIG = {
        'dsn': 'https://b5ab91feb30a457b819d8d6e19754609:6692b5341af34cc2bf274f9dc4e1fe77@app.getsentry.com/49645',
    }
