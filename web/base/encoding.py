from django.conf import settings
from zencoder import Zencoder
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
from django.template.loader import render_to_string
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404
from celery import task

import json

from base.models import *
from base.utils import *

import logging
logger = logging.getLogger(__name__)


@task()
def encode_ping():

    tag_cur  = get_file_contents( settings.PROJECT_ROOT + '/../tag.txt') 
    return tag_cur 
    

def encode_assets( assets, published_campaign=None ):

    if getattr( settings, 'BROKER_URL', False ):
        return encode_assets2.delay( assets, published_campaign=published_campaign )

    return encode_assets2( assets, published_campaign=published_campaign )

@task()
def encode_assets2( assets, published_campaign=None ):

    all_ids = ','.join( str(a.id) for a in assets) 

    client = Zencoder(settings.ZENCODER_API_KEY)
    num_errors  = 0
    num_success = 0

    logger.debug("Sending jobs for assets %s" % (all_ids))
    for asset in assets:

        try:
            BUCKET_AND_INPUT_FILE = '%s/%s' % ( settings.AWS_UPLOAD_BUCKET, asset.file_path)
            OUTPUT_PATH           = '%s/%s' % ( settings.AWS_STATIC_BUCKET, asset.campaign.getOutputPath())

            tmpl_outputs = render_to_string('zencoder_job.html', { 'asset' : asset, 'BUCKET_AND_INPUT_FILE' : BUCKET_AND_INPUT_FILE, 'OUTPUT_PATH' : OUTPUT_PATH } )
            json_outputs = json.loads(tmpl_outputs)

            headers = { "OCHRE-ASSET-ID" : str(asset.id), "Accept": "application/json"}
            if published_campaign:
                headers["OCHRE-PUBLISHING-ASSETS"] = all_ids
                headers["OCHRE-PUBLISHING-ID"]     = str(published_campaign.id)

            options = { "notifications" : [ { "format": "json", "url": settings.ZENCODER_CALLBACK, "headers" : headers }] }

            input_location = 's3://%s' % (BUCKET_AND_INPUT_FILE)

            response = client.job.create( input_location, outputs = json_outputs, options = options)
            if response.code != 201:
                raise Exception("Response code=%s body=%s input=%s output=%s" % (response.code, response.body, input_location, str(json_outputs).replace('\n', '')))
            else:
                logger.debug("Submitted job for asset=%s Zencoder Response code=%s body=%s" % (asset, response.code, response.body) )
                num_success += 1

                # save the job id
                asset.jobid = response.body.get('id' '')
                asset.job_error_notified=False
                asset.save()

        except Exception, inst:
            logger.debug("ERROR: encoding exception=%s" % (inst)) 
            num_errors += 1

    logger.debug("Done sending jobs for assets %s" % (all_ids))

    return (num_success, num_errors)


def encode_campaign( user, campaign, config, request=None ):
    if getattr( settings, 'BROKER_URL', False ):
        return encode_campaign2.delay( user, campaign, config )

    return encode_campaign2( user, campaign, config, request )


@task()
def encode_campaign2( user, campaign, config, request=None ):

    #######################################################################
    # Add zencoder jobs for unencoded videos 
    #######################################################################
    assets = UploadAsset.objects.filter( Q(campaign=campaign) | Q(campaign_id=settings.PLACEHOLDER_CAMPAIGN_ID), upload_successful=True, job_error_notified=False, mime_type__startswith='video' )

    videos_to_encode = []
    project_assets = {}
    for asset in assets:
        project_assets[asset.file_original]=asset

    for id in config:
        obj = config[id]
        if not isinstance( obj, dict):
            continue

        if obj.has_key('original_asset_filename') and project_assets.has_key(obj['original_asset_filename']):

            asset = project_assets[ obj['original_asset_filename'] ]

            logger.debug("Will encode asset=%s" % (asset))

            # add flv to config obj
            config[id]['mp4']     = asset.campaign.getOutputPath() + '/' + asset.file_id + '.mp4'
            config[id]['flv']     = asset.campaign.getOutputPath() + '/' + asset.file_id + '-high.flv'
            config[id]['flv_low'] = asset.campaign.getOutputPath() + '/' + asset.file_id + '-low.flv'
            config[id]['hls']['playlist'] = "%s/hls/%s-playlist.m3u8" % (asset.campaign.getOutputPath(), asset.file_id)

            # add hls objs
            if asset.duration > 0:

                num_10s  = math.floor(asset.duration)/10
                for n in xrange(0, int(num_10s)):
                    config[id]['hls']['segment_durations'].append(10)

                # add remainder
                remainder = math.ceil( asset.duration % 10 )
                if remainder > 0:
                    config[id]['hls']['segment_durations'].append(int(remainder))
            else:
                logger.error("Duration not set for asset %s" % (asset), extra={'request' : request})

            if not asset.encoded and (asset not in videos_to_encode):
                videos_to_encode.append(asset)


    ####################################
    # Encode linear file if provided
    if hasValue(config['options'], 'altMp4'):
        # search based on file_path since we didn't put the id in there
        file_path = config['options']['altMp4']

        try:

            asset = UploadAsset.objects.get(campaign=campaign, file_path=file_path)
            if not asset.encoded:
                logger.debug("Will encode asset=%s" % (asset))
                asset.is_linear = True
                videos_to_encode.append(asset) 

                # also set altHLS in the config
                config['options']['altHLS'] = campaign.getOutputPath() + '/hls/' + asset.file_id + '-playlist.m3u8'
                config['options']['altMobile'] = campaign.getOutputPath() + '/' + asset.file_id + '-mobile.mp4'

        except UploadAsset.DoesNotExist, inst:
            logger.error("Can't find altMp4 file_path in db. %s" % (file_path), extra={'request' : request})
    ####################################

    config_str = json.dumps( config, indent=4, sort_keys=False, separators=(',', ': ') )

    published_campaign = PublishedCampaign.Create( user=user, campaign=campaign, config_data=config_str)

    if len(videos_to_encode) == 0:
        finalize_publishing2( published_campaign )
    else:
        encode_assets2( videos_to_encode, published_campaign=published_campaign )
       

def finalize_publishing(published_campaign):

    if getattr( settings, 'BROKER_URL', False ):
        return finalize_publishing2.delay(published_campaign)

    return finalize_publishing2(published_campaign)


@task()
def finalize_publishing2(published_campaign):

    logger.debug("Finalizing published campaign %s" % (published_campaign))
    published_campaign.finalize()



@csrf_exempt
def zencoder_callback( request ):
    ###################
    # TEST 
    #curl -X POST -H "OCHRE-PUBLISHED: 1" -H "OCHRE_ASSET_ID: 98" -d "{\"output\" : {\"duration_in_ms\" : \"1234\", \"width\":\"640\", \"height\":\"480\", \"file_size_in_bytes\" : \"2048\"}}"  http://localhost.local:8000/zencallback
    ###################


    if request.META.has_key('HTTP_OCHRE_ASSET_ID'):
        assetId = request.META['HTTP_OCHRE_ASSET_ID']
    else:
        logger.error("zencoder_callback: Missing asset/campaign id BODY=%s" %(request.body), extra={'request' : request})
        return getJsonResponse({})

    try:
        asset = UploadAsset.objects.get(pk=int(assetId))
    except UploadAsset.DoesNotExist, inst:
        logger.error("zencoder_callback: Asset not found for id=%s BODY=%s" % (assetId, request.body), extra={'request' : request})
        return getJsonResponse({})

    published_id = request.META.get('HTTP_OCHRE_PUBLISHING_ID', None)
    data  = json.loads( request.body )

    if published_id:

        try:
            published_campaign = PublishedCampaign.objects.get(pk=int(published_id))
        except PublishedCampaign.DoesNotExist, inst:
            logger.error("Can't find PublishedCampaign for id=%s assetId=%s" % (published_id, assetId), extra={'request' : request})

        if data['job']['state'] == 'failed':
            asset.notifyUserError( data['job'].get('error_message', 'Unknown') )
            logger.error("state=failed assetId=%s published_campaign=%s body=%s" % (assetId, published_campaign.id, request.body), extra={'request' : request})
            return getJsonResponse({})


        published_assets = request.META.get('HTTP_OCHRE_PUBLISHING_ASSETS', None)
        if not published_assets:
            logger.error("Missing published_assets for published_id=%s" % (published_id), extra={'request' : request})
            return getJsonResponse({})


        asset.encoded   = True
        asset.published = True
        asset.save()

        asset_ids        = published_assets.split(',')
        unencoded_assets = UploadAsset.objects.filter(id__in=asset_ids, encoded=False, mime_type__startswith='video')
        if unencoded_assets.count() == 0:
            logger.debug("All video assets encoded for campaign %s" % (asset.campaign.id)) 

            finalize_publishing( published_campaign )
         

    else:

        try:
                if data['output']['state'] == 'failed':
                    logger.error("state=failed assetId=%s body=%s" % (assetId, request.body), extra={'request' : request})
                    asset.notifyUserError( data['output'].get('error_message', 'Unknown') )
                    return getJsonResponse({})

                if request.META.has_key('HTTP_OCHRE_MANUAL_ENCODE'):
                    asset.encoded   = True
                    asset.published = True

                asset.duration  = int(data['output'].get('duration_in_ms', 0))/1000
                asset.width     = int(data['output'].get('width', None))
                asset.height    = int(data['output'].get('height', None))
                asset.size_kb   = int(data['output'].get('file_size_in_bytes', 0))/1000

                data_input = data.get('input', None)
                if data_input:
                    asset.original_width  = data_input.get('width', None)
                    asset.original_height = data_input.get('height', None)
                else:
                    logger.warn("Missing input in zencoder body=%s" % (data))

                # update asset.thumbnails[] if data exists
                if hasValue(data['output'], 'thumbnails') and len(data['output']['thumbnails']) > 0:
                    for data in data['output']['thumbnails']:
                        if hasValue(data, 'images') and hasValue(data, 'label') and data['label'] == 'thumb':
                            asset.thumbnails = []
                            for image in data['images']:
                                asset.thumbnails.append( os.path.basename(image['url']) )



                asset.save()
        except TypeError, inst:
            logger.error("TypeError %s assetId=%s body=%s" % (inst, assetId, request.body), extra={'request' : request})
            raise

    logger.debug("Successful zencoder_callback=%s\nbody=%s" % (request.META, request.body))

    return getJsonResponse({})




