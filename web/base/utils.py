from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.conf import settings
import sendgrid
import json
import os
import requests

import logging
logger = logging.getLogger(__name__)


def subscribe_mailchimp_list(list_id, user):
    from models import Profile
    api_endpoint = settings.MAILCHIMP_BASE_API_ENDPOINT +'/lists/%s/members/' % str(list_id)

    try:
        profile = Profile.objects.get(user=user)
    except Profile.DoesNotExist:
        profile = None

    payload = {
        "email_address": user.email,
        "status": "subscribed",
        "merge_fields": {
            "FNAME": user.first_name,
            "LNAME": user.last_name,
        }
    }

    if profile:
        # Mailchimp not handling None values in mail merge fields
        if profile.dob:
            payload['merge_fields']['MMERGE3'] = profile.dob.strftime("%m/%d")  # MC Birthday field

        if profile.occupation:
            payload['merge_fields']['MMERGE4'] = profile.occupation

        if profile.location:
            payload['merge_fields']['MMERGE5'] = profile.location

        if profile.website:
            payload['merge_fields']['WEBSITE'] = profile.website

    headers = {'content-type': 'application/json'}
    r = requests.post(api_endpoint, data=json.dumps(payload), auth=('apikey', settings.MAILCHIMP_API_KEY), headers=headers)

    # ignore errors but log them
    if r.status_code != 200:
        print "Error: {} {}".format(str(r.status_code), r.reason)
        logger.info("Error adding email to subscriber list %s" % json.dumps(r.json(), indent=4))
    else:
        logger.debug("Added mailchimp subscriber %s to list ID %s" % (user.email, list_id))



def send_email( to_emails, subject, body, from_email, attachments=[]):

    logger.debug("Sending email to %s" % (to_emails))
    try:
    
        s = sendgrid.SendGridClient(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD, secure=True)

        message = sendgrid.Mail(subject=subject, html=body, text=body, from_email=from_email )

        for email in to_emails:
            message.add_to( email )

        for attachment in attachments:
            message.add_attachment( os.path.basename( attachment), attachment )

        (code, msg) = s.send(message)
        if code != 200:
            raise Exception(msg)
        logger.debug("Sent email to %s" % (to_emails))

    except Exception, inst:
        logger.error("Error sending email to %s inst=%s" % (to_emails, inst))



def getJsonResponse( data=dict(), serializer=None ):
	response = HttpResponse(mimetype='application/json') #set javascript mimetype? see http://www.prototypejs.org/api/ajax/request
	response.write(json.dumps( data, cls=serializer ))

	return response


def hasValue( obj, key):
    if not obj.has_key(key):
        return False

    val = obj[key]
    if val == None or val == '':
        return False

    return True


def get_file_contents(filename):
    if os.path.exists(filename):
        try:
            f = open(filename, 'r')
            tag = f.read().strip()
            f.close()
            return tag

        except Exception:
            pass

    return ''


