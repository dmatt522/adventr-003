from datetime import datetime, timedelta, date
from datetime import time as dtime
import time
from django.conf import settings
from urlparse import urlparse
from collections import Mapping, Iterable
from base.models import *
from django.db.models import Q
import dj_database_url
import os, re
import operator
import pymongo
import json
import pytz
from base.utils import hasValue


from ua_parser import user_agent_parser


import logging
logger = logging.getLogger(__name__)


REMOVE_SUBDOMAINS = ['www.', 'http.', 'www1.', 'www2.', 'www3.']
NETLOC_REPLACEMENTS = [ (r'^ochre.*','Ochre'),(r'^fb$','facebook.com'),(r'^twitter$','twitter.com') ]

def convert(data):
    if isinstance(data, unicode):
        return str(data)
    elif isinstance(data, Mapping):
        return dict(map(convert, data.iteritems()))
    elif isinstance(data, Iterable):
        return type(data)(map(convert, data))
    else:
        return str(data)



class AggregateByDateAnalytics():


    def initForDevice( self, device, start_date, end_date, rendered):
        dates = [ ]
        length = (end_date - start_date).days
        for i in xrange(0, length):

            next_date = start_date + timedelta(i)
            dates.append( [next_date.strftime("%F"), 0] )
                
        label = u''.join( device).encode('utf-8')

        return { 'label': label, 'label2' : label.lower(), 'data' : dates, 'color' : len(rendered) }


    def trimDates( self, data, forwards ):

        deleting = True
        while deleting:
            deleting = False
            idx = 0 if forwards else (len(data) - 1)
            while data[idx][1] < 1:
                del data[idx]
                deleting = True
                break

            if not deleting:
                break


    def renderData( self, analyticsid, segment_type, segment, unique, equals_text, start_date, end_date):
        if analyticsid == '1257-1163-1':
            analyticsid = '1-14-1'

        logger.debug("AggregateByDateAnalytics %s eq='%s' %s %s" % (analyticsid, equals_text, start_date, end_date))

        if analyticsid:
            q = Q(analyticsid=analyticsid) & Q(date__gte=start_date)
        else:
            q = Q(date__gte=start_date)

        q_segment = segment_type + segment + ('Unique' if unique else '')

        if equals_text:
            points = DataPoint.objects.filter( q, datatype=q_segment, key1=equals_text, date__lte=end_date)
        else:
            points = DataPoint.objects.filter( q, datatype=q_segment, date__lte=end_date)

        rendered = {}

        campaign_keys = {}
        if not analyticsid:
            for campaign in VideoCampaign.objects.all().select_related():
                campaign_keys[ '%s-%s-1' % (campaign.client.id, campaign.id) ] = '%s (%s)' % (campaign.name, campaign.client.name)

 
        for row in points:

            datestr = row.date.strftime("%F")
            if analyticsid:
                label = row.key1
            else:
                label = campaign_keys.get(row.analyticsid, 'Unknown (%s)' % (row.analyticsid))

            if not rendered.has_key(label):
                rendered[label] = self.initForDevice( label, start_date, end_date, rendered)

            for info in rendered[label]['data']:
                if info[0] == datestr:
                    info[1] += float(row.value)


        # trim empty values from front and end
        for k,v in rendered.items():
            self.trimDates( v['data'], True)
            self.trimDates( v['data'], False) 


        table = {}
        if segment_type == 'Views':
            v_key = q_segment
            d_key = 'Duration' + segment + ('Unique' if unique else '')
        else:
            d_key = q_segment
            v_key = 'Views' + segment + ('Unique' if unique else '')

        
        # ViewsSource
        if equals_text:
            views_result = DataPoint.objects.filter( q, datatype=v_key, key1=equals_text, date__gte=start_date, date__lt=end_date)
        else:
            views_result = DataPoint.objects.filter( q, datatype=v_key, date__gte=start_date, date__lt=end_date)

        for view in views_result:

            key = view.key1 if analyticsid else campaign_keys.get(view.analyticsid, 'Unknown (%s)' % (view.analyticsid))
            data = None

            campaign_id = view.getCampaignId()
            if not campaign_id:
                continue

            if not table.has_key(key):
                table[key] = {'plays' : 0, 'duration' : 0, 'duration_count' : 0, 'campaign_id' : campaign_id}
            data = table[key]

            data['plays'] += float(view.value)


        # DurationSource
        if equals_text:
            duration_result = DataPoint.objects.filter( q, datatype=d_key, key1=equals_text, date__gte=start_date, date__lt=end_date)
        else:
            duration_result = DataPoint.objects.filter( q, datatype=d_key, date__gte=start_date, date__lt=end_date)

        for view in duration_result:

            key = view.key1 if analyticsid else campaign_keys.get(view.analyticsid, 'Unknown (%s)' % (view.analyticsid))

            data = table.get(key, None)
            if not data:
                continue
            data['duration']       += float(view.value)
            data['duration_count'] += 1


        #logger.debug("DATA=%s" % (table))

        #logger.debug("RENDERED=%s" % (rendered)) 
            
        return (rendered, table)


class ClickDataAnalytics():

    def renderData( self, analyticsid, filter_type, equals_text, autoChoose, start_date, end_date, config_data ):

        if analyticsid == '1257-1163-1':
            analyticsid = '1-14-1'


        config_data['Load']     = {}
        config_data['Finished'] = {'id' : 'Finished', 'next_video_id' : 'Finished', 'video_choices' : [], 'original_asset_thumbnail' : 'advntr/assets/adventr_completed.png' }

        logger.debug("ClickDataAnalytics filter_type=%s equals_text=%s autoChoose=%s" % (filter_type, equals_text, autoChoose))

        if equals_text:
            points = DataPoint.objects.filter( analyticsid=analyticsid, datatype=filter_type, key1=equals_text, date__gte=start_date, date__lte=end_date)
        else:
            points = DataPoint.objects.filter( analyticsid=analyticsid, datatype=filter_type, date__gte=start_date, date__lte=end_date)
        
        for click in points:

            total = float(click.value)

            if equals_text:
                choiceInfo = click.key2
            else:
                choiceInfo = click.key1

            if not choiceInfo:

                logger.debug("\nMissing choiceInfo in result for %s\n" % (click))
                continue


            parts = choiceInfo.split('#')

            autoChooseChoice = 0
            referringChoice = None
            clip = None
            if len(parts) > 2:
                (referringChoice, clip, autoChooseChoice) = parts
            else:
                logger.error("Expecting > 2 len for parts in click=%s match=%s" % (click, match))
                clip = choiceInfo

            if autoChoose != -1 and clip != 'Load' and clip != 'Finished' and clip != 'Replay' and clip != config_data['options']['initial']:
                if autoChoose != int(autoChooseChoice):
                    continue 
            
            if not config_data.has_key(clip):
                #logger.debug("config does not have %s" %(clip))
                continue


            if referringChoice == None or referringChoice == '' or referringChoice == "None" or referringChoice == 0:

                if clip == 'Load':
                    referringChoice = 'Load' # count Load events as views of itself
                else:
                    continue;


            if not config_data[clip].has_key('clicks'):
                config_data[clip]['clicks'] = {}

            if not config_data[clip]['clicks'].has_key(referringChoice):
                config_data[clip]['clicks'][referringChoice] = int(total)
            else:
                config_data[clip]['clicks'][referringChoice] += int(total);


        #logger.debug("config=%s" % (config_data))

        return config_data




def get_date(today=None, daysback=0):
    
    if not today:
        today = datetime.now().date()

    today = datetime.combine( today, dtime(0, 0))
    if not daysback:
        return today

    return today - timedelta(daysback) 


    
class AnalyticsManager():

    mongo_db = None

    modules = []

    def __init__(self, *args, **kwargs):
        use_db = dj_database_url.parse(settings.MONGODB_URL)['NAME']

        self.modules = []

        try:

            mongo_db_c = pymongo.MongoClient( host=settings.MONGODB_URL )
            if mongo_db_c is None:
                logger.error("Unable to connect to mongodb. mongo_db_c is None")

            self.mongo_db = mongo_db_c[use_db]

        except Exception, inst:
            logger.error("mongodb connection problem with database " + use_db + ". " + " exception: " + str(type(inst)) + " " + str(inst) )


    def insert( self, table, obj):

        e = self.mongo_db[table].insert( obj )
        return e

    def find( self, table, obj ):
        docs = self.mongo_db[table].find( obj )
        return docs


    def convertJsonFile( input_filename, output_filename ):

        out = open(output_filename, 'w')

        skip = ['mp_lib', 'pageUrl', 'ua', 'token', 'swfUrl', 'ip', 'referrer' ]

        # convert time to datetime
        
        f = open(input_filename, 'r')
        for line in f:
            line = line.strip()
            if not line:
                continue

            try:
                item  = json.loads( line )
                props = item.get('properties', False)

                if not props:
                    logger.debug("missing properties in %s" % (line))
                    continue


                timestamp = item['properties']['time']
                ua        = item['properties'].get('ua', None)

                dt = datetime.fromtimestamp( int(timestamp) )
                data = {'datetime' : {'$date' : int(timestamp) }, 'ua_os' : None, 'ua_browser' : None }

                data['source'] = getNetloc(props)

                #########################################################
                # Add UA
                if ua and len(ua) > 1 and ua != 'undefined':
                    try:
                        res     = user_agent_parser.Parse(ua)
                        if res:
                            os      = str(res.get('os', {}).get('family', '') )
                            browser = str(res.get('user_agent', {}).get('family', '') )

                            data['ua_os']      = os
                            data['ua_browser'] = browser

                    except Exception, inst:
                        logger.debug("addMixpanelData: Parsing UA %s" % (inst))
                #########################################################


                for k,v in item['properties'].items():
                    if k in skip:
                        continue

                    if k.find('$') == 0:
                        data[k[1:]] = v
                        del data[k]

                    else:
                        data[k] = v


                out.write(json.dumps(data) + '\n')

            except Exception, inst:
                logger.debug("Error importing line %s, inst=%s" % (line, inst))


        f.close()
        out.close()



def getUAInfo( ua ):
    if ua and len(ua) > 1 and ua != 'undefined':
        try:
            res = user_agent_parser.Parse(ua)
            if res:
                os      = str(res.get('os', {}).get('family', '') )
                browser = str(res.get('user_agent', {}).get('family', '') )

                return (os, browser)

        except Exception, inst:
            logger.debug("addMixpanelData: Parsing UA %s" % (inst))

    return ('Unknown', 'Unknown')



def getFlashVersion( flash_str ):
    idx = flash_str.find(' ')
    if idx >= 0:
        parts = flash_str[idx+1:].split(',')

        if len(parts) > 1:
            return parts[0] + '.' + parts[1]

    return 'Unknown'

def getNetloc(data):

    if not hasValue(data, 'referrer'):
        if hasValue(data, 'source'):
            data['referrer'] = data['source']
        else:
            return 'Unknown'

    netloc = getNetloc2(data['referrer'])

    if not netloc:
        return 'Unknown'

    for r in NETLOC_REPLACEMENTS:
        if re.match( r[0], netloc):
            return r[1]

    return netloc
        

def getNetloc2(referrer):

    try:
        if not referrer:
            return ''

        u = urlparse(referrer)
        if not u:
            return referrer

        if u.netloc != None:
            parts = u.netloc.split('.')
            if len(parts) > 2:
                for x in REMOVE_SUBDOMAINS:
                    if u.netloc.find(x) == 0:
                        return u.netloc.replace(x, '')

                return u.netloc

        if len(u.netloc) == 0:
            if not u.path:
                return referrer

            idx = u.path.find('/')
            if idx > 0:
                return u.path[0:idx]
            return u.path

        return u.netloc
    except Exception, inst:
        print("Exception in getNetloc %s inst=%s" % (referrer, inst))

    return referrer


