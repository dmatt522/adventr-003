from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from optparse import make_option
import os, string
from base.analytics import *

import logging
logger = logging.getLogger(__name__)

class Command(BaseCommand):
    args = ''
    help = ''

    option_list = BaseCommand.option_list + (
        make_option('--filename', '-f', dest='filename', help='Filename', default=False),
        make_option('--output', '-o', dest='output', help='Output filename', default=False),
        make_option('--table', '-t', dest='table', help='Table : views or events', default=False),
    )


    def handle(self, *args, **options):

        filename = options.get('filename')
        output   = options.get('output')
        table    = options.get('table')

        logger.info("analytics_load_json filename=%s output=%s table=%s" % (filename, output, table))

        if not os.path.exists(filename):
            logger.error("File %s does not exist" % (filename))
            return 1

        am = AnalyticsManager()
        
        if table == 'events':
            am.parseClickData( filename, output ) 
        elif table == 'views':
            am.parseViewData( filename, output ) 

