import os, sys
import mmap, json


file_in = sys.argv[1]
file_out = sys.argv[2]
t = int(sys.argv[3])


out = open(file_out, 'w')

f = open(file_in, 'r+b')
map = mmap.mmap(f.fileno(), 0, prot=mmap.PROT_READ)
for line in iter(map.readline, ""):
    line = line.strip()
    if not line:
        continue

    try:
        item  = json.loads( line )
        date  = item.get('datetime', {}).get('$date', None)

        if not date:
            print 'Missing date in %s' % (line)
            continue

        d = int(date)
        if d > t:
            #print 'Skipping %s > %s' % (d, t)
            continue

        
        out.write( line + '\n' )

    except Exception, inst:
        print "Exception: %s" % (inst)




