from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from datetime import datetime
from optparse import make_option
import os, string, csv
import pickle
from base.analytics import *
import mmap
import pygeoip
import logging
logger = logging.getLogger(__name__)


SKIP_FIELDS = set(['mp_lib', 'pageUrl', 'token', 'ip', 'time', 'http_referrer', 'computed', 'ad_id', 'referringChoice', 'clip', 'referrer', 'referrer_old', 'time', 'Video', 'choiceCount', 'clipRoute', 'action', 'ua', 'mp_country_code', 'autoChoose'])

VIDEO_ANALYTICS_IDS = {'THE RETURNED :30s Trailer' : '68-145-1', 'Stylitics' : '69-89-1', 'Chevy V2' : '1-125-1', 'Z&V': '1-1-1', 'Amazing Spiderman' : '1-14-1', 'Videology-ZnV' : '1-112-1', 'Video 41' : '1-122-1', 'TheAmazingSpiderman Trailer' : '1-14-1', 'Wale' : '1-80-1', 'Indigo' : '1-11-1', 'Indigo Devo' : '1-49-1', 'Z&V-Linear' : '1-66-1', 'Hunger Games' : '1-57-1', 'Ochre Cinematic' : '1-187-1', 'Ochre.com' : '1-64-1', 'Promo' : '1-64-1', 'WebMD' : '1-72-1', 'A$AP ROCKY' : '1-188-1', 'A$AP - PMW' : '1-188-1'}



class GeoCode():

    regionCodes = {}
    geo         = None

    def __init__(self, dbLoc, regionCodeLoc=None):

        self.geo = pygeoip.GeoIP(dbLoc, pygeoip.MEMORY_CACHE)

        self.regionCodes = {}

        with open(regionCodeLoc, 'r') as csvfile:
            c = csv.reader(csvfile)

            for row in c:
                if self.regionCodes.has_key(row[0]):
                    d = self.regionCodes[row[0]]
                else:
                    self.regionCodes[row[0]] = {}
                    d = self.regionCodes[row[0]]
                d[row[1]] = row[2]

 
    def getLocationInfo(self, ip):

        if ip:
            try:
                geo_data = self.geo.record_by_addr(ip)
                if geo_data:
                    country_code = geo_data.get('country_code', None)
                    region  = geo_data.get('region_name', None)

                    region_name = None
                    if country_code and region:
                        region_name = self.regionCodes.get( country_code, {}).get( region, None)

                    if not region_name:
                        region_name = geo_data.get('region_name', 'Unknown')
                        
                    return (geo_data.get('city', 'Unknown'), region_name, geo_data.get('country_name', 'Unknown') ) 

            except pygeoip.GeoIPError, inst:
                logger.warn("Unable to find ip data from %s, inst=%s" % (item, inst))
            except UnicodeDecodeError, inst:
                logger.warn("UnicodeDecodeError: %s" % (inst))

        return ('Unknown', 'Unknown', 'Unknown')
           

class Command(BaseCommand):
    args = ''
    help = ''

    option_list = BaseCommand.option_list + (
        make_option('--eventsfile', '-e', dest='eventsfile', help='Events file', default=False),
        make_option('--engagementfile', '-g', dest='engagementfile', help='Engagement file', default=False),
        make_option('--outputfile', '-o', dest='outputfile', help='Output file', default=False),
        make_option('--dryrun', '-d', dest='dryrun', help='Dry run', default=False, action="store_true"),
        make_option('--durations', '-x', dest='durations', help='durations', default=False),
        make_option('--durations_only', '-z', dest='durations_only', help='Durations Only', default=False, action="store_true"),
    )


    def handle(self, *args, **options):

        eventsfile      = options.get('eventsfile')
        engagementfile  = options.get('engagementfile')
        outputfile      = options.get('outputfile')
        dryrun          = options.get('dryrun')


        ###################################################################
        ## engagement file
        durations = {}


        geo = GeoCode( settings.MAXMIND_CITY_DB, settings.MAXMIND_REGION_CODES)


        if not options.get('durations_only') and options.get('durations'):

            logger.debug("Loading %s" % (options.get('durations')))
            with open(options.get('durations'), 'rb') as handle:
                durations = pickle.load(handle)

            logger.debug("Read %s durations"% (len(durations)))

        else:

            skip_bad_durations = 0
            f = open(engagementfile, 'r')
            line = f.readline()
            while line:

                line = line.strip()
                if not line:
                    line = f.readline()
                    continue

                try:
                    if line[0] != '{':
                        item  = json.loads( line[ line.find('{'): ] )
                    else:
                        item  = json.loads( line )

                    d = item.get('duration', 0)

                    if d < 1 or d > 300:
                        #logger.debug("bad duration=%s" % (item))
                        skip_bad_durations += 1
                        line = f.readline()
                        continue


                    viewId = item.get('viewId', None)
                    if not viewId:
                        #logger.debug("Missing viewId in %s" % (line))
                        line = f.readline()
                        continue

                    durations[viewId] = d

                except Exception, inst:
                    logger.warn("Exception reading engagement file: %s" % (inst))

                line = f.readline()

            f.close()

            logger.debug("Read %s engagement items. skipped %s bad durations" % (len(durations), skip_bad_durations ))

            filename = 'durations.map' if not options.get('durations') else options.get('durations')

            with open(filename, 'wb') as handle:
                pickle.dump(durations, handle)

            logger.debug("Wrote %s" % (filename))

 
        if options.get('durations_only'):
            return

        ###################################################################
        ###################################################################
        # events file

        events = {}
        missingDurations = 0

        f = open(eventsfile, 'r')
        line = f.readline()
        count = 0
        total = 0
        while line:

            count += 1
            if count == 5000:
                logger.debug("Read %s lines" % (total))
                total += count
                count = 0


            line = line.strip()
            if not line:
                line = f.readline()
                continue

            if line[0] != '{':
                s = line[ line.find('{'): ]
                item  = json.loads( s )
            else:
                item  = json.loads( line )

            clip        = item.get('clip', None)
            date        = item.get('datetime', {}).get('$date', None)
            analyticsId = item.get('analyticsId', None)
            link        = item.get('link', None)
            video       = item.get('Video', None)
            viewId      = item.get('viewId', None)
            di          = item.get('distinct_id', None)
            autoChoose  = item.get('autoChoose', 0)
            referringChoice = item.get('referringChoice', None)

            if not analyticsId and video:
                if video in VIDEO_ANALYTICS_IDS:
                    item['analyticsId'] = VIDEO_ANALYTICS_IDS[video]
                else: 
                    line = f.readline()
                    continue


            if not clip:
                logger.debug("Missing clip in %s" % (line))
                line = f.readline()
                continue


            if not viewId or not di:
                logger.debug("Missing viewId or distinct_id in events file. %s" % (line))
                line = f.readline()
                continue  


            if autoChoose == False:
                autoChoose = 0
            elif autoChoose == True:
                autoChoose = 1 

            referringChoice = '' if referringChoice == None else referringChoice #None may already be in dict, handle it here

            obj = item
            if events.has_key( viewId ):
                obj = events[viewId]
            else:
                events[viewId] = obj

            choices = []
            if obj.has_key('choices'):
                choices = obj['choices']
            else:
                obj['choices'] = choices

            found = False
            for a in choices:
                parts = a.split('#')
                if clip == parts[1]:
                    found = True
                    break

            if found:
                #logger.debug("Found dupe clip %s in %s" % (clip, obj))
                line = f.readline()
                continue
        

            choiceStr = '%s#%s#%s' % (referringChoice, clip, autoChoose)
            choices.append( choiceStr )

            obj['source']       = getNetloc(obj)
            
            if not durations.has_key(viewId):
                #logger.debug("Missing duration for %s" % (viewId))
                missingDurations += 1
            else:
                obj['duration'] = durations[viewId]

            if obj.has_key('mp_country_code'):
                obj['country'] = obj['mp_country_code']

            if not obj.has_key('flash'):
                obj['flash'] = getFlashVersion( obj.get('flashVersion', '') )
            
            if not obj.has_key('city'):
                (city, region, country) = geo.getLocationInfo( obj.get('ip', None))
                obj['city']    = city
                obj['region']  = region
                obj['country'] = country

            if not obj.has_key('ua_os'):
                (os, browser) = getUAInfo( obj.get('ua', None))
                obj['ua_os']      = os
                obj['ua_browser'] = browser

            for k in SKIP_FIELDS:
                if obj.has_key(k):
                    del obj[k]


            line = f.readline()

        logger.debug("Done reading %s events" % (len(events)))

        f.close()

        out = open(outputfile, 'w')
        count = 0
        for k,v in events.iteritems():
            try:
                out.write(json.dumps(v) + '\n')
                count += 1
            except Exception, inst:
                logger.warn("Exception writing events: %s" % (inst))

        out.close()

        logger.debug("Wrote %s events. Missing %s durations" % (count, missingDurations))

