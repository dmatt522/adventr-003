from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from optparse import make_option
import os, string, datetime, time
from base.analytics import *
from base.models import *

import logging
logger = logging.getLogger(__name__)



SUNDANCE_ROLLUP  = set( ['facebook.com', 'sundancechannel.com', 'twitter.com', 'adweek.com', 'marketer.ge'] )
SUNDANCE_AD_NAME = 'THE RETURNED :30s Trailer Ad'

LINUXES = set([ 'Fedora', 'Slackware', 'FreeBSD', 'Ubuntu', 'Debian', 'SUSE', 'CentOS', 'Mageia', 'Linux', 'Linux Mint'])

US_STATES = {'AL':'Alabama', 'AK':'Alaska', 'AZ':'Arizona', 'AR':'Arkansas', 'CA':'California', 'CO':'Colorado', 'CT':'Connecticut', 'DE':'Delaware', 'FL':'Florida', 'GA':'Georgia', 'HI':'Hawaii', 'ID':'Idaho', 'IL':'Illinois', 'IN':'Indiana', 'IA':'Iowa', 'KS':'Kansas', 'KY':'Kentucky', 'LA':'Louisiana', 'ME':'Maine', 'MD':'Maryland', 'MA':'Massachusetts', 'MI':'Michigan', 'MN':'Minnesota', 'MS':'Mississippi', 'MO':'Missouri', 'MT':'Montana', 'NE':'Nebraska', 'NV':'Nevada', 'NH':'New Hampshire', 'NJ':'New Jersey', 'NM':'New Mexico', 'NY':'New York', 'NC':'North Carolina', 'ND':'North Dakota', 'OH':'Ohio', 'OK':'Oklahoma', 'OR':'Oregon', 'PA':'Pennsylvania', 'RI':'Rhode Island', 'SC':'South Carolina', 'SD':'South Dakota', 'TN':'Tennessee', 'TX':'Texas', 'UT':'Utah', 'VT':'Vermont', 'VA':'Virginia', 'WA':'Washington', 'WV':'West Virginia', 'WI':'Wisconsin', 'WY':'Wyoming', 'AS':'American Samoa', 'DC':'District of Columbia', 'GU':'Guam', 'MH':'Marshall Islands', 'MP':'Northern Mariana Islands', 'PW':'Palau', 'PR':'Puerto Rico', 'VI':'Virgin Islands'}


class Command(BaseCommand):
    args = ''
    help = ''

    option_list = BaseCommand.option_list + (
        make_option('--filename', '-f', dest='filename', help='Filename', default=False),
        make_option('--dryrun', '-d', dest='dryrun', help='dryrun', default=False, action="store_true"),

    )


    def handle(self, *args, **options):

        filename = options.get('filename')
        dryrun   = options.get('dryrun')

        logger.info("analytics_aggregate filename=%s dryrun=%s" % (filename, dryrun))

        data = {}

        f = open(filename, 'r')
        line = f.readline()
        badvalues = 0
        while line:

            try:

                line = line.strip()
                if not line:
                    line = f.readline()
                    continue

                item  = json.loads( line )


                t = item.get('datetime', {}).get('$date', None)
                date = datetime.utcfromtimestamp(t/1000)
                date_str = date.strftime('%F')

                analyticsId = item['analyticsId']

                analyticsData = {}

                if not data.has_key(analyticsId):
                    data[analyticsId] = analyticsData
                else:
                    analyticsData = data[analyticsId]

                d = {}

                if not analyticsData.has_key(date_str):
                    analyticsData[date_str] = d
                else:
                    d = analyticsData[date_str]


                duration     = makestr( item.get('duration', 0))
                browser      = makestr( item.get('ua_browser',''))
                os           = makestr( item.get('ua_os',''))
                city         = makestr( item.get('city',''))
                region       = makestr( item.get('region',''))
                flashVersion = makestr( item.get('flash',''))
                distinct_id  = makestr( item.get('distinct_id',''))
                viewId       = makestr( item.get('viewId',''))
                choices      = item.get('choices', None)
                swfUrl       = item.get('swfUrl', None)


                location = ''
                source      = makestr( item.get('source', ''))

                if source.find('localhost') == 0:
                    line = f.readline()
                    continue 

                if analyticsId == '68-145-1' or analyticsId == '68-180-1':
                    location = region

                    if region:
                        if region in US_STATES:
                            location = US_STATES[region]
                        elif len(region) < 4:
                            location = ''

                    if analyticsId == '68-145-1' and ( source not in SUNDANCE_ROLLUP or ( swfUrl and swfUrl.find('AdvntrPlayer.swf') < 0) ):
                        source = SUNDANCE_AD_NAME

                elif city and region:
                    if city == 'Unknown':
                        location = region
                    elif region == 'Unknown':
                        location = 'Unknown'
                    else:
                        location = city + ', ' + region


                if duration < 1 or len(choices) < 2:
                    badvalues += 1
                    line = f.readline()
                    continue 


                if os and os in LINUXES:
                    os = 'Linux'

                if not hasLoad(choices) and analyticsId == '68-145-1':
                    line = f.readline()
                    continue 


                # don't add if no Load found
                for choice in choices:
                    self.increment( d, 'Funnel', choice)
                    self.increment( d, 'FunnelSource',  source, key2=choice)
                    self.increment( d, 'FunnelRegion',  region, key2=choice)
                    self.increment( d, 'FunnelBrowser',  browser, key2=choice)
                    self.increment( d, 'FunnelOS',  os, key2=choice)


                self.increment( d, 'SourceRegion', source, key2=location)
                self.increment( d, 'SourceOS', source, key2=os)
                self.increment( d, 'SourceBrowser', source, key2=browser)

                self.incrementUnique( d, 'Views',  viewId)
                self.increment( d, 'ViewsSource',  source)
                self.increment( d, 'ViewsRegion',  location)
                self.increment( d, 'ViewsOS',  os)
                self.increment( d, 'ViewsBrowser', browser)
                self.increment( d, 'ViewsFlash',   flashVersion)


                self.duration(d, 'Duration', duration, analyticsId)
                self.duration(d, 'DurationSource', duration, analyticsId, key=source)
                self.duration(d, 'DurationRegion', duration, analyticsId, key=location)
                self.duration(d, 'DurationOS', duration, analyticsId, key=os)
                self.duration(d, 'DurationBrowser', duration, analyticsId, key=browser)
                self.duration(d, 'DurationFlash', duration, analyticsId, key=flashVersion)


                self.incrementUnique( d, 'Unique', distinct_id)

                if not choices or not isinstance( choices, list):
                    logger.warn("Bad choices for %s" % (item))
                    line = f.readline()
                    continue


            except Exception, inst:
                logger.warn("Exception reading lines: %s" % (inst))

            line = f.readline()

        # count uniques

        logger.debug("Read %s dates. Skipped %s bad items" % (len(data), badvalues))

        if dryrun:
            logger.debug("data=%s\n\n\n" % (data))
            return

        for analyticsId, analyticsData in data.iteritems():

            for date_str, date_obj in analyticsData.iteritems():

                date = datetime.utcfromtimestamp( time.mktime( time.strptime( date_str, "%Y-%m-%d") ) )
                
                for datatype, values in date_obj.iteritems():

                    if isinstance( values, list):

                        if isinstance(values[0], int):
                            d = values[0] / values[1]
                        else:
                            d = values[1]

                        #logger.debug("%s >>> %s %s=%s" % (analyticsId, date_str, datatype, d ))
                        dp = DataPoint.objects.create( analyticsId=analyticsId, datatype=datatype, value=float(d), date=date)

                    else:
                        for label, value in values.iteritems():
                            
                            d = 0
                            if isinstance( value, int):
                                d = value
                            elif isinstance( value, list):
                                if isinstance(value[0], int):
                                    d = float( value[0] / value[1]) 
                                else:
                                    d = value[1]
                            elif isinstance( value, dict):
                                
                                for k,v in value.iteritems():

                                    if isinstance( v, list):
                                        if isinstance(v[0], int):
                                            d = v[0] / v[1]
                                        else:
                                            d = v[1]

                                        #logger.debug("%s >>>**** %s %s %s->%s=%s" % (analyticsId, date_str, datatype, label, k, d))
                                        dp = DataPoint.objects.create( analyticsId=analyticsId, datatype=datatype, key1=label, key2=k, value=float(d), date=date)
                                        
                                    else:
                                        #logger.debug("%s >>>** %s %s %s->%s=%s" % (analyticsId, date_str, datatype, label, k, v))
                                        dp = DataPoint.objects.create( analyticsId=analyticsId, datatype=datatype, key1=label, key2=k, value=float(v), date=date)

                                continue

                            else:
                                logger.debug("%s Unknown value type for %s %s %s" % (analyticsId, date_str, datatype, label))
                                continue

                            #logger.debug("%s >>> %s %s %s=%s" % (analyticsId, date_str, datatype, label, d))
                            dp = DataPoint.objects.create( analyticsId=analyticsId, datatype=datatype, key1=label, value=float(d), date=date)

                            

    def duration( self, date_obj, data_type, duration, analyticsId, key=None):

        if isinstance( key, basestring) and (len(key) == 0 or key == 'None'):
            return
        if duration < 1:
            return
        if duration > 30 and analyticsId == '68-145-1':
            return
        elif duration > 62 and analyticsId == '1-1-1':
            return

        val = [0,0]

        if key:
            
            dataset = {}
            if not date_obj.has_key(data_type):
                date_obj[data_type] = dataset
            else:
                dataset = date_obj[data_type]

            if not dataset.has_key(key):
                dataset[key] = val
            else:
                val = dataset[key] 
        else:
            if not date_obj.has_key(data_type):
                date_obj[data_type] = val
            val = date_obj[data_type]

        val[0] += duration
        val[1] += 1 


    def incrementUnique( self, date_obj, data_type, key):
        if not key:
            return

        if not date_obj.has_key(data_type):
            date_obj[data_type] = [set() , 0]

        if key not in date_obj[data_type][0]:
            date_obj[data_type][1] += 1
        date_obj[data_type][0].add(key)



    def increment(self, date_obj, data_type, key, key2=None, count=False):

        if not key:
            return

        if isinstance( key2, basestring) and len(key2) == 0:
            return
        
        t = {}
        if not date_obj.has_key(data_type):
            date_obj[data_type] = t
        else:
            t = date_obj[data_type]

        if count:

            if key not in t:
                t[key] = set()
            t.add(key)

        else:
            if not t.has_key( key):
                if key2:
                    t[key] = {key2 : 0}
                else:
                    t[key] = 0
            elif key2 and not t[key].has_key(key2):
                t[key][key2] = 0

            if key2:
                t[key][key2] += 1
            else:
                t[key] += 1


def makestr( val ):
    if val == None:
        return ''
    else:
        return val
    

def hasLoad( choices ):
    hasLoad = False
    hasIntro = False
    for choice in choices:
        if choice.find('#Load') == 0:
            hasLoad = True
        elif choice.find('Load#in') == 0:
            hasIntro = True

    return (hasLoad and hasIntro)

            
