from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from optparse import make_option
import os, string
from base.analytics import *

import logging
logger = logging.getLogger(__name__)

class Command(BaseCommand):
    args = ''
    help = ''

    option_list = BaseCommand.option_list + (
        make_option('--table',  '-t', dest='table',   help='Table', default=False),
        make_option('--overwrite', '-o', dest='overwrite', help='overwrite', default=False),
        make_option('--dryrun',  '-d', dest='dryrun',   help='Dry Run', default=False, action="store_true"),
    )


    def handle(self, *args, **options):

        dry_run  = options.get('dryrun')


        am = AnalyticsManager()
        res = am.find( options.get('table'), {} )
        updated = 0
        for row in res:

            if not options.get('overwrite') and hasValue(row, 'source'):
                continue

            netloc = getNetloc(row)
            
            e = am.mongo_db[options.get('table')].update( {'_id' : row['_id']}, {'$set' : {'source' : netloc} } )
            if not e or e['err'] != None:
                logger.debug("Error updating %s e=%s" % (row, e))
            else:
                updated = updated + 1

        logger.error("Updated %s docs" % (updated))


def hasValue( obj, key):
    if not obj.has_key(key):
        return False

    val = obj[key]
    if val == None or val == '':
        return False

    return True
