from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
import os, string, binascii, base64
from optparse import make_option
from django.conf import settings
from django.template.loader import render_to_string

import utils.fabric_utils

from base.management.commands.s3sync import sync, save_files
from base.management.commands.cloudfront_invalidate import cloudfront_invalidate

import logging
logger = logging.getLogger(__name__)


# should be using django-pipeline to version and minify files
versioned_files = [ 'static/advntr/playerV2/banner.png', 'static/amazon_cart.png', 'static/theme2/img/TouchingBackground.jpg', 'static/theme2/img/CaseStudyGraphic.png', 'static/advntr/assets/znv/znv_splash.png', 'static/theme2/img/ipad.jpg', 'static/theme2/css/webfont.css', 'static/theme2/css/style.css', 'static/theme2/js/custom.js', 'static/theme2/img/bg_blur.jpg', 'static/advntr/ochre_font.css', 'static/theme2/img/logo.png', 'static/theme2/img/gallery1.jpg', 'static/theme2/img/gallery2.jpg', 'static/theme2/img/gallery3.jpg', 'static/theme2/img/gallery4.jpg', 'static/theme2/img/gallery5.jpg', 'static/theme2/img/gallery6.jpg', 'static/fonts/ochreicons.*', 'static/*.png', 'static/advntr/assets/indigo/indigo_bg.png', 'static/advntr/assets/ochre_play_big.png', 'static/advntr/assets/ochre_controls.png', 'static/advntr/assets/spiderman/spiderman.data', 'static/s3upload.js', 'static/jquery.imageReloader.js', 'static/select2/select2.css', 'static/select2/select2.min.js', 'static/advntr/ochre_player.js', 'static/advntr/swfobject.js', 'static/advntr/ochre_player.css', 'static/theme1/css/layout.css', 'static/theme1/img/bg*.jpg', 'static/theme1/img/*.png', 'static/theme1/js/jquery.easing.sticky.scrollTo.nav.selectnav.parallax.settings.js', 'static/advntr/ochre_accounts.css', 'static/advntr/ochre_accounts.js', 'static/advntr/assets/ochrelogo_advntr_editor.png',  'static/advntr/assets/ochrelogo_advntr_analytics.png',  'static/advntr/assets/ochrelogo_advntr.png', 'static/advntr/assets/ochre_top_logo_dark.png', 'static/bootstrap.min.css', 'static/bootstrap-select.min.css', 'static/bootstrap-datepicker.css', 'static/advntr/assets/helpboxes.png']

minify_files = [ 'static/advntr/ochre_accounts.js', ] 

def flatten_file( template_name, destination, template_data):
    logger.debug("Flattening %s to %s" % (template_name, destination) )

    # flatten embed.html and move to s3
    embed_html = render_to_string( template_name, template_data )
    with open(destination, "w") as f:
        f.write(embed_html.encode("utf-8"))


# base64 encode the config file (twice)
def configure(file):
    print 'Reading ' + file

    f = open(file, 'r')

    str = f.read()
    f.close()

    newstr = base64.b64encode(str)

    newfile = file[:file.rfind('.')]
    f = open(newfile,"wb")
    f.write(binascii.b2a_base64(newstr))
    f.close()

    print 'Wrote ' + newfile


def findAndConfigure(dir):
     for file in os.listdir(dir):

        dir_file = dir + '/' + file

        if os.path.isdir(dir_file):
            findAndConfigure(dir_file) # recursive call
        else:
            if not file.endswith('.data'):
                continue

            configure(dir_file)   



class Command(BaseCommand):
    args = ''
    help = ''

    option_list = BaseCommand.option_list + (
        make_option('--skipVersion', '-s', dest='skipVersion', help='Skip Versioning', default=False, action="store_true"),
    )


    def handle(self, *args, **options):

        path = 'static'
        skip = []

        logger.debug("Syncing %s to %s" % (path, settings.AWS_STATIC_BUCKET))

        utils.fabric_utils.search_replace( [ 'templates', 'static'] , '\/\/localhost.local:8000\/static\/', settings.STATIC_URL )
        utils.fabric_utils.search_replace( [ 'templates', 'static'] , 'localhost.local:8000', 'video.theochre.com' )

        findAndConfigure(path) 

        flatten_file( 'public_index.html', 'static/index.html', { 'STATIC_URL' : settings.STATIC_URL })
        flatten_file( 'public_theme2_index.html', 'static/theme2_index.html', { 'STATIC_URL' : settings.STATIC_URL })

        if not options.get('skipVersion'):
            logger.debug("Minifying and versioning")

            utils.fabric_utils.minify_files( 'java -jar %s/yuicompressor-2.4.7.jar' % (settings.UTILS_DIR), minify_files, '.' )

            collected_files = utils.fabric_utils.collect_files(versioned_files)

            utils.fabric_utils.version_files( [ 'templates', 'static'], collected_files )

            sync(settings.AWS_ACCESS_KEY, settings.AWS_SECRET_KEY, settings.AWS_STATIC_BUCKET, path, True, skip_files=skip, check_exist_only=True)



            # explicitly save these unversioned filenames to s3
            collected_files.append( 'static/advntr/AdvntrPlayer.swf' )
            collected_files.append( 'static/advntr/AdvntrVPAID.swf' )
            collected_files.append( 'static/advntr/assets/znv/znv.data' )
            collected_files.append( 'static/advntr/assets/znv/znv' )
            collected_files.append( 'static/advntr/assets/hungergames2/hungergames2.data' )
            collected_files.append( 'static/advntr/assets/revolt/wale.data' )
            collected_files.append( 'static/advntr/assets/spiderman/spiderman.data' )
            collected_files.append( 'static/advntr/assets/spiderman/spiderman' )
            collected_files.append( 'static/advntr/assets/webmd/webmd.data' )
            collected_files.append( 'static/advntr/assets/webmd/webmd' )
            collected_files.append( 'static/advntr/assets/indigo/indigo' )
            collected_files.append( 'static/advntr/assets/indigo/indigo.data' )


            invalidates = []
            invalidates.append( 'advntr/AdvntrPlayer.swf' )
            invalidates.append( 'advntr/AdvntrVPAID.swf' )
            invalidates.append( 'advntr/assets/znv/znv.data' )
            invalidates.append( 'advntr/assets/znv/znv' )
            invalidates.append( 'advntr/assets/hungergames2/hungergames2.data' )
            invalidates.append( 'advntr/assets/revolt/wale.data' )
            invalidates.append( 'advntr/assets/spiderman/spiderman.data' )
            invalidates.append( 'advntr/assets/spiderman/spiderman' )
            invalidates.append( 'advntr/assets/webmd/webmd.data' )
            invalidates.append( 'advntr/assets/webmd/webmd' )
            invalidates.append( 'advntr/assets/indigo/indigo' )
            invalidates.append( 'advntr/assets/indigo/indigo.data' )

            flatten_file( 'embed.html', 'static/embed.html', { 'STATIC_URL' : '//static.theochre.com/'})
            collected_files.append( 'static/embed.html')
            invalidates.append( 'embed.html')

            save_files( settings.AWS_ACCESS_KEY, settings.AWS_SECRET_KEY, settings.AWS_STATIC_BUCKET, 'static', map( lambda x: x.replace('static/', ''), collected_files) )

            cloudfront_invalidate(','.join(invalidates), 'static')



        logger.debug("Done.")


