from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.db.models import Max
from django.db.models import Q
from optparse import make_option
from boto.dynamodb import exceptions as dynamodb_exceptions
import datetime as dt
import time
import os, string, md5, json
from urllib2 import Request, urlopen, URLError, HTTPError

import pygeoip

from base.models import *


from boto.dynamodb.table import Table
from boto.dynamodb.item import Item
from boto import connect_dynamodb


import logging
logger = logging.getLogger(__name__)



class Command(BaseCommand):
    args = ''
    help = ''
    geo  = None
    filename = ''
    f = None


    option_list = BaseCommand.option_list + (
        make_option('--filename', '-f', dest='filename', help='Filename', default=False),
        make_option('--startdate', '-s', dest='startdate', help='Start Date', default=False),
        make_option('--event', '-x', dest='event', help='Event', default='Video View'),
        make_option('--query_id', '-i', dest='query_id', help='Query Id', default=''),
    )


    def handle(self, *args, **options):

        startdate = options.get('startdate')
        dest      = options.get('dest')
        self.filename  = options.get('filename')
        event     = options.get('event')
        query_id  = options.get('query_id')


        logger.debug("analytics_download_dynamodb startdate=%s filename=%s" % (startdate, self.filename))


        # load ip database
        self.geo = pygeoip.GeoIP(settings.MAXMIND_CITY_DB, pygeoip.MEMORY_CACHE)

        conn   = connect_dynamodb( settings.AWS_ACCESS_KEY, settings.AWS_SECRET_KEY )
        tables = getEventTables(conn, settings.ADVENTR_EVENT_TABLES)
        if not tables.has_key(event):
            logger.error("Missing table for %s" % (event))
            return

        event_table = tables[event]

        written = 0
        if query_id:
            written += self.query( event_table, query_id)
        else: 
            for h in xrange(4,61):

                h_str = ':%02d' %(h)
                
                event_id = startdate + h_str

                written += self.query( event_table, event_id)

        self.f.close()

        logger.info("Saved %s. Wrote %s items" % (self.filename, written))

    def query(self, event_table, query_id):

        logger.debug("Querying %s in %s" % (query_id, event_table.name)) 

        data = event_table.query(query_id)
        #data = event_table.scan()#query(query_id)

        written = 0
        retries = 5
        while retries >= 0:

            if not self.f:
                self.f = open(self.filename, 'wb')
                if not self.f:
                    raise Exception("unable to open file for writing. " + self.filename)


            try:
                for obj in data:

                    # convert properties to json
                    if not hasValue(obj, 'properties'):
                        logger.warn("Missing properties in %s" % (obj))
                        continue

                    properties_str = obj['properties']
                    properties     = json.loads(properties_str)

                    
                    ##############################################
                    # get $city and $region based on ip
                    city    = 'Unknown'
                    region  = 'Unknown'
                    country = 'Unknown'

                    if hasValue(properties, 'ip'):

                        try:
                            geo_data = self.geo.record_by_addr(properties['ip'])
                            if not geo_data:
                                raise pygeoip.GeoIPError('geo_data is None')

                            city    = geo_data.get('city', 'Unknown')
                            region  = geo_data.get('region_name', 'Unknown')
                            country = geo_data.get('country_name', 'Unknown')

                        except pygeoip.GeoIPError, inst:
                            logger.warn("Unable to find ip data from %s, inst=%s" % (obj, inst))
                        except UnicodeDecodeError, inst:
                            logger.warn("UnicodeDecodeError: %s" % (inst))
                    else:
                        logger.warn("Mising ip from %s" % (obj))

                    properties['$city']    = city
                    properties['$region']  = region
                    properties['$country'] = country

                    obj['properties'] = properties
                    ##############################################

                    try:
                        self.f.write( json.dumps(obj) + '\n') 
                        written += 1
                    except Exception, inst:
                        logger.warn("Unable to write obj to file. inst=%s obj=%s" % (inst, obj))

            except dynamodb_exceptions.DynamoDBThroughputExceededError, inst:
                logger.warn("DynamoDBThroughputExceededError %s" % (inst))
                self.f.close()
                self.f = None
                retries -= 1
                time.sleep(10)
                data = event_table.query(query_id)
                #data = event_table.scan()
                
                continue
            break

        return written


def getEventTables(conn, adventr_event_tables_str):

    tables = {}

    EVENT_TABLES=adventr_event_tables_str.split(',')
    for info in EVENT_TABLES:
        
        parts = info.split('=')
        event = parts[0].strip()
        table = parts[1].strip()

        logger.info("Loading table info: %s=%s" % (event, table))
        tables[event] = conn.get_table(table)

    return tables

