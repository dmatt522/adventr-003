from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from optparse import make_option
from boto.dynamodb import exceptions as dynamodb_exceptions
import datetime as dt
import time
import os, string, md5, json
import threading
from urllib2 import Request, urlopen, URLError, HTTPError

from base.models import hasValue
from base.analytics import * 


from boto.dynamodb2.table import Table
from boto.dynamodb.condition import BEGINS_WITH
from boto import connect_dynamodb


import logging
logger = logging.getLogger(__name__)

conn   = connect_dynamodb( settings.AWS_ACCESS_KEY, settings.AWS_SECRET_KEY )


def process_segment(segment=0, total_segments=1, startdate=None, filename=None, event=None, cmd=None):
    # This method/function is executed in each thread, each getting its
    # own segment to process through.

    table = cmd.tables[event]
    
    handleDuration = (event == 'Video View')

    written = 0

    f = open(filename, 'w')

    for h in xrange(0,61):

        h_str    = ':%02d' %(h)
        event_id = startdate + h_str

        logger.debug("Querying %s. Opening %s for writing" % (event_id, filename))

        results = table.query(event_id)#, scan_index_forward=False)
        for item in results:

            try:
                props = item['properties']
                if not props:
                    logger.warn("Missing properties in %s" % (item))
                    continue 

                properties  = json.loads(item['properties'])


                d = int(properties['time']) * 1000
                properties['datetime']     = {'$date' : d }

                str_data = json.dumps(properties)

                if handleDuration:
                    duration = int(properties.get('duration', 0))
                    if duration <= 0:
                        logger.debug("Skipping missing duration" % (properties))
                        continue

                    f.write( str(properties['duration']) + ' ' + str_data + '\n')
                else:
                    f.write( str(d) + ' ' + str_data + '\n')

                written += 1

            except Exception, inst:
                logger.warn("Exception in table scan. %s" % (inst))

    f.close()
    logger.debug("Done writing %s. Wrote %s items" % (filename, written))





class Command(BaseCommand):
    args = ''
    help = ''
    tables = None

    option_list = BaseCommand.option_list + (
        make_option('--startdate', '-s', dest='startdate', help='Start Date', default=False),
        make_option('--event', '-e', dest='event', help='Event', default='Video View'),
        make_option('--filename', '-f', dest='filename', help='Filename'),
        make_option('--segments', '-n', dest='segments', help='Num segments', default=1),
    )

    def handle(self, *args, **options):

        startdate = options.get('startdate')
        event     = options.get('event')
        segments  = int(options.get('segments'))

        logger.debug("analytics_download_data startdate=%s" % (startdate)) 

        self.tables = getEventTables(conn, settings.ADVENTR_EVENT_TABLES)
        if not self.tables.has_key(event):
            logger.error("Missing table for %s" % (event))
            return


        pool = []
        # We're choosing to divide the table in 3, then...

        # ...spinning up a thread for each segment.
        for i in range(segments):
            worker = threading.Thread(
                target=process_segment,
                kwargs={
                    'segment': i,
                    'total_segments': segments,
                    'event' : event,
                    'startdate' : startdate,
                    'filename' : options.get('filename'),
                    'cmd' : self,
                }
            )
            pool.append(worker)
            # We start them to let them start scanning & consuming their
            # assigned segment.
            worker.start()

        # Finally, we wait for each to finish.
        for thread in pool:
            thread.join()
            


def getEventTables(conn, adventr_event_tables_str):

    tables = {}

    EVENT_TABLES=adventr_event_tables_str.split(',')
    for info in EVENT_TABLES:
        
        parts = info.split('=')
        event = parts[0].strip()
        table = parts[1].strip()

        logger.info("Loading table info: %s=%s" % (event, table))
        tables[event] = conn.get_table( table)#Table(table, connection=conn)

    return tables

