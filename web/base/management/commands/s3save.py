from django.core.management.base import BaseCommand, CommandError
import os, string
from optparse import make_option
from django.conf import settings

from base.management.commands.s3sync import sync

import logging
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    args = ''

    option_list = BaseCommand.option_list + (
        make_option('--dir', '-d', dest='directory', help='Directory'),
        make_option('--bucket', '-b', dest='bucket', help='bucket'),
    )


    def handle(self, *args, **options):

        logger.debug("Syncing %s to %s" % (options.get('directory'), options.get('bucket')))

        sync(settings.AWS_ACCESS_KEY, settings.AWS_SECRET_KEY, options.get('bucket'), options.get('directory'), False)

        logger.debug("Done.")


