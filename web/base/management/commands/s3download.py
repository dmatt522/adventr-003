from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
import os, string
from django.conf import settings
from boto.s3.connection import S3Connection
from boto.s3.key import Key

import logging
logger = logging.getLogger(__name__)



class Command(BaseCommand):
    args = ''
    help = ''

    option_list = BaseCommand.option_list + (
        make_option('--bucket', '-b', dest='bucket', help='Bucket Name', default=False),
        make_option('--directory', '-d', dest='directory', help='To Directory', default='.'),
    )



    def handle(self, *args, **options):

       
        conn    = S3Connection(settings.AWS_ACCESS_KEY, settings.AWS_SECRET_KEY)

        bucket    = options.get('bucket')
        directory = options.get('directory')
        if directory and not os.path.exists(directory):
            os.makedirs(directory) 

        b = conn.lookup( bucket )

        for k in b:
            filename = directory + '/' + k.name
            logger.info("Downloading %s to %s" % (k.name, filename))
            k.get_contents_to_filename(filename)


        logger.debug("Done.")


