from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from optparse import make_option
from django.db import transaction
import os, string
from base.models import *

import logging
logger = logging.getLogger(__name__)

class Command(BaseCommand):
    args = ''
    help = ''

    option_list = BaseCommand.option_list + ()

    def handle(self, *args, **options):

        logger.info("analytics_load_values")

        # ordering should correspond to order of SEGMENT_TYPES in models.py
        datatypes = [ 
                        ['ViewsSource',  'DurationSource',  'FunnelSource'],
                        ['ViewsRegion',  'DurationRegion',  'FunnelRegion'],
                        ['ViewsOS',      'DurationOS',      'FunnelOS'],
                        ['ViewsFlash',   'DurationFlash',   'FunnelFlash'],
                        ['ViewsBrowser', 'DurationBrowser', 'FunnelBrowser']
                    ]


        dp_values = []
        charttype = 0

        for types in datatypes:

            datapoints = DataPoint.objects.values('analyticsid', 'key1').distinct().filter(datatype__in = types)#[:100]
            for datapoint in datapoints:

                campaign_id = DataPoint.ParseAnalyticsId(datapoint['analyticsid'])
                if not campaign_id:
                    continue

                # get the charttype
                dp_values.append( DataPointValue( campaign_id=int(campaign_id), charttype=charttype, value=datapoint['key1']) )

            charttype += 1


        if len(dp_values) < 100:
            logger.error("Too few DataPointValues, quitting")
            return 1

        with transaction.commit_on_success():
            DataPointValue.objects.all().delete()
            DataPointValue.objects.bulk_create(dp_values)

        logger.info("Created %s DataPointValues" % (len(dp_values)))




