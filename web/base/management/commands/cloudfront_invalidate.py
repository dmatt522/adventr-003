from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
import os, string, glob
from django.conf import settings

from boto.cloudfront import CloudFrontConnection
from boto.cloudfront.exception import CloudFrontServerError

import logging
logger = logging.getLogger(__name__)

def collectFiles(matches, dir='.'):

    files = []
    for f in matches:
        files = files + glob.glob(dir + '/' + f)

    for file in os.listdir(dir):

        dir_file = dir + '/' + file
        if os.path.isdir(dir_file):
            files = files + collectFiles(matches, dir_file)

    return files


def cloudfront_invalidate( files, dir, dryrun=False ):

        matches     = []  # paths containing '*' that need to be searched for
        invalidates = []  # resulting file paths that will be invalidated


        for f in files.split(','):
            if f.find("*") >= 0:
                matches.append(f)
                logger.debug("Searching for %s" % (f))
            else:
                invalidates.append(f)
                logger.debug("Adding explicit file %s" % (f))

        logger.debug("Switching dir to " + dir)
        cur_dir = os.getcwd()

        try:
            os.chdir(dir)

            invalidates = invalidates + collectFiles(matches)

            # remove ./ from the beginning of files
            invalidates = map( lambda x : x.replace('./', ''), invalidates)

            for found in invalidates:
                logger.debug("Invalidating %s" % (found)) 

            if not dryrun:
                if len(invalidates) > 0:
                    conn = CloudFrontConnection(settings.AWS_ACCESS_KEY, settings.AWS_SECRET_KEY) 
                    conn.create_invalidation_request(settings.AWS_CLOUDFRONT_ID, invalidates)
                else:
                    logger.debug("No files matched")
            else:
                logger.debug("Dryrun. Not doing anything")
        except CloudFrontServerError, inst:
            logger.error("CloudFrontServerError error %s" % (inst))
        finally:
            os.chdir(cur_dir)

        logger.debug("Done.")



class Command(BaseCommand):
    args = ''
    help = ''

    option_list = BaseCommand.option_list + (
        make_option('--dryrun', '-x', dest='dryrun', help='Dry Run', default=False, action="store_true"),
        make_option('--dir', '-d', dest='dir', help='Chdir', default='static'),
        make_option('--files', '-f', dest='files', help='Files', default=[]),

    )


    def handle(self, *args, **options):
        cloudfront_invalidate( options.get('files'), options.get('dir'), options.get('dryrun') )


