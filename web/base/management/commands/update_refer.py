from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from optparse import make_option
import os, string, random
from base.analytics import *

import logging
logger = logging.getLogger(__name__)

"""
update_refer: Updates referringChoice to random values. 
              For testing purposes
"""
class Command(BaseCommand):
    args = ''
    help = ''

    option_list = BaseCommand.option_list + (
        make_option('--video',  '-x', dest='video',   help='video', default=False),
        make_option('--target',  '-t', dest='target',   help='Target', default=False),
        make_option('--referrers', '-r', dest='referrers', help='Referrers', default=False),
        make_option('--overwrite', '-o', dest='overwrite', help='overwrite', default=False),
        make_option('--dryrun', '-d', dest='dryrun', help='dryrun', default=False, action="store_true"),
    )


    def handle(self, *args, **options):

        target = options.get('target')
        video  = options.get('video')

        referrers = options.get('referrers').split(',')

        logger.debug("Video=%s Target=%s Referrers=%s" % (video, target, referrers ))


        # select target

        # set referringChoice of one of referrers

        am = AnalyticsManager()
        
        res = am.find( 'events', {'clip' :  target, 'Video' : video } )
        if res.count() == 0:
            logger.error("No docs found")
            return

        for row in res:
            try:
                if not options.get('overwrite') and hasValue(row, 'referringChoice'):
                    continue

                target = row['clip']

                referringChoice = random.choice(referrers)

                oldReferringChoice = row.get('referringChoice', 'None')

                logger.debug("Setting target=%s referringChoice=%s _id=%s oldReferringChoice=%s" % (target, referringChoice, row['_id'], oldReferringChoice ))

                if not options.get('dryrun'):
                    am.mongo_db.events.update( {'_id' : row['_id']}, {'$set' : {'referringChoice' : referringChoice} }, multi=False )

               
            except Exception, inst:
                logger.error("error: %s" % (inst))

def hasValue( obj, key):
    if not obj.has_key(key):
        return False

    val = obj[key]
    if val == None or val == '':
        return False

    return True
