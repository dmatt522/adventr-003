######################################################################################
# s3sync.py
#
# Author: Ryan Aviles
# Purpose: Sync a local directory to an Amazon S3 bucket.
# Operations are only additive, no files are deleted from S3
#
# Usage:
# python sync.py <options> directory
# Options:
# -a <aws access key>
# -s <aws secret key>
# -b <aws bucket>
# -p Make files public
# -h Full usage with enviornment variable options
#
######################################################################################
import os, string, sys, re, time

from boto.s3.connection import S3Connection
from boto.s3.key import Key
from boto.utils import ISO8601

from datetime import datetime
from optparse import OptionParser
from pytz import timezone, UTC


def sync( aws_access_key, aws_secret_key, aws_bucket, local_path, make_public, skip_files=[], check_exist_only=False ):

    # handle /<folder> in bucket
    idx = aws_bucket.find('/')
    s3_path = None
    if idx>=0: 
        s3_path = aws_bucket[idx+1:]
        aws_bucket = aws_bucket[:idx]

    """ Sync a local directory to an S3 bucket with appropriate credentials """
    conn    = S3Connection(aws_access_key, aws_secret_key)
    bucket  = conn.create_bucket(aws_bucket)
 
    _sync_dir( bucket, local_path, s3_path, make_public, skip_files=skip_files, check_exist_only=check_exist_only) # s3_path argument is empty on first call


def save_files( aws_access_key, aws_secret_key, aws_bucket, local_path, keynames ):
    
    conn    = S3Connection(aws_access_key, aws_secret_key)
    bucket  = conn.create_bucket(aws_bucket)

    for keyname in keynames:
        k = bucket.lookup( keyname )
        if not k:
            k = Key(bucket)
            k.key = keyname

        headers = {'Cache-Control' : ('max-age=%d, public' % (60 * 10) ) }
        k.set_contents_from_filename(local_path + '/' + keyname, headers=headers)
        k.set_acl('public-read')
        print 'Saved %s' % (keyname) 
            

def _save_to_aws( bucket, local_path, s3_path, make_public, check_exist_only=False ):
    """ Sync a file to an s3 bucket path """

    basename = os.path.basename(local_path)
    d = ''
    if s3_path:
        d = s3_path + '/'

    keyname = d + basename
    k = bucket.lookup( keyname )
    update = False

    if k: # key exists, check modification date

        if not check_exist_only:
            #########################################################################
            # Convert s3-file and local-file time stamps to localized time for comparison
            t_s3 =  _iso8601_to_timestamp(k.last_modified ) # returns UTC struct_time
            secs_s3 = time.mktime( t_s3 )

            secs_local = os.path.getmtime(local_path) # returns epoch-seconds local time
            t_local = time.gmtime(secs_local) # convert to UTC struct_time 
            secs_local = time.mktime( t_local ) # convert back to epoch-seconds UTC time

            if secs_local > secs_s3:
                print("Modified: %s Prev Time %s New Time %s. Uploading..." % (k.key, secs_s3, secs_local))
                update = True

    else: # new file, save to s3

        k = Key(bucket)
        k.key = keyname
        print("New File: %s. Uploading" % (k.key))
        update = True


    if update:
        k.set_contents_from_filename(local_path)
        if make_public:
            k.set_acl('public-read')

        print("Done uploading %s" % k.key)


def _sync_dir( bucket, local_path, s3_path, make_public, skip_files=[], check_exist_only=False):
    """ Recursive function to sync a directory path s3 bucket path """

    for file in os.listdir(local_path):

        dir_file = local_path + '/' + file

        if file in skip_files:
            continue

        if os.path.isdir(dir_file):
            new_s3_path = s3_path + '/' + file if s3_path else file
            _sync_dir(bucket, dir_file, new_s3_path, make_public, check_exist_only=check_exist_only) # recursive call
        else:
            _save_to_aws( bucket, dir_file, s3_path, make_public, check_exist_only=check_exist_only)


SUBSECOND_RE = re.compile('\.[0-9]+')
RFC1123 = '%a, %d %b %Y %H:%M:%S %Z' # Thu, 29 Mar 2012 04:55:44 GMT
def _iso8601_to_timestamp(iso8601_time):
    """ S3 quirky datetime handling taken from https://github.com/Yelp/mrjob/blob/master/mrjob/emr.py """
    iso8601_time = SUBSECOND_RE.sub('', iso8601_time)
    try:
        return time.strptime(iso8601_time, ISO8601)
    except ValueError:
        return time.strptime(iso8601_time, RFC1123)


###########################################################################################################
if __name__ == "__main__":

    parser = OptionParser(usage="usage: %prog [options] directory", version="%prog 1.0")
    parser.add_option("-a", "--s3_access", action="store", dest="aws_access_key", 
                      default=os.environ.get('AWS_ACCESS_KEY', ''), help="AWS S3 access key, Default: Env var AWS_ACCESS_KEY")

    parser.add_option("-s", "--s3secret", action="store", dest="aws_secret_key", 
                      default=os.environ.get('AWS_SECRET_KEY', ''), help="AWS S3 secret key, Default: Env var AWS_SECRET_KEY")

    parser.add_option("-b", "--s3bucket", action="store", dest="aws_bucket", 
                      default=os.environ.get('AWS_BUCKET', ''), help="AWS S3 destination bucket, Can include /<folder> to put contents into folder inside bucket ie mybucket/myfolder. Default: Env var AWS_BUCKET")

    parser.add_option("-p", "--public", action="store_true", dest="make_public", 
                      default=False, help="Set public-read ACL on each file")

    (options, args) = parser.parse_args()

    if len(args) < 1:
        print(parser.get_usage())
        sys.exit(1)


    path = args[0]
    sync( options.aws_access_key, options.aws_secret_key, options.aws_bucket, path, options.make_public )

    print("Done.")

