from django.shortcuts import render, render_to_response, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import RequestContext
from django.core.urlresolvers import reverse
from slugify import slugify
from django.core.serializers.json import DjangoJSONEncoder
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.db import IntegrityError, DatabaseError
from django.utils.encoding import smart_str, smart_unicode
from django.template.loader import render_to_string
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.contrib import messages
from django.forms.forms import NON_FIELD_ERRORS
from models import Client, Profile
from forms import SubscriptionForm
from zencoder import Zencoder
import operator

import string, os, math
import csv
import json
import datetime, time
import uuid, base64, hmac, sha
import hashlib
import stripe

from djstripe.utils import subscriber_has_active_subscription, Customer
from djstripe.models import CurrentSubscription

from base.forms import *
from base.analytics import *
from base.encoding import *
from base.utils import *
stripe.api_key = settings.STRIPE_SECRET_KEY
import logging

logger = logging.getLogger(__name__)


def ping(request):
    tag_prev = get_file_contents(settings.PROJECT_ROOT + '/../tag-prev.txt')
    tag_cur = get_file_contents(settings.PROJECT_ROOT + '/../tag.txt')

    # test postgresql
    numCampaigns = VideoCampaign.objects.all().count()

    today = get_date()

    celery_tag = 'disabled'
    if getattr(settings, 'BROKER_URL', False):
        result = encode_ping.delay()
        result.wait()
        celery_tag = result.result

        if tag_cur != celery_tag:
            log.error("GIT TAG MISMATCH: www tag=%s, celery worker tag=%s" % (tag_cur, celery_tag))
            raise Http404()

    return getJsonResponse({'numCampaigns': numCampaigns, 'tag_prev': tag_prev, 'tag': tag_cur, 'date': str(today),
                            'celery_tag': celery_tag, 'debug': settings.DEBUG})


@login_required
def accounts_revisions(request):
    profile = request.user.profile
    campaign_id = request.GET['campaign_id']

    campaign = get_campaign_data(profile, campaign_id, False)

    history = ConfigurationHistory.objects.filter(campaign=campaign).order_by('-created')[1:15]

    configs = []
    for c in history:
        configs.append({'value': c.id, 'text': c.created.strftime('%c')})

    return getJsonResponse(configs, serializer=DjangoJSONEncoder)


@login_required
def accounts_delete_asset(request):
    profile = request.user.profile
    asset_id = request.POST['asset_id']
    asset = get_asset(profile, asset_id)

    logger.info("Deleting %s for user %s" % (asset, request.user))

    asset.delete()

    return getJsonResponse({"success": 1})


def getStartPage(request, profile, fallback='accounts_dashboard'):
    next_page = request.GET.get('next', request.POST.get('next', None))

    if next_page:
        return redirect(next_page)

    try:
        demo = VideoCampaign.objects.filter(client=profile.client).order_by('-created')
        return redirect(reverse('accounts_edit_campaign') + '?campaign=%s' % (demo[0].id))
    except IndexError:
        pass

    return redirect(reverse(fallback))


def play(request, code=None):
    x = request.GET.get('x', None)
    if x:
        return render_to_response('play.html', {}, context_instance=RequestContext(request))

    pid = request.GET.get('preview', code)
    if not pid:
        raise Http404()

    published = get_object_or_404(PublishedCampaign, preview_code=pid)

    config_obj = json.loads(published.config_data)

    client_id = int(config_obj['campaign']['clientId'])

    if client_id:
        try:
            client = Client.objects.get(pk=client_id)
        except Client.DoesNotExist:
            client = None

    width = config_obj.get('width', None)
    height = config_obj.get('height', None)
    width = 1280 if not width or width <= 0 else width
    height = 720 if not height or height <= 0 else height
    fb_width = 487
    fb_height = int(float(height) / float(width) * fb_width)

    twitter_width = 640
    twitter_height = int(float(height) / float(width) * twitter_width)

    (showWidth, showHeight) = sizeDownWidthHeight(width, height)

    return render_to_response('play.html', {'published': published, 'config': config_obj, 'showWidth': showWidth,
                                            'showHeight': showHeight, 'fb_width': fb_width, 'fb_height': fb_height,
                                            'twitter_width': twitter_width, 'twitter_height': twitter_height},
                              context_instance=RequestContext(request))


@login_required
def accounts_encode(request):
    profile = request.user.profile

    asset_id = request.GET.get('asset_id', False)
    campaign_id = request.GET.get('campaign', False)

    if asset_id:
        assets = UploadAsset.objects.filter(id=int(asset_id))
    elif campaign_id:
        assets = UploadAsset.objects.filter(campaign__id=int(campaign_id), mime_type__startswith='video',
                                            upload_successful=True, encoded=False, job_error_notified=False)
    else:
        raise Http404()

    encode_assets(assets)

    return getJsonResponse({"success": 1})


def update_thumbnails(request, asset, headers={}):
    client = Zencoder(settings.ZENCODER_API_KEY)

    BUCKET_AND_INPUT_FILE = 's3://%s/%s' % (settings.AWS_UPLOAD_BUCKET, asset.file_path)
    OUTPUT_PATH = 's3://%s/%s/thumbnails/' % (settings.AWS_UPLOAD_BUCKET, asset.campaign.getOutputPath())

    headers["OCHRE-ASSET-ID"] = str(asset.id)
    headers["Accept"] = "application/json"

    thumbnails_output = {"notifications": [{"format": "json", "url": settings.ZENCODER_CALLBACK, "headers": headers}],
                         'thumbnails':

                             [
                                 {'prefix': (asset.file_id + '_poster'),
                                  'number': 1,
                                  'label': 'poster',
                                  # 'size'     :'400x300',
                                  'base_url': OUTPUT_PATH
                                  },

                                 {'number': 6,
                                  'start_at_first_frame': False,
                                  'label': 'thumb',
                                  'prefix': asset.file_id,
                                  'size': '150x120',
                                  'base_url': OUTPUT_PATH

                                  }
                             ]
                         }

    if asset.mime_type.startswith('video'):
        flv_preview = 'preview_' + asset.file_id + '.flv'
        thumbnails_output["h264_profile"] = "high"
        thumbnails_output["h264_bframes"] = 3
        thumbnails_output["video_codec"] = "h264"
        thumbnails_output["one_pass"] = True
        thumbnails_output["audio_quality"] = 1
        thumbnails_output["width"] = 1280
        thumbnails_output["crf"] = 26
        # thumbnails_output["quality"]        = 1
        thumbnails_output["type"] = "standard"
        thumbnails_output["filename"] = flv_preview
        thumbnails_output["base_url"] = 's3://%s/%s' % (settings.AWS_UPLOAD_BUCKET, asset.campaign.getOutputPath())

    response = client.job.create(BUCKET_AND_INPUT_FILE, outputs=thumbnails_output)

    if response.code != 201:
        logger.error("Zencoder error %s with input=%s output=%s response=%s" % (
            response.code, BUCKET_AND_INPUT_FILE, thumbnails_output, response.body), extra={'request': request})
    else:
        logger.debug("Zencoder output=%s" % (response.body))

        # save the job id
        asset.jobsys = SYS_ZENCODER[0]
        asset.jobid = response.body.get('id', '')
        asset.job_error_notified = False
        asset.save()


def accounts_update_thumbnails(request):
    upload_asset_id = request.GET['upload_asset_id']

    for asset_id in upload_asset_id.split(','):

        try:
            asset = get_asset(request.user.profile, asset_id)

            update_thumbnails(request, asset)
            asset.resetThumbnail()
            asset.save()
        except UploadAsset.DoesNotExist, inst:
            logger.error("Asset id=%s does not exist" % (asset_id), extra={'request': request})

    return getJsonResponse({"success": 1})


@login_required
def accounts_s3state(request):
    profile = request.user.profile
    upload_asset_id = int(request.POST['upload_asset_id'])
    state = request.POST['state']

    asset = get_asset(profile, upload_asset_id)

    if state == '1':
        asset.upload_successful = True
        asset.published = asset.encoded = False

    asset.save()
    logger.info("Updated upload asset id=%s state=%s" % (upload_asset_id, state))

    if asset.upload_successful and asset.mime_type.startswith('video'):

        if settings.ZENCODER_CREATE_THUMBNAILS:
            update_thumbnails(request, asset)

        campaign_url = "%s/accounts/campaigns/edit/?campaign=%s" % (settings.HTTPS_DOMAIN, asset.campaign.id)
        body = "Asset: %s<br/>User: %s<br/>%s" % (asset.file_path, asset.user.email, campaign_url)
        send_email(settings.NOTIFY_EMAILS,
                   'Asset Uploaded by %s: %s' % (asset.campaign.client.name, asset.file_original), body,
                   'info@adventr.tv')

    return getJsonResponse({"success": 1})


@login_required
def accounts_s3sign(request):
    profile = request.user.profile

    object_name = request.GET['s3_object_name']
    mime_type = request.GET['s3_object_type']
    campaign_id = request.GET['campaign_id']

    campaign = get_campaign_data(profile, campaign_id, False)

    try:

        file_id = UploadAsset.NewFileId(object_name)

        remote_name = file_id + os.path.splitext(object_name)[1]

        path_file = "%s/%s" % (campaign.getOutputPath(), remote_name)

        file_preview_flv = ''
        if mime_type.startswith('video'):
            if mime_type == 'video/x-flv':
                file_preview_flv = path_file
            else:
                file_preview_flv = campaign.getOutputPath() + '/preview_' + file_id + '.flv'

        (upload_asset, created) = UploadAsset.objects.get_or_create(campaign=campaign, file_original=object_name,
                                                                    mime_type=mime_type,
                                                                    defaults={'user': request.user, 'file_id': file_id,
                                                                              'file_preview_flv': file_preview_flv,
                                                                              'file_path': path_file})

        expires = int(time.time()) + (60 * 10)
        amz_headers = "x-amz-acl:public-read"

        put_request = "PUT\n\n%s\n%d\n%s\n/%s/%s" % (
            mime_type, expires, amz_headers, settings.AWS_UPLOAD_BUCKET, path_file)

        signature = base64.b64encode(hmac.new(settings.AWS_SECRET_KEY, put_request, hashlib.sha1).digest())

        logger.debug("Signed: put_request=%s\n\tsignature=%s" % (put_request, signature))

        url = '%s/%s' % (settings.UPLOAD_URL, path_file)

    except IntegrityError, inst:
        logger.error("IntegrityError %s. %s %s %s" % (inst, profile, object_name, campaign_id),
                     extra={'request': request})
        return getJsonResponse({'error': 1})

    existed = (not created and upload_asset.upload_successful)
    if created and upload_asset.upload_successful != None:
        upload_asset.upload_successful = None;
        upload_asset.save()

        # overwrite certain attributes
    if not created:
        upload_asset.file_id = file_id
        upload_asset.file_preview_flv = file_preview_flv
        upload_asset.file_path = path_file
        upload_asset.resetThumbnail()
        upload_asset.save()

    return getJsonResponse(
        {'upload_asset': upload_asset.toJSON(), 'existed': existed, 'access_key': settings.AWS_ACCESS_KEY,
         'expires': expires, 'signature': signature, 'url': url})


@login_required
def accounts_dashboard_values(request):
    profile = request.user.profile
    campaign_id = request.GET.get('campaign', None)

    if campaign_id == '1163':
        campaign_id = '14'

    dp_values = DataPointValue.objects.filter(campaign_id=int(campaign_id)).order_by('value')
    data = {}

    for value in dp_values:
        name = SEGMENT_TYPES[value.charttype][1]
        if not data.has_key(name):
            data[name] = []

        data[name].append(value.value)

    return getJsonResponse({'data': data})


@login_required
def accounts_dashboard_filter(request):
    profile = request.user.profile

    campaign_id = request.GET.get('campaign', None)

    (campaign, campaigns) = get_campaign_data(profile, campaign_id)

    campaignData = {}

    start_date = request.GET.get('start_date', None)
    end_date = request.GET.get('end_date', None)

    start_date = datetime.utcfromtimestamp(time.mktime(time.strptime(start_date, "%Y-%m-%d")))
    end_date = datetime.utcfromtimestamp(time.mktime(time.strptime(end_date, "%Y-%m-%d"))) + timedelta(days=1)

    segment_type = request.GET.get('select_segment', 'Views')
    segment = request.GET.get('filter', 'Source')
    unique = (request.GET.get('unique', '') == 'on')

    equals_text = request.GET.get('equals_text', '')

    filters_list = []
    allcampaigns = (request.GET.get('allcampaigns', '') == 'on')
    if campaign:
        am = AggregateByDateAnalytics()
        (campaignData, table) = am.renderData(campaign.getAnalyticsId(), segment_type, segment, unique, equals_text,
                                              start_date, end_date)

    return getJsonResponse({'click_data': campaignData, 'segment_type': segment_type, 'table': table})


@login_required
def accounts_dashboard(request):
    profile = request.user.profile

    campaign_id = request.GET.get('campaign', None)

    if not campaign_id:
        campaigns = VideoCampaign.objects.filter(client=profile.client).order_by('name')

        if campaigns.count() == 0:
            messages.info(request, "You haven't created any new campaigns yet")
            return redirect("accounts_create_campaign")

        campaign = campaigns[0]
    else:
        (campaign, campaigns) = get_campaign_data(profile, campaign_id)

    try:
        # Check if Analytics are enabled
        if not profile.client.feature_analytics:
            return redirect(reverse('accounts_edit_campaign') + '?campaign=1')

    except ObjectDoesNotExist:
        # Create missing Client relation
        create_client = Client.objects.create(name=request.user.username)
        profile.client = create_client
        profile.save()
        return redirect(reverse('accounts_edit_campaign') + '?campaign=1')


    campaign_name = campaign.name

    start_date = request.GET.get('start_date', None)
    end_date = request.GET.get('end_date', None)
    select_segment = request.GET.get('select_segment', 'views')

    start_date = get_date(daysback=15)
    end_date = get_date()

    logger.info("Searching %s with Dates %s - %s" % (campaign.name, start_date, end_date))

    allcampaigns = (request.GET.get('allcampaigns', '') == 'on')

    return render_to_response('accounts_dashboard.html', {'client': campaign.client,
                                                          'allcampaigns': allcampaigns,
                                                          'campaigns': campaigns,
                                                          'campaign': campaign,
                                                          'start_date': start_date,
                                                          'end_date': end_date,
                                                          'select_segment': select_segment},
                              context_instance=RequestContext(request))


@login_required
def accounts_dashboard_allclients(request):
    profile = request.user.profile

    if not profile.user.is_superuser:
        raise Http404()

    start_date = request.GET.get('start_date', None)
    end_date = request.GET.get('end_date', None)
    select_segment = request.GET.get('select_segment', 'Views')

    if not start_date or not end_date:
        start_date = get_date(daysback=15)
        end_date = get_date()
    else:
        start_date = datetime.utcfromtimestamp(time.mktime(time.strptime(start_date, "%Y-%m-%d")))
        end_date = datetime.utcfromtimestamp(time.mktime(time.strptime(end_date, "%Y-%m-%d")))

    if request.GET.get('filter', None) == '1':

        logger.info("Searching all campaigns with Dates %s - %s selecting %s" % (start_date, end_date, select_segment))

        am = AggregateByDateAnalytics()

        unique = (request.GET.get('unique', '') == 'on')
        (campaignData, table) = am.renderData(None, select_segment, 'Source', unique, None, start_date, end_date)

        return getJsonResponse({'click_data': campaignData, 'select_segment': select_segment, 'table': table})

    else:

        return render_to_response('accounts_allclients_dashboard.html', {'start_date': start_date,
                                                                         'end_date': end_date,
                                                                         'select_segment': select_segment},
                                  context_instance=RequestContext(request))


def login_user(request):
    if not request.user.is_anonymous():
        return redirect("logout")

    if request.method == 'POST':

        form = LoginForm(request.POST)

        if not form.is_valid():
            return render_to_response('accounts_login.html', {'form': form}, context_instance=RequestContext(request))

        else:
            username = form.cleaned_data.get('username', '').strip()
            password = form.cleaned_data.get('password', '').strip()
            user = authenticate(username=username, password=password)

            if not user:
                logger.info("Invalid login for %s" % (username))
                form._errors['username'] = 'Login/Password is incorrect'

                return render_to_response('accounts_login.html', {'form': form},
                                          context_instance=RequestContext(request))

        login(request, user)

        # if VideoCampaign.objects.filter(client = user.profile.client).count() == 0:
        #    return redirect("accounts_create_campaign")
        # return getStartPage(user.profile)

        return getStartPage(request, user.profile, fallback="accounts_create_campaign")

    else:
        form = LoginForm()

        return render_to_response('accounts_login.html', {'form': form}, context_instance=RequestContext(request))


def logout_user(request):
    logout(request)
    return redirect('accounts_login')


@transaction.commit_on_success
def create_user(email, first_name, last_name, password, client):
    username = str(uuid.uuid4())[:30]

    user = User.objects.create_user(email, email, password)
    user.first_name = first_name
    user.last_name = last_name
    user.save()

    profile = Profile()
    profile.user = user
    profile.client = client
    profile.save()

    return user


def get_asset(profile, asset_id):
    if profile.user.is_superuser:
        return get_object_or_404(UploadAsset, id=int(asset_id))
    else:
        return get_object_or_404(UploadAsset, campaign__client=profile.client, id=int(asset_id))


def get_campaign_data(profile, campaign_id, campaigns=True):
    if profile.user.is_superuser:
        campaign = get_object_or_404(VideoCampaign, id=campaign_id)
    else:
        campaign = get_object_or_404(VideoCampaign, client=profile.client, id=campaign_id)

    if not campaigns:
        return campaign

    if profile.user.is_superuser:
        campaigns = VideoCampaign.objects.filter(client=campaign.client).order_by('name')
    else:
        campaigns = VideoCampaign.objects.filter(client=profile.client).order_by('name')

    return (campaign, campaigns)


class EncodeException(Exception):
    pass


@login_required
def accounts_published(request):
    profile = request.user.profile
    campaign_id = request.GET.get('campaign', None)

    (campaign, campaigns) = get_campaign_data(profile, campaign_id)

    published = PublishedCampaign.objects.filter(campaign=campaign, finalized=True).exclude(
        preview_code=None).select_related().order_by('-published_on')

    return render_to_response('accounts_published.html',
                              {'campaigns': campaigns, 'client': profile.client, 'campaign': campaign,
                               'published': published}, context_instance=RequestContext(request))


@login_required
def accounts_edit_campaign(request):
    profile = request.user.profile

    if request.method == 'POST':
        campaign_id = int(request.POST.get('campaign', 0))

        auto_save = request.POST.get('auto_save', None)
        msgs = []

        try:

            config_data = request.POST['configData']
            config_obj = json.loads(config_data, strict=False) # validates

            if not campaign_id:  # new campaign

                try:
                    campaign = VideoCampaign.objects.create(client=profile.client, name=config_obj['name'])
                    config_obj['campaign']['campaignId'] = campaign.id

                except DatabaseError, inst:
                    msgs.append([messages.error, 'A campaign with that name already exists'])
                    return redirect("accounts_create_campaign")

            else:
                campaign = get_campaign_data(profile, campaign_id, False)

            config_data = json.dumps(config_obj, indent=4, sort_keys=False, separators=(',', ': '))
            campaign.config_data = config_data

            try:
                if config_obj['name'] != campaign.name:
                    campaign.name = config_obj['name']

                campaign.save()

                history = ConfigurationHistory.objects.create(user=request.user, campaign=campaign,
                                                              config_data=config_data)

                # check for publish
                if request.POST.has_key('publishing') and request.POST['publishing'] == '1':

                    encode_campaign(request.user, campaign, config_obj, request)
                    logger.debug("Campaign %s is publishing" % (campaign))
                    msgs.append(
                        [messages.success, 'Your project is being published and we will email you when it is ready.'])

                else:
                    logger.info("Saved config for %s" % (campaign.name))
                    msgs.append([messages.success, 'Campaign Updated'])

            except EncodeException, inst:
                msgs.append([messages.error, inst])

            except DatabaseError, inst:
                msgs.append([messages.error, 'Campaign %s already exists.' % (campaign.name)])

            if not auto_save:
                map(lambda x: x[0](request, x[1]), msgs)
                return redirect("/accounts/campaigns/edit/?campaign=" + str(campaign.id))
            else:
                return getJsonResponse({"success": 1, "campaign_id": campaign.id})


        except Exception, inst:
            logger.error("accounts_edit_campaign: %s json=%s" % (inst, config_data), extra={'request': request})
            if settings.DEBUG == False:
                inst = 'An error occurred. We have been notified and are looking into it'
            messages.error(request, inst)

    else:
        campaign_id = request.GET.get('campaign', None)

    (campaign, campaigns) = get_campaign_data(profile, campaign_id)

    if request.GET.has_key('revision'):
        ch = get_object_or_404(ConfigurationHistory, campaign=campaign, id=int(request.GET['revision']))
        messages.warning(request,
                         'You are editing a saved project from %s. Saving this will overwrite your latest changes.' % (
                             ch.created.strftime('%c')))
        config_data = ch.config_data.strip().replace('\n', '')
    else:
        config_data = campaign.config_data.strip().replace('\n', '')

    assets = UploadAsset.objects.filter(campaign=campaign, upload_successful=True,
                                        job_error_notified=False).select_related().order_by('file_original')
    assets_ours = UploadAsset.objects.filter(campaign_id=settings.PLACEHOLDER_CAMPAIGN_ID,
                                             upload_successful=True).select_related().order_by('file_original')



    return render_to_response('accounts_edit.html',
                              {'campaigns': campaigns, 'client': profile.client, 'campaign': campaign,
                               'config_data': config_data, 'config': json.loads(campaign.config_data.strip()),
                               'assets': assets, 'assets_ours': assets_ours}, context_instance=RequestContext(request))


@login_required
def accounts_account(request):
    current_subscription = 'basic'
    cancelled_subscription = False
    active_subscriber = subscriber_has_active_subscription(request.user)
    # safety net for faulty subscription created before stripe was fixed
    customer_account = False
    customer_acc = Customer.objects.filter(subscriber=request.user)
    if customer_acc:
        subscription = CurrentSubscription.objects.filter(customer=customer_acc[0])
        if subscription:
            customer_account = True

    if active_subscriber and customer_account:
        customer = Customer.objects.get(subscriber=request.user)
        current_subscription = CurrentSubscription.objects.get(customer=customer)

        subscription_form = SubscriptionForm(existing_plan=current_subscription.plan)

        if current_subscription:
            if current_subscription.cancel_at_period_end:
                cancelled_subscription = True

            current_subscription = current_subscription.plan_display()

    else:
        subscription_form = SubscriptionForm()

    context = {
        'STRIPE_PUBLIC_KEY': settings.STRIPE_PUBLIC_KEY,
        'ACTIVE_PLANS': json.dumps(settings.DJSTRIPE_PLANS),
        'active_subscriber': active_subscriber,
        'current_subscription': current_subscription,
        'subscription_form': subscription_form
    }

    if active_subscriber and cancelled_subscription:
        context['cancelled_plan'] = True

    if request.method == 'POST':
        form = ProfileEditForm(request.POST)
        subscription_form = SubscriptionForm(request.POST)
        form.user = request.user
        stripe_token = request.POST.get('stripe_token', None)
        if stripe_token:
            context['stripe_token'] = stripe_token

        context['form'] = form
        context['subscription_form'] = subscription_form
        subscription_plan = request.POST.get('subscription_plan', 'basic')

        if not form.is_valid() or (active_subscriber and not subscription_form.is_valid()):
            return render_to_response('accounts_account.html', context,
                                      context_instance=RequestContext(request))

        if not active_subscriber and subscription_plan != 'basic' and not stripe_token:
            form.errors['__all__'] = form.error_class(["Credit Card Error occurred"])
            return render_to_response('accounts_account.html', context, context_instance=RequestContext(request))

        if form.is_valid():
            request.user.email = request.user.username = form.cleaned_data['email']

            # ensure password is valid then set new password
            if len(form.cleaned_data['password_new']) > 0:
                request.user.set_password(form.cleaned_data['password_new'])
                request.user.save()

            else:
                request.user.save()

            messages.success(request, 'Profile updated')

        # Check if changing the subscription plan
        if subscription_form.is_valid() and subscription_form.cleaned_data['plan'] != '--':
            try:
                if subscriber_has_active_subscription(request.user):
                    customer = Customer.objects.get(subscriber=request.user)
                    current_subscription = CurrentSubscription.objects.get(customer=customer)

                    if subscription_form.cleaned_data['plan'] == 'basic':
                        # Cancel subscription
                        customer.cancel_subscription()
                        context['current_subscription'] = 'basic'
                        messages.success(request, 'Subscription plan cancelled')

                    elif subscription_form.cleaned_data['plan'] != current_subscription.plan:
                        customer.subscribe(plan=subscription_form.cleaned_data['plan'])
                        current_subscription = CurrentSubscription.objects.get(customer=customer)
                        context['current_subscription'] = current_subscription.plan_display()
                        messages.success(request, 'Subscription plan changed')

                else:
                    customer = Customer.objects.create(subscriber=request.user)
                    customer.subscribe(plan=subscription_form.cleaned_data['plan'])
                    messages.success(request, 'Subscription plan changed')

            except stripe.StripeError as exc:
                #form.add_error(None, str(exc))
                form._errors[NON_FIELD_ERRORS] = form.error_class([str(exc)])
                context['form'] = form
                return render_to_response('accounts_account.html', context, context_instance=RequestContext(request))


        # Converting from free to paid
        elif subscription_plan != 'basic':
            try:
                customer, created = Customer.get_or_create(
                    subscriber=request.user)

                customer.update_card(stripe_token)
                customer.subscribe(plan=subscription_plan)
                current_subscription = CurrentSubscription.objects.get(customer=customer)
                context['current_subscription'] = current_subscription.plan_display()

                # Activate analytics and disable auto-watermark
                client = request.user.profile.client
                client.feature_vpaid = True
                client.feature_analytics = True
                client.feature_watermark = False  # Turns off
                client.save()

                messages.success(request, 'Subscription plan changed')

            except stripe.StripeError as exc:
                #form.add_error(None, str(exc))
                form._errors[NON_FIELD_ERRORS] = form.error_class([str(exc)])
                context['form'] = form
                return render_to_response('accounts_account.html', context, context_instance=RequestContext(request))

    return render_to_response('accounts_account.html', context, context_instance=RequestContext(request))


@login_required
def accounts_reset_campaign(request):
    profile = request.user.profile

    campaign_id = request.POST.get('campaign', '0')
    campaign = get_campaign_data(profile, campaign_id, False)
    campaign.config_data = render_to_string('config_template.html',
                                            {'upload_url': settings.UPLOAD_URL, 'client': campaign.client,
                                             'campaign': campaign, 'campaign_name': campaign.name})

    campaign.save()

    messages.success(request, 'Project has been reset.')

    return getJsonResponse({"success": 1})


def getNextCampaignName(request, client):
    num_campaigns = VideoCampaign.objects.filter(client=client).count()

    num_campaigns += 1
    videoName = 'Video ' + str(num_campaigns)
    while VideoCampaign.objects.filter(client=client, name=videoName).count() != 0:
        num_campaigns += 1
        videoName = 'Video ' + str(num_campaigns)

    return videoName


@login_required
def accounts_create_campaign(request):
    if request.method == 'POST':
        return accounts_edit_campaign(request)
    try:
        profile = request.user.profile
    except:
        # create profile for this user
        client = Client.Objects.create(name=request.user.email)
        profile = Profile.objects.create(user=request.user, client=client,
                                         uid='not-set', access_token = 'not-set')





    num_campaigns = VideoCampaign.objects.filter(client=profile.client).count()
    campaign_name = getNextCampaignName(request, profile.client)

    config_data_context = {'upload_url': settings.UPLOAD_URL, 'client': profile.client,
                           'campaign_name': campaign_name}

    config_data = render_to_string('config_template.html', config_data_context)

    assets = []  # UploadAsset.objects.filter( campaign__client=profile.client, upload_successful=True  ).order_by('file_original')
    assets_ours = UploadAsset.objects.filter(campaign_id=settings.PLACEHOLDER_CAMPAIGN_ID,
                                             upload_successful=True).select_related().order_by('file_original')

    return render_to_response('accounts_edit.html',
                              {'config_data': config_data, 'client': profile.client, 'assets': assets,
                               'assets_ours': assets_ours}, context_instance=RequestContext(request))


def analytics_track(request):
    logger.debug("PROPS=%s" % (request.POST))
    return getJsonResponse({})


@login_required
def accounts_preview(request):
    profile = request.user.profile

    if request.GET.get('config_url', None):
        return render_to_response('accounts_preview.html',
                                  {'config_url': '/static/previews/' + request.GET['config_url'],
                                   'width': request.GET['width'], 'height': request.GET['height']},
                                  context_instance=RequestContext(request))

    else:
        campaign_id = request.POST.get('campaign', '0')
        config_json = request.POST['config_json']
        (width, height) = sizeDownWidthHeight(request.POST.get('width', 0), request.POST.get('height', 0))

        if campaign_id != '0':
            campaign = get_campaign_data(profile, campaign_id, False)
            campaign_name = '%s-%s' % (campaign.name, campaign.id)
        else:
            campaign_name = 'unsaved-campaign'

        # write tmp file to server
        filename = "%s-%s-%s-%s-%s.json" % (
            slugify(profile.client.name), profile.client.id, profile.id, slugify(campaign_name), get_rand_id())
        local_filename = settings.PREVIEWS_DIR + '/' + filename

        with open(local_filename, "w") as f:
            f.write(config_json.encode('utf8') + '\n')

        return getJsonResponse({'config_url': filename, 'width': width, 'height': height})


@login_required
def accounts_play(request):
    profile = request.user.profile

    # campaign_id sent with Saved campaign
    campaign_id = request.GET.get('campaign', None)
    campaign = get_campaign_data(profile, campaign_id, False)

    json_url = "%s/cfg/?campaign=%s" % (settings.HTTPS_DOMAIN, campaign.id)

    campaigns = VideoCampaign.objects.filter(client=profile.client).order_by('name')
    config = json.loads(campaign.config_data.strip())

    (width, height) = sizeDownWidthHeight(config['width'], config['height'])

    return render_to_response('accounts_play.html',
                              {'json_url': json_url, 'campaigns': campaigns, 'campaign': campaign, 'width': width,
                               'height': height}, context_instance=RequestContext(request))


# TODO remove?
def get_config(request):
    profile = request.user.profile
    campaign_id = request.GET.get('campaign', None)

    campaign = get_object_or_404(VideoCampaign, client=profile.client, id=campaign_id)

    config_obj = json.loads(campaign.config_data.strip())

    response = HttpResponse(
        mimetype='application/json')  # set javascript mimetype? see http://www.prototypejs.org/api/ajax/request
    #response.write(campaign.config_data)
    response.write(config_obj)

    return response


@login_required
def accounts_config(request):
    profile = request.user.profile
    campaign_id = request.GET.get('campaign', None)
    campaign = get_campaign_data(profile, campaign_id, False)

    return getJsonResponse({'config': campaign.config_data})


@login_required
def accounts_funnel_filter(request):
    profile = request.user.profile

    campaign_id = request.GET.get('campaign', None)

    (campaign, campaigns) = get_campaign_data(profile, campaign_id)

    start_date = request.GET.get('start_date', None)
    end_date = request.GET.get('end_date', None)

    start_date = datetime.utcfromtimestamp(time.mktime(time.strptime(start_date, "%Y-%m-%d")))
    end_date = datetime.utcfromtimestamp(time.mktime(time.strptime(end_date, "%Y-%m-%d")))

    config_obj = json.loads(campaign.config_data)

    segment = 'Funnel' + request.GET.get('filter', '')
    autoChoose = int(request.GET.get('autoChoose', -1))
    equals_text = request.GET.get('equals_text', '').strip()

    if segment == 'Funnel':
        equals_text = ''

    am = ClickDataAnalytics()
    config_click_data = am.renderData(campaign.getAnalyticsId(), segment, equals_text, autoChoose, start_date, end_date,
                                      config_obj)

    return getJsonResponse({'config_data': config_click_data, 'has_data': (len(config_click_data) > 0)})


@login_required
def accounts_funnel(request):
    profile = request.user.profile

    campaign_id = request.GET.get('campaign', None)

    (campaign, campaigns) = get_campaign_data(profile, campaign_id)

    # Check if Analytics are enabled
    if not profile.client.feature_analytics:
        return redirect(reverse('accounts_edit_campaign') + '?campaign=%s' % campaign_id)

    start_date = get_date(daysback=30)
    end_date = get_date()

    logger.info("Searching %s with Dates %s - %s" % (campaign.name, start_date, end_date))

    return render_to_response('accounts_funnel.html',
                              {'client': campaign.client, 'campaigns': campaigns, 'campaign': campaign,
                               'start_date': start_date, 'end_date': end_date},
                              context_instance=RequestContext(request))


def fb_share(request):
    videoName = request.GET.get('videoName', '')
    campaignId = request.GET.get('campaignId', '')
    clientId = request.GET.get('clientId', '')
    post_id = request.GET.get('post_id', None)

    if post_id:
        am = AnalyticsManager()
        data = {'Video': videoName, 'source': 'fb', 'campaignId': campaignId, 'clientId': clientId, 'post_id': post_id,
                'created': datetime.now(), 'count': 1}
        e = am.insert('share', data)
        logger.debug("Inserted fb_share %s->%s" % (e, data))

    return redirect("https://www.facebook.com")


# create new user account
def accounts_create(request):
    if not request.user.is_anonymous():
        return redirect("accounts_account")

    context = {
        'STRIPE_PUBLIC_KEY': settings.STRIPE_PUBLIC_KEY,
        'ACTIVE_PLANS': json.dumps(settings.DJSTRIPE_PLANS),
        'subscription_form': SubscriptionForm(),
        'plan': request.GET.get('plan', None)
    }

    if request.method == 'POST':

        form = ProfileCreateForm(request.POST)

        context['form'] = form
        stripe_token = request.POST.get('stripe_token', None)
        if stripe_token:
            context['stripe_token'] = stripe_token

        subscription_plan = request.POST.get('subscription_plan', 'basic')

        if not form.is_valid():
            return render_to_response('accounts_create.html', context, context_instance=RequestContext(request))

        # Check for stripe token when signing up for a paid plan
        if subscription_plan != 'basic' and not stripe_token:
            form.errors['__all__'] = form.error_class(["Credit Card Error occurred"])
            return render_to_response('accounts_create.html', context, context_instance=RequestContext(request))

        email = form.cleaned_data['email'].strip()
        first_name = form.cleaned_data['first_name'].strip()
        last_name = form.cleaned_data['last_name'].strip()
        password = form.cleaned_data['password'].strip()

        dob = form.cleaned_data['dob']
        occupation = form.cleaned_data['occupation'].strip()
        location = form.cleaned_data['location'].strip()
        website = form.cleaned_data['website'].strip()

        if User.objects.filter(username=email).count() > 0:
            form._errors['email'] = 'That email already exists in our system'
            return render_to_response('accounts_create.html', context, context_instance=RequestContext(request))

        else:

            # Create 1:1 Client for each user
            # Note: Use email at first signup, then use name=user.username for lookups
            client = Client.objects.create(name=email)

            user = create_user(email, first_name, last_name, password, client)

            # Update profile
            if dob or occupation or location:
                profile = Profile.objects.get(user=user)
                profile.dob = dob
                profile.occupation = occupation
                profile.location = location
                profile.website = website
                profile.save()

            # Check if paid the subscription plan
            if subscription_plan != 'basic':
                customer, created = Customer.get_or_create(subscriber=user)

                try:
                    customer.update_card(request.POST.get("stripe_token"))
                    customer.subscribe(plan=subscription_plan)

                    # Activate analytics and disable auto-watermark
                    client.feature_vpaid = True
                    client.feature_analytics = True
                    client.feature_watermark = False  # Turns off
                    client.save()

                except stripe.StripeError as exc:
                    client.delete()
                    user.delete()
                    # delete this stripe customer
                    if customer.stripe_id:
                        cu = stripe.Customer.retrieve(customer.stripe_id)
                        cu.delete()

                    form._errors[NON_FIELD_ERRORS] = form.error_class([str(exc)])
                    context['form'] = form
                    return render_to_response('accounts_create.html', context, context_instance=RequestContext(request))

            # Signup for subscriber list
            subscribe_mailchimp_list(settings.MAILCHIMP_SUBSCRIBER_LIST, user)

            user = authenticate(username=email, password=password)
            login(request, user)

            logger.debug("Created user %s,%s,%s,%s" % (email, first_name, last_name, password))

            messages.success(request, 'Account Created!')

        return getStartPage(request, user.profile, fallback='accounts_create_campaign')

    else:
        context['form'] = ProfileCreateForm()

        return render_to_response('accounts_create.html', context, context_instance=RequestContext(request))


def vast_events(request):
    logger.info("VAST EVENT:\n\trequest=%s\n\tbody=%s" % (request.META, request.body))
    return getResponse({})
