from django import forms
from django.utils.translation import gettext as _

from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout

import time
import datetime
from datetime import date

from djstripe.settings import PLAN_CHOICES

from models import *

import logging
logger = logging.getLogger(__name__)


class ProfileEditForm(forms.Form):
    email            = forms.EmailField(max_length=75, widget=forms.TextInput(attrs={'class':'long required'}), required=True )

    password         = forms.CharField( widget=forms.PasswordInput(), max_length=40, required=False )
    password_new     = forms.CharField( widget=forms.PasswordInput(), max_length=40, required=False )
    password_confirm = forms.CharField( widget=forms.PasswordInput(), max_length=40, required=False )

    dob              = forms.DateField(required=False, widget=forms.DateInput(attrs={'placeholder': 'mm/dd/yyyy'}, format = '%m/%d/%Y'))
    occupation       = forms.CharField( widget=forms.TextInput(attrs={'placeholder':'Videographer', 'class':'' }), required=False, max_length=100 )
    location         = forms.CharField( widget=forms.TextInput(attrs={'placeholder':'Paris, France', 'class':'' }), required=False, max_length=100 )
    website         = forms.CharField( widget=forms.TextInput(attrs={'placeholder':'yourwebsite.com', 'class':'' }), required=False, max_length=255 )

    user = None

    def clean(self):
        
        email            = self.cleaned_data.get('email', '').strip()
        password_new     = self.cleaned_data.get('password_new', '').strip()
        password_cleaned = self.cleaned_data.get('password_confirm', '').strip()

        # check for existing email
        if User.objects.filter(email=email).exclude(id=self.user.id).count() > 0:
            self._errors['email'] = 'That email address is already in use'
            raise forms.ValidationError('')

        if len(password_new) > 0:
            if password_new != password_cleaned:
                self._errors['password_confirm'] = 'Passwords do not match'
                raise forms.ValidationError('')

            elif self.user.check_password(self.cleaned_data['password']) is not True:
                self._errors['password'] = 'Your current password is not correct'
                raise forms.ValidationError('')

        return self.cleaned_data


class ProfileCreateForm(forms.Form):
    email            = forms.EmailField(max_length=75, widget=forms.TextInput(attrs={'class':'long required','rel':'tipsy', 'title': "Don't worry, we'll keep this private!"}) )

    first_name       = forms.CharField( widget=forms.TextInput(attrs={'placeholder':'First Name', 'class':'' }), required=True, max_length=30 )
    last_name        = forms.CharField( widget=forms.TextInput(attrs={'placeholder':'Last Name', 'class':'' }), required=True, max_length=30 )

    password         = forms.CharField( widget=forms.PasswordInput(), required=True, max_length=40, min_length=5 )
    password_confirm = forms.CharField( widget=forms.PasswordInput(), required=True, max_length=40, min_length=5 )

    dob              = forms.DateField(required=False, widget=forms.DateInput(attrs={'placeholder': 'mm/dd/yyyy'}, format = '%m/%d/%Y'))
    occupation       = forms.CharField( widget=forms.TextInput(attrs={'placeholder':'Videographer', 'class':'' }), required=False, max_length=100 )
    location         = forms.CharField( widget=forms.TextInput(attrs={'placeholder':'Paris, France', 'class':'' }), required=False, max_length=100 )
    website         = forms.CharField( widget=forms.TextInput(attrs={'placeholder':'yourwebsite.com', 'class':'' }), required=False, max_length=255 )

    def clean(self):
        
        password         = self.cleaned_data.get('password', '')
        password_cleaned = self.cleaned_data.get('password_confirm', '')

        if password != password_cleaned:
            self._errors['password'] = 'Passwords do not match'

        # lookup account#

       

        return self.cleaned_data


class LoginForm(forms.Form):
    
    username         = forms.CharField( widget=forms.TextInput(attrs={'placeholder':'Login', 'class':'' }), required=True, max_length=75, min_length=3 )
    password         = forms.CharField( widget=forms.PasswordInput(attrs={'placeholder':'Password', 'class':'' }), required=True, max_length=40, min_length=5 )

    def clean(self):

        return self.cleaned_data



class SubscriptionForm(forms.Form):
    plan = forms.ChoiceField(choices=[('--', 'Please Select'),('basic', 'Cancel/Go to Free account')]+PLAN_CHOICES)

    def __init__(self, *args, **kwargs):
        existing_plan = kwargs.pop('existing_plan', None)
        super(SubscriptionForm, self).__init__(*args, **kwargs)
        if existing_plan:
            # Remove existing plan from options
            self.fields['plan'].choices = [i for i in self.fields['plan'].choices if i[0] != existing_plan]



