import os
import random
import base64, json
from django import template

import logging
logger = logging.getLogger(__name__)

register = template.Library()

@register.tag(name="randompick")
def randompick(parser, token):
    items = []
    bits =  token.split_contents()
    for item in bits:
        items.append(item)
    return RandomPickNode(items[1:])

class RandomPickNode(template.Node):
    def __init__(self, items):
        self.items = []
        for item in items:
            self.items.append(item)
    
    def render(self, context):
        idx = random.randint(0, len(self.items)-1)
        return self.items[idx]


class VerbatimNode(template.Node):

    def __init__(self, text):
        self.text = text
    
    def render(self, context):
        return self.text


@register.tag
def verbatim(parser, token):
    text = []
    while 1:
        token = parser.tokens.pop(0)
        if token.contents == 'endverbatim':
            break
        if token.token_type == template.TOKEN_VAR:
            text.append('{{')
        elif token.token_type == template.TOKEN_BLOCK:
            text.append('{%')
        text.append(token.contents)
        if token.token_type == template.TOKEN_VAR:
            text.append('}}')
        elif token.token_type == template.TOKEN_BLOCK:
            text.append('%}')
    return VerbatimNode(''.join(text))


@register.filter
def startswith(value, arg):
    """Usage, {% if value|startswith:"arg" %}"""
    return value.startswith(arg)

@register.filter
def to_mixpanel_event( data, stuff ):

    adType     = None
    adEvent    = None
    adClickUrl = None
    values = stuff.split(',')

    if len(values) == 2:
        (adType, adEvent) = values
    elif len(values) == 3:
        (adType, adEvent, adClickUrl) = values

    data['properties']['adType']  = adType
    data['properties']['adEvent'] = adEvent

    if adClickUrl:
        data['properties']['adClickUrl'] = adClickUrl

    s = base64.b64encode( json.dumps( data ) )

    return s


    
