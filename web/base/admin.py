from django.shortcuts import render, render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.contrib import admin
from django.conf.urls.defaults import *
from django.core.urlresolvers import reverse
from django.conf import settings
from datetime import date
from base.models import *

import logging
logger = logging.getLogger(__name__)


def edit_campaign(obj):
    return '<a href="/accounts/campaigns/edit/?campaign=%s">Edit in ADVNTR</a>' % (obj.id )
edit_campaign.allow_tags = True

def client_name(obj):
    return obj.client.name
client_name.allow_tags = True

class VideoCampaignAdmin(admin.ModelAdmin):

    list_display  = ('name', edit_campaign, client_name, 'active', 'created')
    search_fields = ('name',)

class ConfigurationHistoryAdmin(admin.ModelAdmin):

    list_display  = ('campaign', 'user', 'created')
    search_fields = ('campaign','user')



class ClientAdmin(admin.ModelAdmin):

    list_display  = ('name', 'account_code', 'active', 'created')
    search_fields = ('name', 'account_code')


def profile_overview(request, id):
    profile = Profile.objects.get(id=id)
    opts = Profile._meta

    #create template page and extend admin/base.html
    data = { 'profile' : profile }        

    return render_to_response('admin_user.html', data, context_instance=RequestContext(request))


def full_name_overview(obj):
    return "<a href='%s'>%s</a>" % (reverse('admin_profile', args=[obj.id]), obj.user.email)
full_name_overview.allow_tags = True
full_name_overview.short_description = 'Name'


class ProfileAdmin(admin.ModelAdmin):
    list_display  = ('id', full_name_overview )
    search_fields = ('user__email',)

def published_url(obj):
    url = settings.PLAY_URL + '/' + obj.preview_code
    return '%s' % (url)

class PublishedCampaignAdmin(admin.ModelAdmin):
    list_display  = ('campaign', 'user', 'published_on', published_url)
    search_fields = ('user__email',)

class UploadAssetAdmin(admin.ModelAdmin):
    pass

admin.site.register(VideoCampaign, VideoCampaignAdmin)
admin.site.register(ConfigurationHistory, ConfigurationHistoryAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Profile, ProfileAdmin )
admin.site.register(PublishedCampaign, PublishedCampaignAdmin)
admin.site.register(UploadAsset, UploadAssetAdmin)
