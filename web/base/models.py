from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from slugify import slugify
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse
from django.core.serializers.json import DjangoJSONEncoder
from django.core.serializers import serialize
from south.modelsinspector import add_introspection_rules
from django.shortcuts import get_object_or_404
from datetime import datetime, timedelta, date
from datetime import datetime, timedelta
from base.utils import *

from boto.s3.connection import S3Connection
from boto.s3.bucket import Bucket as S3Bucket
from boto.s3.key import Key
from boto.exception import S3CopyError

import os, random, string, errno, math, copy, codecs
import pymongo
import json
import pytz


import logging
logger = logging.getLogger(__name__)


SYS_ZENCODER = (0, 'Zencoder')
SYS_ADVENTR  = (1, 'Adventr')

JOB_SYSTEMS = ( SYS_ZENCODER, SYS_ADVENTR )

SEGMENT_TYPES = (
                (0, 'Source'),
                (1, 'Region'),
                (2, 'OS'),
                (3, 'Flash'),
                (4, 'Browser')
)

class JSONConvertable():

    def toJSON(self):
        fields = json.loads( serialize('json', [ self ]) )[0]['fields']
        fields['id'] = self.id

        if hasattr(self, 'campaign'):
            fields['outputPath'] = self.campaign.getOutputPath()

        return json.dumps(fields)



class JSONField(models.TextField):
    """JSONField is a generic textfield that neatly serializes/unserializes
    JSON objects seamlessly"""

    # Used so to_python() is called
    __metaclass__ = models.SubfieldBase

    def to_python(self, value):
        """Convert our string value to JSON after we load it from the DB"""

        if value == "":
            return None

        try:
            if isinstance(value, basestring):
                return json.loads(value)
        except ValueError:
            pass

        return value

    def get_db_prep_save(self, value, connection):
        """Convert our JSON object to a string before we save"""

        if value == "":
            return None

        if isinstance(value, dict):
            value = json.dumps(value, cls=DjangoJSONEncoder)

        return super(JSONField, self).get_db_prep_save(value, connection)


#######################################################################################
#######################################################################################


class Client(models.Model):

    name         = models.CharField(max_length=50, blank=False, null=False) # Use email at first signup, then use name=user.username for lookups
    account_code = models.CharField(max_length=10, blank=True, null=True)  # No longer used, leaving for existing data
    active       = models.BooleanField(default=True)
    created      = models.DateTimeField(default=datetime.utcnow().replace(tzinfo = pytz.utc),auto_now_add=True)
    ochre_notes  = models.TextField(blank=True, null=True)

    feature_publish = models.BooleanField(default=True)
    feature_vpaid   = models.BooleanField(default=False)
    feature_config  = models.BooleanField(default=False)
    feature_analytics = models.BooleanField(default=False, verbose_name='Adventr analytics enabled')
    feature_watermark = models.BooleanField(default=True, verbose_name='Adventr watermark enabled')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return unicode(self).encode('ascii', 'ignore')



class VideoCampaign(models.Model):

    name  = models.CharField(max_length=50, blank=False, null=False)
    client       = models.ForeignKey(Client)
    active       = models.BooleanField(default=True)
    created      = models.DateTimeField(default=datetime.utcnow().replace(tzinfo = pytz.utc),auto_now_add=True)

    config_data  = models.TextField(max_length=25600, blank=True, null=True )

    class Meta:
        unique_together = ( ( 'name', 'client'), )

    def getOutputPath(self):
        return 'clients/%s-%s/%s' % (slugify(self.client.name), self.client.id, self.id)

    def getAnalyticsId(self):
        return '%s-%s-%s' % (self.client.id, self.id, 1)

    def getEditorUrl(self):
        return settings.HTTPS_DOMAIN + reverse('accounts_edit_campaign') + '?campaign=%s' % (self.id)

    def __unicode__(self):
        return "%s " % (self.name + ' ( Client ' + str(self.client) + ')')

    def __str__(self):
        return unicode(self).encode('ascii', 'ignore')


class PublishedCampaign(models.Model):

    campaign     = models.ForeignKey(VideoCampaign)
    config_data  = models.TextField(max_length=25600, blank=True, null=True )

    user         = models.ForeignKey(User)
    published_on = models.DateTimeField(default=datetime.utcnow().replace(tzinfo = pytz.utc),auto_now_add=True)
    config_url   = models.CharField(max_length=200, blank=True, null=True, default='')
    finalized    = models.BooleanField(default=False)

    preview_code = models.CharField(max_length=62, blank=True, null=True, editable=True)

    @classmethod
    def Create(cls, user, campaign, config_data):

        code = slugify(campaign.name) + '-' + get_rand_id(8)
        return cls.objects.create( user=user, campaign=campaign, config_data=config_data, preview_code=code)

    def finalize(self):

        conn    = S3Connection(settings.AWS_ACCESS_KEY, settings.AWS_SECRET_KEY)
        bucket  = S3Bucket( conn, settings.AWS_STATIC_BUCKET)

        #######################################################################
        # copy image files
        #######################################################################
        imageAssets = UploadAsset.objects.filter( campaign=self.campaign, mime_type__startswith='image', upload_successful=True, encoded=False )
        if imageAssets.count() > 0:

            logger.info("Will publish %s image files for campaign %s" % (imageAssets.count(), self.campaign))

            for asset in imageAssets:
                s3CopyKey( conn, bucket, asset.file_path, settings.AWS_UPLOAD_BUCKET)
                asset.encoded = True
                asset.save()

            conn.close()


        #######################################################################
        # Update configuration file
        #######################################################################
        mkdir_p(settings.PUBLISHED_DIR)

        filename = "%s-%s-%s.data" % ( slugify(self.campaign.client.name), self.campaign.client.id, self.preview_code )


        keyname = self.campaign.getOutputPath() + '/published/' + filename
        self.config_url = settings.CONFIG_URL_BASE + '/' + keyname

        preview_url = "%s/%s" % (settings.PLAY_URL, self.preview_code)

        config_obj  = json.loads(self.config_data, strict=False) # validates

        if config_obj['width'] < 1 or config_obj['height'] < 1 and hasValue(config_obj['options'], 'initial'):
            logger.info("Missing width/height in config for %s" % (self))
            initial = config_obj['options']['initial']
            initialAsset = get_object_or_404( UploadAsset,  file_id=initial, campaign=self.campaign )
            config_obj['width']  = initialAsset.width
            config_obj['height'] = initialAsset.height

        if not hasValue(config_obj['share'], 'link'):
            config_obj['share']['link'] = preview_url

        (width, height) = sizeDownWidthHeight( config_obj['width'], config_obj['height'] )
        config_obj['share']['embed'] = "<iframe src=\"%s/embed.html?x=%s\" width=\"%spx\" height=\"%spx\" frameborder=\"0\" style=\"overflow:hidden\" scrolling=\"no\" allowFullscreen></iframe>" % ( 'http://static.theochre.com', self.config_url, width, height)

        #base = string.rstrip( settings.STATIC_URL, '/')
        base = settings.VIDEO_URL # this takes care or local setups, ensures video's are got from s3 always
        config_obj['options']['baseUrl']    = base
        config_obj['options']['baseUrlHLS'] = base + '/' + self.campaign.getOutputPath() + '/hls'
        config_obj['options']['images']     = base
        config_obj['campaign']['published'] = True

        # if no poster, set share and splash img to initial thumbnail
        if hasValue(config_obj['options'], 'initial') and (not hasValue(config_obj['splash'], 'src')):
            initial = config_obj['options']['initial']

            if hasValue( config_obj[initial], 'original_output_path'):
                poster_key = config_obj[ initial ]['original_output_path'] + '/thumbnails/' + initial + '_poster_0000.png'

                config_obj['splash']['src']  = poster_key
                config_obj['share']['image'] = poster_key

                s3CopyKey( conn, bucket, poster_key, settings.AWS_UPLOAD_BUCKET)

            else:
                config_obj['share']['image'] = 'advntr/assets/ochrelogo.png'
                logger.info("%s missing initial original_output_path" % (filename))

        if not hasValue(config_obj['options'], 'clickThrough'):
            config_obj['options']['clickThrough'] = config_obj['share']['link']

        self.config_data = json.dumps( config_obj, indent=4, sort_keys=False, separators=(',', ': ') )

        local_filename = settings.PUBLISHED_DIR + '/' + filename
        with codecs.open(local_filename, "w", encoding='utf-8') as f:
            f.write(self.config_data)

        logger.info("%s wrote config %s" % (self.campaign, filename))

        uploadToS3( conn, bucket, local_filename, settings.AWS_STATIC_BUCKET, keyname)

        self.finalized  = True
        self.save()

        logger.info("Saved published config for %s to %s" % (self.campaign, filename))

        body = """
            Your interactive video for %s is ready at %s
        """ % (self.campaign.name, preview_url)

        attachments = []

        if self.campaign.client.feature_vpaid:


            mixpanel_event_data = { "event" : "AdTracking",
                                    "properties": {
                                                    "token" : settings.MIXPANEL_AD_TRACKING_TOKEN,
                                                    "Video" : self.campaign.name,
                                                    "campaignId" : self.campaign.id,
                                                    "campaignVersion" : config_obj['campaign']['campaignVersion'],
                                                    "clientId" : self.campaign.client.id} }

            mixpanel_event_data_click_tracking = copy.deepcopy(mixpanel_event_data)
            mixpanel_event_data_click_tracking['properties']['adClickUrl'] = config_obj['options']['clickThrough']


            vpaid_tag_out = render_to_string('vpaid_template.html', { 'config' : config_obj, 'config_url' : self.config_url, 'mixpanel_event_data' : mixpanel_event_data, 'mixpanel_event_data_click_tracking' : mixpanel_event_data_click_tracking, 'STATIC_URL' : settings.STATIC_URL, 'HTTPS_DOMAIN' : settings.HTTPS_DOMAIN } )

            filename = "%s-%s-%s-vpaid.xml" % ( slugify(self.campaign.client.name), self.campaign.client.id, self.preview_code )
            local_vpaid_file = settings.PUBLISHED_DIR + '/' + filename
            with codecs.open(local_vpaid_file, "w", encoding='utf-8') as f:
                f.write(vpaid_tag_out)

            keyname = self.campaign.getOutputPath() + '/published/' + filename
            uploadToS3( conn, bucket, local_vpaid_file, settings.AWS_STATIC_BUCKET, keyname)

            attachments.append( local_vpaid_file )

            vast_url = 'https:' + settings.STATIC_URL + keyname
            body += "<br/><br/>You can download a VAST/VPAID tag here: %s" % (vast_url)


        send_email([self.user.email,], 'Your Adventr is ready', body, 'info@adventr.tv', attachments=attachments)
        body += "<br/>%s" % (self.campaign.getEditorUrl())
        send_email(settings.NOTIFY_EMAILS, '%s campaign published by %s' % (self.campaign.name, self.user.email), body, 'info@adventr.tv', attachments=attachments)
        logger.info("Sent email for published config %s (%s)" % (self.campaign, filename))

    def getPublishedUrl(self):
        pass

    def __unicode__(self):
        return "PublishedCampaign(%s) for %s by %s" % (self.id, self.campaign, self.user)

    def __str__(self):
        return unicode(self).encode('ascii', 'ignore')

def s3CopyKey( conn, bucket, keyname, to_bucket):

    try:
        logger.debug("S3 Copying %s to %s" % (keyname, to_bucket))
        bucket.copy_key( keyname, to_bucket, keyname)
        logger.debug("S3 Done Copying %s to %s" % (keyname, to_bucket))
    except S3CopyError, inst:
        logger.error("S3 ERROR Copying %s to %s, inst=%s" % (keyname, to_bucket, inst))



def uploadToS3( conn, bucket, filename, aws_bucket, keyname):

    try:
        logger.info("Saving %s to %s/%s" % (filename, aws_bucket, keyname))

        k = Key(bucket)
        k.key = keyname
        k.set_contents_from_filename(filename)
        k.close()

        logger.info("Done saving %s to %s/%s" % (filename, aws_bucket, keyname))
    except Exception, inst:
        logger.error("Error saving %s to %s/%s: %s" % (filename, aws_bucket, keyname, inst))


class DataPoint(models.Model):

    analyticsid = models.CharField(max_length=24, blank=False, null=False, editable=True)

    # source, ua_browser
    datatype = models.CharField(max_length=100, blank=False, null=False, editable=True)

    # facebook.com, Chrome, etc
    key1  = models.CharField(max_length=100, blank=True, null=True, editable=True)

    # CA, IL, etc
    key2  = models.CharField(max_length=100, blank=True, null=True, editable=True)

    value = models.FloatField(default=None, null=True, editable=True)
    date  = models.DateTimeField(blank=True, null=True, editable=True)

    @classmethod
    def ParseAnalyticsId(cls, aid):

        parts = aid.split('-')
        if len(parts) != 3:
            return None

        return parts[1]

    def getCampaignId(self):
        return DataPoint.ParseAnalyticsId(self.analyticsid)




class DataPointValue(models.Model, JSONConvertable):
    # use int field instead of foreign key since campaigns may be missing (violates foreign key constraints)
    campaign_id = models.PositiveIntegerField(default=0, null=False, db_index=True)
    charttype   = models.PositiveIntegerField(default=0, choices=SEGMENT_TYPES)
    value       = models.CharField(max_length=100, blank=True, null=True, editable=True)


class DataFacts(models.Model):

    analyticsid = models.CharField(max_length=24, blank=False, default='', null=False, editable=True)

    day_of_month = models.PositiveIntegerField(default=0, null=False)
    day_of_week  = models.PositiveIntegerField(default=0, null=False)
    month        = models.PositiveIntegerField(default=0, null=False)
    year         = models.PositiveIntegerField(default=0, null=False)
    hour         = models.PositiveIntegerField(default=0, null=False)

    location     = models.CharField(max_length=100, blank=True)
    source       = models.CharField(max_length=255, blank=True)
    browser      = models.CharField(max_length=30, blank=True)
    os           = models.CharField(max_length=40, blank=True)
    flashversion = models.CharField(max_length=10, blank=True)

    views       = models.PositiveIntegerField(default=0, null=False)
    viewsunique = models.PositiveIntegerField(default=0, null=False)
    completions = models.PositiveIntegerField(default=0, null=False)
    duration    = models.FloatField(default=0)

#class DataFacts2(models.Model):

#    analyticsid = models.CharField(max_length=24, blank=False, default='', null=False, editable=True)

#    time     = models.ForeignKey(DimensionTime)
#    location = models.ForeignKey(DimensionLocation)
#    source   = models.ForeignKey(DimensionSource)
#    browser  = models.ForeignKey(DimensionBrowser)
#    os       = models.ForeignKey(DimensionOS)
#    flashversion = models.ForeignKey(DimensionFlashVersion)

#    views       = models.PositiveIntegerField(default=0, null=False)
#    viewsunique = models.PositiveIntegerField(default=0, null=False)
#    completions = models.PositiveIntegerField(default=0, null=False)
#    duration    = models.FloatField(default=0)

#class DimensionTime(models.Model):
#    day         = models.PositiveIntegerField(default=0, null=False)
#    day_of_week = models.PositiveIntegerField(default=0, null=False)
#    month       = models.PositiveIntegerField(default=0, null=False)
#    year        = models.PositiveIntegerField(default=0, null=False)
#    hour        = models.PositiveIntegerField(default=0, null=False)

#class DimensionLocation(models.Model):
#    city     = models.CharField(max_length=100, blank=True)
#    country  = models.CharField(max_length=100, blank=True)

#class DimensionSource(models.Model):
#    source  = models.CharField(max_length=250, blank=True)

#class DimensionBrowser(models.Model):
#    browser  = models.CharField(max_length=30, blank=True)

#class DimensionOS(models.Model):
#    os  = models.CharField(max_length=40, blank=True)

#class DimensionFlashVersion(models.Model):
#    flashversion  = models.CharField(max_length=10, blank=True)


#class DataFacts(models.Model):

#    dimTime = models.ForeignKey(DimensionTime)
#    dimLocation = models.ForeignKey(DimensionLocation)
#    dimSource = models.ForeignKey(DimensionSource)

#    views       = models.PositiveIntegerField(default=0, null=False)
#    viewsUnique = models.PositiveIntegerField(default=0, null=False)
#    completions = models.PositiveIntegerField(default=0, null=False)
#    duration = models.FloatField(default=0)


class UploadAsset(models.Model, JSONConvertable):

    file_original    = models.CharField(max_length=100, blank=False, null=False)
    file_path        = models.CharField(max_length=200, blank=False, null=False)
    file_id          = models.CharField(max_length=100, blank=False, null=False, default='')
    file_preview_flv = models.CharField(max_length=200, blank=False, null=False, default='')
    mime_type        = models.CharField(max_length=30, blank=True, null=True, unique=False, default='')
    user             = models.ForeignKey(User)
    campaign         = models.ForeignKey(VideoCampaign)
    created          = models.DateTimeField(default=datetime.utcnow().replace(tzinfo = pytz.utc),auto_now_add=True)
    upload_successful= models.NullBooleanField()

    duration         = models.DecimalField(default=0, max_digits=5, decimal_places=2)
    size_kb          = models.DecimalField(default=0, max_digits=8, decimal_places=0)
    width            = models.PositiveIntegerField(default=None, null=True)
    height           = models.PositiveIntegerField(default=None, null=True)
    thumbnails       = JSONField(default='[]')
    thumbnail_chosen = models.CharField(max_length=200, blank=True, null=True, default='')
    original_width   = models.PositiveIntegerField(default=None, null=True)
    original_height   = models.PositiveIntegerField(default=None, null=True)

    published        = models.BooleanField(default=False)
    encoded          = models.BooleanField(default=False)

    jobsys           = models.PositiveIntegerField(default=0, choices=JOB_SYSTEMS)
    jobid            = models.CharField(max_length=25, blank=True, null=True, default='')
    job_error_notified = models.BooleanField(default=False)

    class Meta:
        unique_together = ( ( 'campaign', 'file_path'), ('campaign', 'file_id') )

    def resetThumbnail(self):

        if self.mime_type and self.mime_type.startswith('image'):
            self.thumbnail_chosen = self.file_path
        elif self.mime_type and self.mime_type.startswith('video'):
            self.thumbnail_chosen = '%s/thumbnails/%s_0000.png' % (self.campaign.getOutputPath(), self.file_id)
        else:
            self.thumbnail_chosen = 'advntr/unknown_thumbnail.png'

    @classmethod
    def NewFileId(cls, name):
        name = name.replace(' ', '')
        idx = name.rfind('.')
        if idx > 0:
            name = name[:idx]

        name = name + '-' + get_rand_id(4, True)

        name = name.replace('.', '')
        name = slugify(name)

        return name


    def save(self, *args, **kwargs):

        if not self.thumbnail_chosen:
            self.resetThumbnail()

        super(UploadAsset, self).save(*args, **kwargs)

    def notifyUserError(self, error_msg):
        subj = 'Adventr: An error occurred encoding %s' % (self.file_original)
        body = subj + '\nError:%s' % (error_msg)

        if not self.job_error_notified:
            self.job_error_notified = True
            self.save()

            send_email( [self.user.email, ], subj, body, 'info@adventr.tv')

    def __unicode__(self):
        return "UploadAsset(%s) file_original=%s file_id=%s file_path=%s" % (self.id, self.file_original, self.file_id, self.file_path)

    def __str__(self):
        return unicode(self).encode('ascii', 'ignore')


class ConfigurationHistory(models.Model):

    user         = models.ForeignKey(User)
    campaign     = models.ForeignKey(VideoCampaign)
    created      = models.DateTimeField(default=datetime.utcnow().replace(tzinfo = pytz.utc),auto_now_add=True)
    config_data  = models.TextField(max_length=25600, blank=True, null=True )



class Profile(models.Model):

    user         = models.OneToOneField(User)
    client       = models.ForeignKey(Client)
    uid          = models.CharField(max_length=25, null=True)
    access_token = models.CharField(max_length=250, null=True)
    dob          = models.DateField(null=True, blank=True)
    occupation   = models.CharField(max_length=100, null=True, blank=True)
    location     = models.CharField(max_length=100, null=True, blank=True)
    website      = models.CharField(max_length=255, null=True, blank=True)



    def __init__(self, *args, **kwargs):
        super(Profile, self).__init__(*args, **kwargs)

    def __unicode__(self):
        return "%s " % (self.user)

    def __str__(self):
        return unicode(self).encode('ascii', 'ignore')

    def save(self, user=None, fb=None):

        if user:
            self.user = user

        if fb:
            self.uid          = fb.uid
            self.access_token = fb.access_token
            self.client = Client.objects.create(name=self.user.username, account_code=get_rand_id(10) )

        super(Profile,self).save()

def get_rand_id(x=6, alpha=False):
    choices = string.digits
    if alpha:
        choices += string.letters

    return ''.join(random.choice(choices) for x in range(x))


def mkdir_p(path):
    try:
        if not os.path.exists(path):
            os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST:
            pass
        else: raise


def sizeDownWidthHeight( width, height, maxWidth=640, maxHeight=360 ):
    width = math.floor( float( width) )
    height = math.floor( float( height ) )

    width  = maxWidth  if not width or width <= 0 else width
    height = maxHeight if not height or height <= 0 else height

    newWidth  = width
    newHeight = height
    if width > maxWidth:
        newWidth = maxWidth
        newHeight = float(height) / float(width) * newWidth
    return (int(newWidth), int(newHeight))

add_introspection_rules([], ["^base\.models\.JSONField"])
