from django.contrib.auth.models import User
from django.contrib.auth import logout as auth_logout

import logging
logger = logging.getLogger(__name__)

########################
# example nginx config
#
# http {
#     log_format custom '"$sent_http_x_remote_user_id" $remote_addr - $remote_user [$time_local] "$request" $status $bytes_sent "$http_referer" "$http_user_agent"';
#     access_log /var/log/nginx/access.log custom;
# }
########################
class ProfileHeaderMiddleware(object):

    def process_response(object, request, response):

        if response:
            if hasattr(request, 'user') and hasattr(request.user, 'id') and request.user.is_authenticated():
                response['X-Remote-User-Id'] = 'OCHRE-%s[%s]' % ( str(request.user.id), request.user.email)
            else:
                response['X-Remote-User-Id'] = 'OCHRE-Anon'

        return response
        

class ImpersonateMiddleware(object):
    def process_request(self, request):

        if ( request.session.has_key('impersonate_user') and request.META.has_key('PATH_INFO') and request.META['PATH_INFO'].startswith('/admin') ) \
            or "__unimpersonate" in request.GET:

            logger.info('Logging out impersonated user ' + request.session['impersonate_user']  + ' for admin')
            del request.session['impersonate_user']
            auth_logout(request)

            return None

        elif request.user.is_superuser and "__impersonate" in request.GET:
            request.session['impersonate_user'] = request.GET["__impersonate"]

        if request.user.is_superuser and 'impersonate_user' in request.session:
            admin_username = request.user.username
            username = request.session['impersonate_user']

            app = ''
            if request.META.has_key('PATH_INFO'):
                app = ': Viewing ' + request.META['PATH_INFO']

            request.user = User.objects.get(username=username)
            logger.info("Admin " + admin_username + " is impersonating " + request.user.email + '/' + username + app)

        return None

