
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
from django.conf import settings
import time, string, math, urllib, uuid
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout as auth_logout
from web.base.models import *

import facebook

import logging
logger = logging.getLogger(__name__)

class Auth(object):
    supports_object_permissions = False
    supports_anonymous_user = False
    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


class FacebookAuth(Auth):
    def authenticate(self, uid=None):
        try:
            return Profile.objects.get( uid=uid).user

        except Profile.DoesNotExist:
            return None
        except Exception, inst:
            logger.error("FacebookAuth Exception %s" % (inst))
            return None


class Facebook(object):

    def __init__(self, access_token):
        self.access_token = access_token
        logger.info("Loading graph from access_token=%s" % (self.access_token))
        self.graph = facebook.GraphAPI(self.access_token)

        try:
            self.me = self.graph.get_object("me")
        except facebook.GraphAPIError, inst:
            logger.error("Error getting graph object for access_token=%s error=%s" % (access_token, inst))
            raise


        self.uid = self.me.get('uid', self.me.get('id', None))
        if not self.uid:
            logger.error("Error getting uid object for access_token=%s" % (access_token))


def fb_required(function):
    def wrapper(request, *args, **kw):

        if not request.user or not request.user.is_authenticated():
            return redirect(reverse('facebook_login'))

        fb = getFacebookSession( request )
        if not fb:
            return redirect(settings.LOGIN_URL)

        request.fb = fb
        return function(request, *args, **kw)
    return wrapper


def getFacebookSession( request ):

    if request.session.has_key('facebook'):
        fb = request.session['facebook']

        # TODO determine if access_token is stale/expired
        return fb

    try:
        fb = Facebook( request.user.profile.access_token )
        return fb
    except facebook.GraphAPIError, inst:
        logger.error("Unable to create Facebook obj from profile.access_token %s" % (inst))
        return None 

    return None


def facebook_login(request):
    if not request.user.is_authenticated():

        FB_REQUEST_URL = 'https://www.facebook.com/dialog/permissions.request?app_id=%s&display=page&next=%s&response_type=code&perms=%s&fbconnect=1&cancel_url=%s' % (settings.FACEBOOK_APP_ID, settings.HTTP_DOMAIN + reverse('facebook_login_callback'), settings.FACEBOOK_REQUEST_PERMISSIONS, settings.HTTPS_DOMAIN)

        return redirect(FB_REQUEST_URL)

    return redirect('start')



def facebook_login_callback(request):

    try:
        code = request.GET['code']
        access_token = facebook.get_token_from_code( code, settings.FACEBOOK_APP_ID, settings.FACEBOOK_APP_SECRET, settings.HTTP_DOMAIN + reverse('facebook_login_callback'))

    except Exception, inst:
        logger.error("Error getting access code %s" % (inst))
        return redirect(settings.HTTP_DOMAIN + '/?error=1')


    fb = Facebook( access_token )
    request.session['facebook'] = fb

    user = authenticate(uid=fb.uid)

    logger.info("user=%s " % (user))

    if user is None or user.is_anonymous():

        logger.info("User does not exist for uid=%s" % (fb.uid))

        # TODO make transaction

        # create user
        username = str(uuid.uuid4())[:30]

        email = fb.me.get('email', None)
        if not email:
            logger.info('Missing email')
            #return redirect('get_email')


        # if email is missing, prompt later
        user            = User.objects.create_user(username, email, username)
        user.first_name = fb.me.get('first_name', '')
        user.last_name  = fb.me.get('last_name', '')
        user.save()

        profile = Profile()
        profile.save(user, fb)
        logger.info("Created new user=%s profile=%s" % (user, profile.uid))

        user = authenticate(uid=fb.uid)
        login(request, user)
        
    elif not user.is_active: # user deactivated?
        return render_to_response('inactive.html', extra_context, context_instance=RequestContext(request))
    else:
        login(request, user)

        user.profile.access_token = access_token
        user.profile.save()

    next = request.POST.get("next", request.GET.get("next", None))
    if next:
        return redirect(next)

    return redirect('accounts_dashboard')


