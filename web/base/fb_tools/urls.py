from django.conf.urls.defaults import *

urlpatterns = patterns('base.fb_tools.views',
    url('^start/*', 'start', name='fb_start'),
)

urlpatterns += patterns('base.fb_tools.facebook_auth',

    url('^login/$', 'facebook_login', name='facebook_login'),
    url('^fb_login_callback/$', 'facebook_login_callback', name='facebook_login_callback'),
    
)


