from base.fb_tools.facebook_auth import *
from base.fb_tools.facebook import GraphAPIError
from django.utils import simplejson
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.conf import settings
from django.contrib.auth import login, authenticate, logout as auth_logout
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import RequestContext

import logging
logger = logging.getLogger(__name__)


@fb_required
def start( request ): 
    return redirect('accounts_dashboard')


