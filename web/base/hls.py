from django.shortcuts import render, render_to_response
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import RequestContext
from django.conf import settings
from django.template.loader import render_to_string

import random, string
from base.views import getJsonResponse


def write_segment_file( view_id, segment_id,  bitrate, segment_durations, base_url, instant_switch, write_header=False ):

    filename = "%s-%sk.m3u8" % ( view_id, bitrate )

    # get max segment_durations for ext-x-targetduration
    with open(settings.PLAYLISTS_DIR + '/' + filename, "a") as f:
        if write_header:
            f.write('#EXTM3U\n#EXT-X-ALLOW-CACHE:NO\n#EXT-X-PLAYLIST-TYPE:%s\n#EXT-X-TARGETDURATION:10\n' % ( ('VOD' if instant_switch else 'EVENT')) )

        for i in xrange( 1, len(segment_durations) + 1 ):
            f.write('#EXTINF:%s,\n%s/%s-%sk-000%02d.ts\n' % (segment_durations[i-1], base_url, segment_id, bitrate, i))

        if instant_switch:
            f.write('#EXT-X-ENDLIST')


def init_playlists( request, view_id, segment_id, bitrates, segment_durations, base_url, instant_switch ):

    data = render_to_string('hls_playlist.html', {'segment_id' : segment_id, 'view_id' : view_id, 'bitrates' : bitrates, 'instant_switch' : instant_switch }, context_instance=RequestContext(request))

    filename = '%s.m3u8' % (view_id)

    # write the master playlist file
    with open(settings.PLAYLISTS_DIR + '/' + filename, "w") as f:
        f.write(data)

    for bitrate in bitrates:
        write_segment_file( view_id, segment_id, bitrate, segment_durations, base_url, instant_switch, write_header=True )

    return 'static/playlists/' + filename


def hls_choose( request ):

    view_id           = request.POST['view_id']
    segment_id        = request.POST['segment_id']
    instant_switch    = request.POST['instant_switch'] == '1'
    bitrates          = request.POST['bitrates'].split(',')
    base_url          = request.POST['base_url']
    segment_durations = request.POST['segment_durations'].split(',')

    if instant_switch == True:
        view_id = get_view_id() 
        playlist = init_playlists( request, view_id, segment_id, bitrates, segment_durations, base_url, instant_switch )

    else:
        playlist = 'static/playlists/%s.m3u8' % ( view_id )
        for bitrate in bitrates:
            write_segment_file( view_id, segment_id, bitrate, segment_durations, base_url, instant_switch )

    return getJsonResponse( {'success' : 1, 'view_id' : view_id, 'playlist' : '/' + playlist} )


def get_view_id():
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(15))
 

def hls_create( request ):

    view_id = get_view_id();

    bitrates          = request.GET['bitrates'].split(',')
    segment_id        = request.GET['segment_id']
    segment_durations = request.GET['segment_durations'].split(',')
    base_url          = request.GET['base_url']
    instant_switch    = request.GET['instant_switch'] == '1'

    playlist = init_playlists( request, view_id, segment_id, bitrates, segment_durations, base_url, instant_switch )

    return getJsonResponse( {'success' : 1, 'playlist' : '/' + playlist, 'view_id' : view_id} )
    

