from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.views.generic.base import TemplateView, RedirectView
from django.contrib import admin
from django.views.static import serve
admin.autodiscover()

from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


import os
project_dir = os.path.dirname(__file__)

from djstripe.views import ChangeCardView
# Uncomment the next two lines to enable the admin:
#from django.contrib import admin
#admin.autodiscover()

urlpatterns = patterns('',

    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^logout/*', 'base.views.logout_user', name='logout' ),

    url(r'^zencallback/*', 'base.encoding.zencoder_callback' ),

    url(r'^fb/', include('base.fb_tools.urls')),
    
    (r'^checkr/*', 'base.views.ping'),

    ##################################
    # Http Live Stremaing handlers
    (r'^hls_create/*', 'base.hls.hls_create'),
    (r'^hls_choose/*', 'base.hls.hls_choose'),
    ##################################

    url(r'^(?P<code>.*\d{8})$', 'base.views.play'),
    url(r'^(?P<code>.*\d{10})$', 'base.views.play'),
    url(r'^play/*', 'base.views.play', name='play'),

    url(r'^admin/profile/(?P<id>.*)$', 'base.admin.profile_overview', name='admin_profile'),
    url(r'^accounts/login/*', 'base.views.login_user', name='accounts_login' ),
    url(r'^accounts/account/*', 'base.views.accounts_account', name='accounts_account' ),
    url(r'^accounts/dashboard_values/*', 'base.views.accounts_dashboard_values', name='accounts_dashboard_values'),
    url(r'^accounts/dashboard_filter/*', 'base.views.accounts_dashboard_filter', name='accounts_dashboard_filter'),
    url(r'^accounts/dashboard/*', 'base.views.accounts_dashboard', name='accounts_dashboard'),
    url(r'^accounts/all/*', 'base.views.accounts_dashboard_allclients', name='accounts_dashboard_allclients'),
    url(r'^accounts/create/*', 'base.views.accounts_create', name='accounts_create'),
    url(r'^accounts/campaigns/funnel_filter/*', 'base.views.accounts_funnel_filter', name='accounts_funnel_filter'),
    url(r'^accounts/campaigns/funnel/*', 'base.views.accounts_funnel', name='accounts_funnel'),
    url(r'^accounts/campaigns/reset/*', 'base.views.accounts_reset_campaign', name='accounts_reset_campaign'),
    url(r'^accounts/campaigns/chart/*', 'base.views.accounts_dashboard', name='accounts_dashboard'),
    url(r'^accounts/campaigns/edit/*', 'base.views.accounts_edit_campaign', name='accounts_edit_campaign'),
    url(r'^accounts/campaigns/published/*', 'base.views.accounts_published', name='accounts_published'),
    url(r'^accounts/campaigns/new/*', 'base.views.accounts_create_campaign', name='accounts_create_campaign'),
    url(r'^accounts/campaigns/play/*', 'base.views.accounts_play', name='accounts_play_campaign'),
    url(r'^accounts/campaigns/preview/*', 'base.views.accounts_preview', name='accounts_preview'),
    url(r'^accounts/encode/*', 'base.views.accounts_encode', name='accounts_encode'),
    url(r'^accounts/delete_asset/*', 'base.views.accounts_delete_asset', name='delete_asset'),
    url(r'^accounts/revisions/*', 'base.views.accounts_revisions', name='accounts_revisions'),

    url(r'^accounts/s3sign/*', 'base.views.accounts_s3sign'),
    url(r'^accounts/s3state/*', 'base.views.accounts_s3state'),
    url(r'^accounts/update_thumbnails/*', 'base.views.accounts_update_thumbnails'),

    url(r'^accounts/campaigns/config/*', 'base.views.accounts_config', name='accounts_config'),



    url(r'^cfg/*', 'base.views.get_config', name='get_config'),

    url(r'^vast_events/*', 'base.views.vast_events', name='vast_events'),
    url(r'^fb_share/*', 'base.views.fb_share', name='fb_share'),


    url(r'^analytics/track/*', 'base.views.analytics_track', name='analytics_track'),

    url(r'^payments/', include('djstripe.urls', namespace="djstripe")),


    ##################################
    # Public 
    (r'^$', TemplateView.as_view(template_name='public_theme2_index.html')),
    (r'^index.html', TemplateView.as_view(template_name='public_theme2_index.html')),
    (r'^enterprise', TemplateView.as_view(template_name='public_theme2_enterprise.html')),
    (r'^pricing', TemplateView.as_view(template_name='public_theme2_pricing.html')),
    (r'^jobs/*', TemplateView.as_view(template_name='public_theme2_jobs.html')),
    (r'^privacy/*', TemplateView.as_view(template_name='privacy.html')),
    #(r'^signup/*', TemplateView.as_view(template_name='public_theme2_signup.html')),
    url(r'^signup/*', RedirectView.as_view(url='/pricing/'), name='signup'),
    ##################################



    ##################################
    # Demos
    (r'^m/*', TemplateView.as_view(template_name='demos/demo.html')),
    (r'^platform/*', TemplateView.as_view(template_name='demos/demo.html')),
    (r'^show/*', TemplateView.as_view(template_name='demos/demo.html')),
    (r'^canvas/*', TemplateView.as_view(template_name='demos/demo.html')),
    (r'^demo/*', TemplateView.as_view(template_name='demos/demo.html')),

    (r'^resize', TemplateView.as_view(template_name='demos/resize.html')),
    (r'^znv/html5', TemplateView.as_view(template_name='demos/znv/html5.html')),
    (r'^techcrunch/chevy*', TemplateView.as_view(template_name='demos/techcrunch/chevy.html')),
    (r'^znv/*', TemplateView.as_view(template_name='demos/znv/index.html')),
    (r'^sundancechannel/love-or-hate*', TemplateView.as_view(template_name='demos/sundancechannel/love-or-hate.html')),
    (r'^huffpo/*', TemplateView.as_view(template_name='demos/huffpo/index.html')),
    (r'^revolt/wale/*', TemplateView.as_view(template_name='demos/revolt/wale.html')),
    (r'^indigo2/*', TemplateView.as_view(template_name='demos/indigo2/index.html')),
    (r'^indigo/*', TemplateView.as_view(template_name='demos/indigo/index.html')),
    (r'^spiderman/*', TemplateView.as_view(template_name='demos/spiderman/index.html')),
    (r'^hungergames2/*', TemplateView.as_view(template_name='demos/hungergames2/index.html')),
    (r'^amazingspiderman/*', TemplateView.as_view(template_name='demos/spiderman/index.html')),
    (r'^webmd/*', TemplateView.as_view(template_name='demos/webmd/index.html')),
    (r'^asap/*', TemplateView.as_view(template_name='demos/asap/index.html')),
    (r'^promo/*', TemplateView.as_view(template_name='demos/promo/index.html')),
    (r'^article-view/*', TemplateView.as_view(template_name='demos/article-view/index.html')),

    (r'^camtest/*', TemplateView.as_view(template_name='demos/camtest.html')),

    (r'^spotify/*', TemplateView.as_view(template_name='demos/spotify/index.html')),
    (r'^cinematic/*', TemplateView.as_view(template_name='demos/cinematic/index.html')),
    (r'^riot/*', TemplateView.as_view(template_name='demos/riot/index.html')),
    ##################################


    (r'^embed.html', TemplateView.as_view(template_name='embed.html')),
    (r'^crossdomain.xml', TemplateView.as_view(template_name='crossdomain.xml')),
    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': 'static/'}),

)
