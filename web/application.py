import os, sys

#Calculate the path based on the location of the WSGI script.
this_file_dir= os.path.dirname(__file__)
project_dir = os.path.dirname(this_file_dir)
sys.path.append(this_file_dir)
sys.path.append(project_dir)


def load_env(fn):
    if os.path.exists(fn):
        for line in [line.strip() for line in open(fn)]:
                if len(line) > 0 and not line.startswith('#'):
                        key,value=line.split("=", 1)
                        key = key.replace('export ', '')
                        os.environ[key.strip()]=value.strip('\'" ')

load_env(project_dir + '.env')

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

