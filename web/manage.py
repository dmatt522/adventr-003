#!/usr/bin/env python
from django.core.management import execute_manager
import os,imp
try:
    imp.find_module('settings') # Assumed to be in the same directory.
except ImportError:
    import sys
    sys.stderr.write("Error: Can't find the file 'settings.py' in the directory containing %r. It appears you've customized things.\nYou'll have to run django-admin.py, passing it your settings module.\n" % __file__)
    sys.exit(1)


def load_env(fn):
    if os.path.exists(fn):
        for line in [line.strip() for line in open(fn)]:
                if len(line) > 0 and not line.startswith('#'):
                        key,value=line.split("=", 1)
                        key = key.replace('export ', '')
                        os.environ[key.strip()]=value.strip('\'" ')


this_file_dir= os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.dirname(this_file_dir)

load_env(project_dir + '.env') 

import settings

if __name__ == "__main__":
    execute_manager(settings)
