from settings import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

INTERNAL_IPS = ('0.0.0.0','localhost', '127.0.0.1','10.0.2.15')

STATIC_URL = 'http://10.0.2.15:8000/static/'

DATABASES = {'default': 
                {
                  'ENGINE':  'django.db.backends.sqlite3',
                  'NAME': 'ochre.sqlite'
                }             
}

TEMPLATE_LOADERS = (

        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
)


# django-debug-toolbar
MIDDLEWARE_CLASSES = MIDDLEWARE_CLASSES + ('debug_toolbar.middleware.DebugToolbarMiddleware',)
INSTALLED_APPS += ('debug_toolbar', 'django_extensions',)


ALLOWED_HOSTS = ALLOWED_HOSTS + ['0.0.0.0:8000', 'localhost', '127.0.0.1', *]
