from django.conf import settings

def add_settings(request):
    data = {
        'DEBUG' : settings.DEBUG,
        'HTTP_DOMAIN': settings.HTTP_DOMAIN,
        'HTTPS_DOMAIN': settings.HTTPS_DOMAIN,
        'STATIC_URL' : settings.STATIC_URL,
        'UPLOAD_URL' : settings.UPLOAD_URL,
        'PLAY_URL' : settings.PLAY_URL,
        'MIXPANEL_ACCOUNTS_ACTIVITY_TOKEN' : settings.MIXPANEL_ACCOUNTS_ACTIVITY_TOKEN,
        'AWS_ACCESS_KEY' : settings.AWS_ACCESS_KEY,
    }
    return data
